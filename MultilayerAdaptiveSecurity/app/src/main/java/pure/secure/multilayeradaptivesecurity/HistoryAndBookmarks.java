package pure.secure.multilayeradaptivesecurity;

/**
 * Created by qiuding on 10/16/2014.
 */
public class HistoryAndBookmarks {

    int id;
    String title;
    String url;
    String date;
    String createdAt;

    public HistoryAndBookmarks() {
    }

    public HistoryAndBookmarks(String title, String url) {
        this.id = id;
        this.title = title;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
