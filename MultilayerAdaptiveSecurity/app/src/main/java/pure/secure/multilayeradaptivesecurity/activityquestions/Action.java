package pure.secure.multilayeradaptivesecurity.activityquestions;

public class Action {

	int confidence;
	int type;
	long timestamp;
	
	public Action(int confidence, int type, long timestamp){
		this.confidence = confidence;
		this.type = type;
		this.timestamp = timestamp;
	}
	
	@Override
	public boolean equals(Object o){
		if(!(o instanceof Action))
			return false;
		
		Action action = (Action) o;
		
		return action.type == type;
	}
}
