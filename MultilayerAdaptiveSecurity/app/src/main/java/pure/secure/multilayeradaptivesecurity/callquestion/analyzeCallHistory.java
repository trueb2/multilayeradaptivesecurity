package pure.secure.multilayeradaptivesecurity.callquestion;

import net.sf.jsefa.csv.annotation.CsvDataType;
import net.sf.jsefa.csv.annotation.CsvField;

//import static java.nio.file.Files.readAllBytes;
//import static java.nio.file.Paths.get;

/**
 * Created by Qiuhua on 10/21/14.
 */

@CsvDataType
class callHistoryRow
{
    @CsvField(pos = 1)
    int id;

    @CsvField(pos = 2)
    String  name;

    @CsvField(pos = 3)
    String phoneNumber;

    @CsvField(pos = 4)
    int duration;

    @CsvField(pos = 5)
    long date;

    @CsvField(pos = 6)
    int type;

    @CsvField(pos = 7)
    String timeStamp;
}


public class analyzeCallHistory {
//
//    /**
//     * find the most occurring words in a stringList
//     * @param pairList
//     * @return result
//     */
//    public static List<String> getMostOccurrence(List<Pair<String, String>> pairList){
//        List<String> stringList = new ArrayList<String>();
//        for(Pair pair : pairList){
//            try {
//                stringList.add(pair.getKey().toString());
//            } catch(NullPointerException e) {
//            }
//        }
//
//        List<String> result = new ArrayList<String>();
//        HashMap<String,Integer> stringIntegerHashMap = new HashMap<String, Integer>();
//
//        for(String string : stringList){
//            if (stringIntegerHashMap.containsKey(string)){
//                stringIntegerHashMap.put(string, stringIntegerHashMap.get(string) + 1);
//            }
//            else {
//                stringIntegerHashMap.put(string, 0);
//            }
//        }
//
//        Integer maxValue = Collections.max(stringIntegerHashMap.values());
//
//        for(Map.Entry<String, Integer> entry : stringIntegerHashMap.entrySet()){
//            if(entry.getValue() == maxValue){
//                result.add(entry.getKey());
//            }
//        }
//
//        return result;
//    }
//
//    /**
//     *
//     * @param stringList
//     * @return
//     */
//    public static String pickRandom(List<String> stringList){
//        Random random = new Random();
//        return stringList.get(random.nextInt(stringList.size()));
//    }
//
//    /**
//     *
//     * @param args
//     */
//    public static ArrayList<Question> generateCallQuestion() {
//        String simpleTime;
//        Format formatter;
////        formatter = new SimpleDateFormat("MM/dd/yyyy");
//        formatter = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss z");
//
//        String contact;
//        List<String> contactList = new ArrayList<String>();
//
//        TreeMap<String,List<Pair<String, String>>> callHistoryHash = new TreeMap<String, List<Pair<String, String>>>();
//
//        String cvsData = null;
//        try {
//            cvsData = new String(readAllBytes(get("callHistory.csv")));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        // parse the csv file and insert it into TreeMap
//        CsvConfiguration csvConfiguration = new CsvConfiguration();
//        csvConfiguration.setFieldDelimiter(',');
//        csvConfiguration.setLineFilter(new HeaderAndFooterFilter(1, false, false));
//
//        Deserializer deserializer = CsvIOFactory.createFactory(csvConfiguration, callHistoryRow.class).createDeserializer();
//
//        deserializer.open(new StringReader(cvsData));
//        while (deserializer.hasNext()) {
//            List<Pair<String, String>> temp = new ArrayList<Pair<String, String>>();
//            callHistoryRow row = deserializer.next();
//            Date time = new Date(row.date); // convert unix time to readable time
//            simpleTime = formatter.format(time);
//            String simpleTimeShort = simpleTime.substring(0, 10); // yyyy.MM.dd
//
////            System.out.println(simpleTime);
////            System.out.println(row.name);
////            System.out.println(row.phoneNumber);
////            System.out.println(row.name == "null" ? "1" : "0");
////            if (row.name == "null") contact = row.phoneNumber;
////            else contact = row.name;
//
//            contact = row.phoneNumber;
//            // generate a contact list for random pick when generating questions
//            if (!contactList.contains(contact)) {
//                contactList.add(contact);
//            }
//
////            Pair<String, String> temp = new Pair<String, String>(contact, simpleTime);
//
//            if (callHistoryHash.containsKey(simpleTimeShort)) {
//                temp = callHistoryHash.get(simpleTimeShort);
//                Pair<String, String> tempPair = new Pair<String, String>(contact, simpleTime);
//                temp.add(tempPair);
//                callHistoryHash.put(simpleTimeShort,temp);
//            }
//            else {
//                Pair<String, String> tempPair = new Pair<String, String>(contact, simpleTime);
//                temp.add(tempPair);
//                callHistoryHash.put(simpleTimeShort, temp);
//            }
//        }
//        deserializer.close(true);
//
//        // generate questions
//        for(Map.Entry<String, List<Pair<String, String>>> entry : callHistoryHash.entrySet()){
////            System.out.println(entry.getValue());
//            System.out.printf("Key : %s and Value: %s %n", entry.getKey(), entry.getValue());
//            System.out.println("most occurring contact is:" + getMostOccurrence(entry.getValue()));
//            System.out.println("======================= Question ========================");
//            System.out.println("Who did you contact most on " + entry.getKey() + "?");
//            List<String> candidateList = new ArrayList<String>();
//            // get first element of mostOccurrence as my correct answer
//            String correctAnswer = getMostOccurrence(entry.getValue()).get(0);
//            candidateList.add(correctAnswer);
//            // get three random wrong answer
//            List<String> contactListCopy = new ArrayList<String>(contactList);
//            contactListCopy.removeAll(candidateList); // making sure we will not pick correct answer from contactList
//            for(int i = 0; i < 3; i++){
//                String tmp = pickRandom(contactListCopy);
//                candidateList.add(tmp);
//                contactListCopy.remove(tmp); // make sure there is no duplicate
//            }
//            // shuffle
//            Collections.shuffle(candidateList);
//            System.out.println("A: " + candidateList.get(0) + " B: " + candidateList.get(1)
//                    + " C: " + candidateList.get(2) + " D: " + candidateList.get(3));
//            System.out.println("Correct answer is: " + correctAnswer);
//            System.out.println("==================== End of Question ====================");
//
////            System.out.println("======================= Question 2 ========================");
//
//        }
//
//    }
}
