package pure.secure.multilayeradaptivesecurity;

/**
 * Created by Qiuhua on 10/16/14.
 */
public class Activity {
    int id;
    String confidence;
    String type;
    String createdAt;

    public Activity() {
    }

    public Activity(String confidence, String type) {
        this.confidence = confidence;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getConfidence() {
        return confidence;
    }

    public void setConfidence(String confidence) {
        this.confidence = confidence;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
