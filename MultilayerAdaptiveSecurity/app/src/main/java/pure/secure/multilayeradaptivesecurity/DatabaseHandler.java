package pure.secure.multilayeradaptivesecurity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by qiuding on 9/30/2014.
 *
 * reference: http://www.androidhive.info/2011/11/android-sqlite-database-tutorial/
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    /**
     * bunch of static constant
     */
    // database version
    private static final int DATABASE_VERSION = 1;

    // database name
    private static final String DATABASE_NAME = "userHistoryManager";

    // callHistory table name
    private static final String TABLE_CALLHISTORY = "callHistory";

    // callHistory table columns names
    private static final String CALLHISTORY_KEY_ID = "id";
    private static final String CALLHISTORY_KEY_NAME = "name";
    private static final String CALLHISTORY_KEY_PH_NO = "phoneNumber";
    private static final String CALLHISTORY_KEY_DURATION = "duration";
    private static final String CALLHISTORY_KEY_DATE = "date";
    private static final String CALLHISTORY_KEY_TYPE = "type";

    // listApps table name
    private static final String TABLE_LISTAPPS = "listApps";

    // listApps table columns names
    private static final String LISTAPPS_KEY_ID = "id";
    private static final String LISTAPPS_KEY_NAME = "name";
    private static final String LISTAPPS_KEY_SRCDIR = "srcDir";
    private static final String LISTAPPS_KEY_LAUNACT = "launchAct";

    // historyAndBookmarks table name
    private static final String TABLE_HISNBKMK = "historyAndBookmarks";

    // historyAndBookmarks table columns names
    private static final String HISNBKMK_KEY_ID = "id";
    private static final String HISNBKMK_KEY_TITLE = "title";
    private static final String HISNBKMK_KEY_URL = "url";

    // processes table
    private static final String TABLE_PROCESSES = "processes";
    private static final String PROCESSES_KEY_ID = "id";
    private static final String PROCESSES_KEY_PROCNAME = "processName";
    private static final String PROCESSES_KEY_IMPORTANCE = "importance";

    // activity table
    protected static final String TABLE_ACTIVITY = "activity";
    private static final String ACTIVITY_KEY_ID = "id";
    private static final String ACTIVITY_KEY_CONFIDENCE = "confidence";
    private static final String ACTIVITY_KEY_TYPE = "type";

    // location table
    private static final String TABLE_LOCATION = "location";
    private static final String LOCATION_KEY_ID = "id";
    private static final String LOCATION_KEY_LOCATIONSTR = "locationString";

    // SMS table
    private static final String TABLE_SMS = "sms";
    private static final String SMS_KEY_ID = "id";
    private static final String SMS_KEY_NUMBER = "number";
    private static final String SMS_KEY_DATE = "date";
    private static final String SMS_KEY_TYPE = "type";

    // timestamp
    private static final String TIME_STAMP = "createdAt";

    private static final String CREATE_CALLHISTORY_TABLE = "CREATE TABLE " + TABLE_CALLHISTORY + "(" + CALLHISTORY_KEY_ID +
            " INTEGER PRIMARY KEY," + CALLHISTORY_KEY_NAME + " TEXT," + CALLHISTORY_KEY_PH_NO + " TEXT," + CALLHISTORY_KEY_DURATION +
            " TEXT," + CALLHISTORY_KEY_DATE + " TEXT," + CALLHISTORY_KEY_TYPE + " TEXT," + TIME_STAMP + " DATETIME DEFAULT CURRENT_TIMESTAMP" + ")";

    private static final String CREATE_LISTAPPS_TABLE = "CREATE TABLE " + TABLE_LISTAPPS + "(" + LISTAPPS_KEY_ID + " INTEGER PRIMARY KEY," +
            LISTAPPS_KEY_NAME + " TEXT," + LISTAPPS_KEY_SRCDIR + " TEXT," + LISTAPPS_KEY_LAUNACT + " TEXT" + TIME_STAMP +
            " DATETIME DEFAULT CURRENT_TIMESTAMP" + ")";

    private static final String CREATE_HISNBKMK_TABLE = "CREATE TABLE " + TABLE_HISNBKMK + "(" + HISNBKMK_KEY_ID + " INTEGER PRIMARY KEY," +
            HISNBKMK_KEY_TITLE + " TEXT," + HISNBKMK_KEY_URL + " TEXT," + TIME_STAMP + " DATETIME DEFAULT CURRENT_TIMESTAMP" + ")";

    private static final String CREATE_PROCESSES_TABLE = "CREATE TABLE " + TABLE_PROCESSES + "(" + PROCESSES_KEY_ID + " INTEGER PRIMARY KEY," +
            PROCESSES_KEY_PROCNAME + " TEXT," + PROCESSES_KEY_IMPORTANCE + " TEXT," + TIME_STAMP + " DATETIME DEFAULT CURRENT_TIMESTAMP" + ")";

    private static final String CREATE_ACTIVITY_TABLE = "CREATE TABLE " + TABLE_ACTIVITY + "(" + ACTIVITY_KEY_ID + " INTEGER PRIMARY KEY," +
            ACTIVITY_KEY_CONFIDENCE + " TEXT," + ACTIVITY_KEY_TYPE + " TEXT," + TIME_STAMP + " DATETIME DEFAULT CURRENT_TIMESTAMP" + ")";

    private static final String CREATE_LOCATION_TABLE = "CREATE TABLE " + TABLE_LOCATION + "(" + LOCATION_KEY_ID + " INTEGER PRIMARY KEY," +
            LOCATION_KEY_LOCATIONSTR + " TEXT," + TIME_STAMP + " DATETIME DEFAULT CURRENT_TIMESTAMP" + ")";

    private static final String CREATE_SMS_TABLE = "CREATE TABLE " + TABLE_SMS + "(" + SMS_KEY_ID + " INTEGER PRIMARY KEY," +
            SMS_KEY_NUMBER + " TEXT," + SMS_KEY_DATE + " TEXT," + SMS_KEY_TYPE + " TEXT," + TIME_STAMP + " DATETIME DEFAULT CURRENT_TIMESTAMP" + ")";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * main part of database handler
     * @param db
     */
    // creating tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_CALLHISTORY_TABLE);
        db.execSQL(CREATE_LISTAPPS_TABLE);
        db.execSQL(CREATE_HISNBKMK_TABLE);
        db.execSQL(CREATE_PROCESSES_TABLE);
        db.execSQL(CREATE_ACTIVITY_TABLE);
        db.execSQL(CREATE_LOCATION_TABLE);
        db.execSQL(CREATE_SMS_TABLE);
    }

    // upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CALLHISTORY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LISTAPPS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HISNBKMK);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROCESSES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACTIVITY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SMS);
        // create tables again
        onCreate(db);
    }

    // naive way to cleanup database
    public void overwriteTable(String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + name);

        if(name == "callHistory") db.execSQL(CREATE_CALLHISTORY_TABLE);
        if(name == "listApps") db.execSQL(CREATE_LISTAPPS_TABLE);
        if(name == "historyAndBookmarks") db.execSQL(CREATE_HISNBKMK_TABLE);
        if(name == "processes") db.execSQL(CREATE_PROCESSES_TABLE);
        if(name == "activity") db.execSQL(CREATE_ACTIVITY_TABLE);
        if(name == "location") db.execSQL(CREATE_LOCATION_TABLE);
        if(name == "sms") db.execSQL(CREATE_SMS_TABLE);
    }

    /**
     * callHistory table
     * All CRUD(Create, Read, Update, Delete) Operations
     */
    // adding new call history
    void addCallHistory(CallHistory callHistory) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
//        values.put(CALLHISTORY_KEY_ID, callHistory.getId());
        values.put(CALLHISTORY_KEY_NAME, callHistory.getName());
        values.put(CALLHISTORY_KEY_PH_NO, callHistory.getPhoneNumber());
        values.put(CALLHISTORY_KEY_DURATION, callHistory.getDuration());
        values.put(CALLHISTORY_KEY_DATE, callHistory.getDate());
        values.put(CALLHISTORY_KEY_TYPE, callHistory.getType());
        //values.put(KEY_TIME_STAMP, System.currentTimeMillis()); //maybe

        // inserting row
        db.insert(TABLE_CALLHISTORY, null, values);
        db.close();
    }

    // not sure what parameter we should use to get a single CallHistory. id? Name?
    void getCallHistory(){}

    public List<CallHistory> getAllCallHistory(){
        List<CallHistory> callHistoryList = new ArrayList<CallHistory>();

        String selectQuery = "SELECT * FROM " + TABLE_CALLHISTORY;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                CallHistory callHistory = new CallHistory();
                callHistory.setId(Integer.parseInt(cursor.getString(0)));
                callHistory.setName(cursor.getString(1));
                callHistory.setPhoneNumber(cursor.getString(2));
                callHistory.setDuration(cursor.getString(3));
                callHistory.setDate(cursor.getString(4));
                callHistory.setType(cursor.getInt(5));
                callHistory.setCreatedAt(cursor.getString(6));

                // add to output list
                callHistoryList.add(callHistory);
            } while(cursor.moveToNext());
        }
        return callHistoryList;
    }

    /**
     * listApps table
     * All CRUD(Create, Read, Update, Delete) Operations
     */
    void addListApps(ListApps listApps) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(LISTAPPS_KEY_NAME, listApps.getName());
        values.put(LISTAPPS_KEY_SRCDIR, listApps.getSrcDir());
        values.put(LISTAPPS_KEY_LAUNACT, listApps.getLaunchAct());
        db.insert(TABLE_LISTAPPS, null, values);
        db.close();
    }

    public List<ListApps> getAllListApps() {
        List<ListApps> listAppsList = new ArrayList<ListApps>();
        String selectQuery = "SELECT * FROM " + TABLE_LISTAPPS;
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToNext()) {
            do {
                ListApps listApps = new ListApps();
                listApps.setId(Integer.parseInt(cursor.getString(0)));
                listApps.setName(cursor.getString(1));
                listApps.setSrcDir(cursor.getString(2));
                listApps.setLaunchAct(cursor.getString(3));
                listAppsList.add(listApps);
            } while(cursor.moveToNext());
        }
        return listAppsList;
    }

    /**
     * historyAndBookmarks table
     * @param historyAndBookmarks
     */
    void addHistoryAndBookmarks(HistoryAndBookmarks historyAndBookmarks) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(HISNBKMK_KEY_TITLE, historyAndBookmarks.getTitle());
        values.put(HISNBKMK_KEY_URL, historyAndBookmarks.getUrl());
        db.insert(TABLE_HISNBKMK, null, values);
        db.close();
    }

    public List<HistoryAndBookmarks> getAllHistoryAndBookmarks() {
        List<HistoryAndBookmarks> historyAndBookmarksList = new ArrayList<HistoryAndBookmarks>();
        String selectQuery = "SELECT * FROM " + TABLE_HISNBKMK;
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToNext()) {
            do {
                HistoryAndBookmarks historyAndBookmarks = new HistoryAndBookmarks();
                historyAndBookmarks.setId(Integer.parseInt(cursor.getString(0)));
                historyAndBookmarks.setTitle(cursor.getString(1));
                historyAndBookmarks.setUrl(cursor.getString(2));
                historyAndBookmarks.setCreatedAt(cursor.getString(3));
                historyAndBookmarksList.add(historyAndBookmarks);
            } while(cursor.moveToNext());
        }
        return historyAndBookmarksList;
    }

    /**
     * processes table
     * @param processes
     */
    void addProcesses(Processes processes) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(PROCESSES_KEY_PROCNAME, processes.getProcessName());
        values.put(PROCESSES_KEY_IMPORTANCE, processes.getImportance());
        db.insert(TABLE_PROCESSES, null, values);
        db.close();
    }

    public List<Processes> getAllProcesses() {
        List<Processes> processesList = new ArrayList<Processes>();
        String selectQuery = "SELECT * FROM " + TABLE_PROCESSES;
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToNext()) {
            do {
                Processes processes = new Processes();
                processes.setId(Integer.parseInt(cursor.getString(0)));
                processes.setProcessName(cursor.getString(1));
                processes.setImportance((Integer.parseInt(cursor.getString(2))));
                processes.setCreatedAt(Long.parseLong(cursor.getString(3)));
                processesList.add(processes);
            } while(cursor.moveToNext());
        }
        return processesList;
    }

    /**
     * activity table
     * @param activity
     */
    void addActivity(Activity activity) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ACTIVITY_KEY_CONFIDENCE, activity.getConfidence());
        values.put(ACTIVITY_KEY_TYPE, activity.getType());
        db.insert(TABLE_ACTIVITY, null, values);
        db.close();
    }

    public List<Activity> getAllActivity() {
        List<Activity> activityList = new ArrayList<Activity>();
        String selectQuery = "SELECT * FROM " + TABLE_ACTIVITY;
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToNext()) {
            do {
                Activity activity = new Activity();
                activity.setId(Integer.parseInt(cursor.getString(0)));
                activity.setConfidence(cursor.getString(1));
                activity.setType(cursor.getString(2));
                activity.setCreatedAt(cursor.getString(3));
                activityList.add(activity);
            } while(cursor.moveToNext());
        }
        return activityList;
    }

    /**
     * location table
     * @param location
     */
    void addLocation(Location location) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(LOCATION_KEY_LOCATIONSTR, location.getLocationString());
        try {
            db.insert(TABLE_LOCATION, null, values);
        }catch(Exception e){
            db.execSQL(CREATE_LOCATION_TABLE);
            db.insert(TABLE_LOCATION, null, values);
        }
        db.close();
    }

    public List<Location> getAllLocation() {
        List<Location> locationList = new ArrayList<Location>();
        SQLiteDatabase db = getWritableDatabase();
        String selectQuery;
        try {
            selectQuery = "SELECT * FROM " + TABLE_LOCATION;
        } catch(Exception e){
            db.execSQL(CREATE_LOCATION_TABLE);
            selectQuery = "SELECT * FROM " + TABLE_LOCATION;
        }

        Cursor cursor = db.rawQuery(selectQuery, null);


//        int i = 1;

        if (cursor.moveToNext()) {
            do {
                Location location = new Location();
                location.setId(Integer.parseInt(cursor.getString(0)));
                String str = cursor.getString(1); //I was doing something here for  a while
                location.setLocationString(str);
//                location.setId(i++);
                location.setCreatedAt(cursor.getString(2));
                locationList.add(location);
            } while(cursor.moveToNext());
        }
        return locationList;
    }

    void addSMS(SMS sms){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SMS_KEY_NUMBER, sms.getNumber());
        values.put(SMS_KEY_DATE, sms.getDate());
        values.put(SMS_KEY_TYPE, sms.getType());
        db.insert(TABLE_SMS, null, values);
        db.close();
    }

    public List<SMS> getAllSMS(){
        List<SMS> smsList = new ArrayList<SMS>();
        String selectQuery = "SELECT * FROM " + TABLE_SMS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToNext()) {
            do {
                SMS sms = new SMS();
                sms.setId(Integer.parseInt(cursor.getString(0)));
                sms.setNumber(cursor.getString(1));
                sms.setDate(cursor.getString(2));
                sms.setType(cursor.getString(3));
                smsList.add(sms);
            } while(cursor.moveToNext());
        }
        return smsList;
    }
}
