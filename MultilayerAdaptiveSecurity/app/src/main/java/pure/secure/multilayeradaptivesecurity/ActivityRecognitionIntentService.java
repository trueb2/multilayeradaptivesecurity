package pure.secure.multilayeradaptivesecurity;

import android.app.Activity;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by jwtrueb on 10/7/14.
 */
public class ActivityRecognitionIntentService extends IntentService {

    //dummy constructor
    public ActivityRecognitionIntentService(){
        super("ActivityRecognitionIntentService");
    }

    private DatabaseHandler mDatabaseHandler = new DatabaseHandler(this);
    /**
     * Called when a new activity detection update is available
     */
    @Override
    protected void onHandleIntent(Intent intent){
        //If the intent contains an update
        if(ActivityRecognitionResult.hasResult(intent)){
            //Get the update
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);

            DetectedActivity mostProbableActivity = result.getMostProbableActivity();

            //Get the confidence % (probability)
            int confidence = mostProbableActivity.getConfidence();

            //Get the type
            int activityType = mostProbableActivity.getType();

            pure.secure.multilayeradaptivesecurity.Activity activity = new pure.secure.multilayeradaptivesecurity.Activity(String.valueOf(confidence), String.valueOf(activityType));

//            mDatabaseHandler.addActivity(activity);

            //append to log
            File exportDir = Environment.getExternalStoragePublicDirectory("MyCVS");
            if (!exportDir.exists()) {
                exportDir.mkdirs();
            }

            File activityFile = new File(exportDir, "activity.csv");
            try {
                PrintWriter pw = new PrintWriter(new FileWriter(activityFile, true));
                String record = confidence + "," + activityType + "," + System.currentTimeMillis();
                pw.println(record);
                pw.close();

                String ns = Context.NOTIFICATION_SERVICE;
                NotificationManager mNotificationManager = (NotificationManager) getApplicationContext().getSystemService(ns);
                long when = System.currentTimeMillis();
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
                String date = sdf.format(new Date(when));

                Notification notification = new Notification(R.drawable.walking, "Logged at : " + date , when);
                notification.setLatestEventInfo(getApplicationContext(), "Recognition Service", "T: " + activityType + " C: " + confidence + " Logged: " + date, null);
                mNotificationManager.notify(113373, notification);
            } catch(Exception e) {
                e.printStackTrace();
                mDatabaseHandler.addActivity(activity);
            }
            Log.d(ActivityUtils.APPTAG, "Confidence: " + confidence + " Type: " +activityType);
        }
    }
}
