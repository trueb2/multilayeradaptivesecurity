package pure.secure.multilayeradaptivesecurity.locationquestions;

import java.util.ArrayList;


public class Sort {
	
	public static void mergesort(ArrayList<LocationData> array){
		mergesort(array, new ArrayList<LocationData>(array.size()), 0, array.size()-1);
	}
	
	private static void mergesort(ArrayList<LocationData> array, ArrayList<LocationData> sorted, int left, int right){
		if(left < right){
			int middle = (right + left)/2;
			mergesort(array, sorted, left, middle);
			mergesort(array, sorted, middle+1, right);
			merge(array, sorted, left, middle+1, right);
		}	
	}

	private static void merge(ArrayList<LocationData> a1, ArrayList<LocationData> sorted, int i1, int i2, int end2){
		int end1 = i2-1;
		int index = i1;
		int numElements = end2 - i1 + 1;
		
		while(i1 <= end1 && i2 <= end2)
		{
			if(a1.get(i1).compareTime(a1.get(i2)) < 0)
			{
				if(index != sorted.size())
				{
					sorted.set(index++, a1.get(i1++));
				}
				else
				{
					sorted.add(a1.get(i1++));
					index++;
				}
			}
			
			else
			{
				if(index != sorted.size())
				{
					sorted.set(index++, a1.get(i2++));
				}
				else
				{
					sorted.add(a1.get(i2++));
					index++;
				}
			}
		}
		
		while(i1 <= end1)
		{
			if(sorted.size() != index)
			{
				sorted.set(index++, a1.get(i1++));
			}
			else
			{
				sorted.add(a1.get(i1++));
				index++;
			}
		}
		
		while(i2 <= end2)
		{
			if(sorted.size() != index)
			{
				sorted.set(index++, a1.get(i2++));
			}
			else
			{
				sorted.add(a1.get(i2++));
				index++;
			}
		}
		
		for(int i = 0; i < numElements; i++)
			a1.set(i, sorted.get(i));
	}
	
	
	/**
	 * for sorting Integer ArrayLists
	 */
	public static void mergesortIntegers(ArrayList<Integer> array){
		mergesortIntegers(array, new ArrayList<Integer>(array.size()), 0, array.size()-1);
	}
	
	private static void mergesortIntegers(ArrayList<Integer> array, ArrayList<Integer> sorted, int left, int right){
		if(left < right){
			int middle = (right + left)/2;
			mergesortIntegers(array, sorted, left, middle);
			mergesortIntegers(array, sorted, middle+1, right);
			mergeIntegers(array, sorted, left, middle+1, right);
		}	
	}

	private static void mergeIntegers(ArrayList<Integer> a1, ArrayList<Integer> sorted, int i1, int i2, int end2){
		int end1 = i2-1;
		int index = i1;
		int numElements = end2 - i1 + 1;
		
		while(i1 <= end1 && i2 <= end2)
		{
			if(a1.get(i1).compareTo(a1.get(i2)) < 0)
			{
				if(index != sorted.size())
				{
					sorted.set(index++, a1.get(i1++));
				}
				else
				{
					sorted.add(a1.get(i1++));
					index++;
				}
			}
			
			else
			{
				if(index != sorted.size())
				{
					sorted.set(index++, a1.get(i2++));
				}
				else
				{
					sorted.add(a1.get(i2++));
					index++;
				}
			}
		}
		
		while(i1 <= end1)
		{
			if(sorted.size() != index)
			{
				sorted.set(index++, a1.get(i1++));
			}
			else
			{
				sorted.add(a1.get(i1++));
				index++;
			}
		}
		
		while(i2 <= end2)
		{
			if(sorted.size() != index)
			{
				sorted.set(index++, a1.get(i2++));
			}
			else
			{
				sorted.add(a1.get(i2++));
				index++;
			}
		}
		
		for(int i = 0; i < numElements; i++)
			a1.set(i, sorted.get(i));
	}
}
