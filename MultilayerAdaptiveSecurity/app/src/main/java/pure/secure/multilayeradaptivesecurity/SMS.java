package pure.secure.multilayeradaptivesecurity;

/**
 * Created by Qiuhua on 11/5/14.
 */
public class SMS {

    int id;
    String number;
    String date;
    String type;

    public SMS() {
    }

    public SMS(String number, String date, String type) {
        this.number = number;
        this.date = date;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
