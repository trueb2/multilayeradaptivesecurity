package pure.secure.multilayeradaptivesecurity.activityquestions;

import android.util.Log;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import pure.secure.multilayeradaptivesecurity.ActivityUtils;
import pure.secure.multilayeradaptivesecurity.Question;

/**
 * Created by jwtrueb on 11/25/14.
 */
public class AnalyzeAction {

    private static Calendar cal = Calendar.getInstance();

    public static ArrayList<Question> generateAllQuestions(int daysBack, int upto){
        try {
            long start = System.currentTimeMillis();
            Question question = whendYouWalkMost(daysBack, upto);
            long stop = System.currentTimeMillis();
            Log.d("Timer", "Walk Most: " + (stop - start));
            start = System.currentTimeMillis();
            Question question1 = whendYouDriveMost(daysBack, upto);
            stop = System.currentTimeMillis();
            Log.d("Timer", "Drive Most: " + (stop - start));

            ArrayList<Question> questions = new ArrayList<Question>();
            questions.add(question);
            questions.add(question1);

            return questions;
        }catch(FileNotFoundException e) {
            Log.d(ActivityUtils.APPTAG, "File Not Found");
            return null;
        }
    }

    /**
     * generates a Questions that asks when the user has driven the most
     * over the given period of time
     * @param time0 the number of days back to consider
     * @param time1 the number of days back to stop considering
     * @return Question when drove the most
     * @throws FileNotFoundException
     */
    public static Question whendYouDriveMost(int time0, int time1) throws FileNotFoundException {
        long start = System.currentTimeMillis();
        ArrayList<ConstantAction> cas = ActivityDatabase.getConstantActions();
        long stop = System.currentTimeMillis();
        Log.d("Timer", "Read in Constant Actions took: " + (stop - start));
        ArrayList<Integer> drive = actionsPerHour(cas,0); //driving

        int hour = 0; //hour with the greatest count
        int high = 0; //highest count so far
        int low = Integer.MAX_VALUE; //lowest count so far
        int lowHour = 0; //hour with the lowest count
        for(int j = 0; j < 24; j++)
        {
            int count = drive.get(j);
            if(count > high)
            {
                high = count;
                hour = j;
            }

            if(count < low)
            {
                low = count;
                lowHour = j;
            }
        }

        String query;
        if(time1 == 0)
            query = "During what hour of the day have you driven the most over the past " + time0 +" days?";
        else
            query = "What hour of the day did you drive the most during " + time0 + " and " + time1 + " day(s) ago?";
        String answer = hourto12(hour);

        ArrayList<String> options = loopForOptions(drive, answer, hour, lowHour);

        //now we have a query, an answer, and 4 options
        Collections.shuffle(options);

        Question question = new Question(query, answer, options);
        return question;
    }

    /**
     * generates a Questions that asks when the user has been on foot the
     * most with his or her phone over the given time period
     * @param time0 the number of days back to consider
     * @param time1 the number of days back to stop considering
     * @return Question when were you on foot the most
     * @throws FileNotFoundException
     */
    public static Question whendYouWalkMost(int time0, int time1) throws FileNotFoundException {
        long start = System.currentTimeMillis();
        ArrayList<ConstantAction> cas = ActivityDatabase.getConstantActions();
        long stop = System.currentTimeMillis();
        Log.d("Timer", "Read in Constant Actions took: " + (stop - start));
        ArrayList<Integer> foot = actionsPerHour(cas, 2); //onFoot
        ArrayList<Integer> walk = actionsPerHour(cas, 7);//walking

        for(int i = 0; i < 24; i++){
            walk.set(i, walk.get(i) + foot.get(i));
        }

        int hour = 0; //hour with the greatest count
        int high = 0; //highest count so far
        int low = Integer.MAX_VALUE; //lowest count so far
        int lowHour = 0; //hour with the lowest count
        for(int j = 0; j < 24; j++)
        {
            int count = walk.get(j);
            if(count > high)
            {
                high = count;
                hour = j;
            }

            if(count < low)
            {
                low = count;
                lowHour = j;
            }
        }

        String query;
        if(time1 == 0)
            query = "During what hour of the day have you walked the most over the past " + time0 +" days?";
        else
            query = "What hour of the day did you walk the most during " + time0 + " and " + time1 + " day(s) ago?";
        String answer = hourto12(hour);

        ArrayList<String> options = loopForOptions(walk, answer, hour, lowHour);

        //now we have a query, an answer, and 4 options
        Collections.shuffle(options);

        Question question = new Question(query, answer, options);
        return question;
    }

    /**
     * takes a number between 0 and 23 inclusive and returns a number between 1 and 12
     * inclusive followed by AM or PM
     * @param hour of day
     * @return string of 12 hour format
     */
    public static String hourto12(int hour){
        if(hour == 0)
            return "12 AM";
        else if(hour == 12)
            return "12 PM";
        else
            return hour % 12 + ((hour - 12 > 0) ? " PM" : " AM");
    }

    // does searching for good options that would normally just go in the question generating method but it was lengthy
    private static ArrayList<String> loopForOptions(ArrayList<Integer> walk, String answer, int hour, int lowHour){
        int low = Integer.MAX_VALUE;
        ArrayList<String> options = new ArrayList<String>();
        ArrayList<Integer> intOptions = new ArrayList<Integer>();
        intOptions.add(hour); //integer version of the options for comparison
        options.add(answer); //string version of the options that will be presented to the users
        //make sure that options are not too close together
        if(Math.abs(lowHour - hour) > 2){
            options.add(hourto12(lowHour));
            intOptions.add(lowHour);
        }

        int doubleCheck = 0;
        ArrayList<Boolean> considered = new ArrayList<Boolean>(); //hours that have not been checked
        for(int i = 0; i < 24; i++)
            considered.add(false);

        considered.set(hour, true);
        considered.set(lowHour, true);

        while(options.size() < 4){
            //loop through the hours to find the next lowest
            low = Integer.MAX_VALUE;
            lowHour = 0;
            for(int i = 0; i < 23; i++){
                int j = (int)(Math.random() * 1000) % 24;
                if(options.contains(hourto12(j)))
                    continue;

                if(considered.get(j))
                    continue;

                int count = walk.get(j);

                if(count < low)
                {
                    lowHour = i;
                    low = count;
                }
            }

            //check for any conflicts with previously found options
            boolean add = true;
            for(int j = 0; j < options.size(); j++){
                Integer i = intOptions.get(j);
                if(Math.abs(i - lowHour) <= 2){
                    add = false;
                }
            }

            if(add){
                intOptions.add(lowHour);
                options.add(hourto12(lowHour));
            }

            considered.set(lowHour, true);

            boolean stop = true;
            for(Boolean bool : considered)
                if(!bool)
                    stop = false;

            if(stop && doubleCheck++ > 150)
                return null; //all hours have been considered yet this has not moved on
        }

        return options;
    }

    public static ArrayList<Integer> actionsPerHour(ArrayList<ConstantAction> constantActions,int type){
        ArrayList<Integer> counts = new ArrayList<Integer>(24);
        for(int j = 0; j < 24; j++)
            counts.add(0);

        //total the actions per hour
        for(ConstantAction ca : constantActions){
            if(ca.type != type) //not the type that we are trying total
                continue;

            ArrayList<Boolean> hours = findHours(ca.actions);
            for(int i = 0; i < hours.size(); i++){
                boolean b = hours.get(i);
                if(b)
                    counts.set(i, counts.get(i)+1);//increment count
            }
        }
        return counts;
    }


    /**
     * the arraylist returned will have 24 indexes each representing an hour in the day
     * if the hour did not contain the action then its index will be set to false
     * if true then the action was ongoing during the hour
     * @param actions
     * @return actions ongoing hours
     */
    public static ArrayList<Boolean> findHours(ArrayList<Action> actions){
        //find out what hours are between the first and last timestamp
        long first = actions.get(0).timestamp;
        long last = actions.get(actions.size()-1).timestamp;

        cal.setTime(new Date(first));
        int hour0 = cal.get(Calendar.HOUR_OF_DAY);
        cal.setTime(new Date(last));
        int hour1 = cal.get(Calendar.HOUR_OF_DAY);
        ArrayList<Boolean> hours = new ArrayList<Boolean>(24);
        for(int i = 0; i < 24; i++){
            if(i < hour0)
                hours.add(false);
            else if(i >= hour0 && i <= hour1)
                hours.add(true);
            else if(i > hour1)
                hours.add(false);
        }

        return hours;
    }
}
