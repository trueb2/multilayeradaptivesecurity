package pure.secure.multilayeradaptivesecurity.activityquestions;

import java.util.ArrayList;

public class ConstantAction {

	long startstamp;
	long stopstamp;
	int type;
	int averageConfidence;
	int impurities;
	ArrayList<Action> actions;
	
	/**
	 * constructor that automatically stores the starting and stopping stamps
	 * the type
	 * and finds the average confidence
	 * @param actions
	 */
	public ConstantAction(ArrayList<Action> actions){
		startstamp = actions.get(0).timestamp;
		stopstamp = actions.get(actions.size()-1).timestamp;
		
		type = actions.get(0).type;
		
		int sum = 0;
		for(Action a : actions)
			sum += a.confidence;
		
		averageConfidence = sum / actions.size();
		
		this.actions = actions;
		
		impurities = percentageImpurities();

	}
	
	/**
	 * looks at the type that the first action in the constant action is
	 * and the number of times some other type of activity is included
	 * in the ConstantAction
	 * this does not take confidence into consideration
	 * 
	 * a low averageConfidence and a low percentageImpurities
	 * likely means that we cannot be sure of what has been logged
	 * @return double percentage of impurities
	 */
	public int percentageImpurities(){
		int count = 0;
		for(Action a : actions)
			if(a.type != type)
				count++;
		
		return (int) (((double)count/(double)actions.size()) * 100);
	}
	
	@Override
	/**
	 * String representation of constant actions
	 */
	public String toString(){
		String s = "Type : " + type + " Confidence : " + averageConfidence + " Percentage Impure : " + impurities;
		return s;
	}
}
