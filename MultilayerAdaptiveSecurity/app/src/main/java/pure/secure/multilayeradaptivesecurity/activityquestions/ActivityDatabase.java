package pure.secure.multilayeradaptivesecurity.activityquestions;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

import pure.secure.multilayeradaptivesecurity.locationquestions.Analyze;

public class ActivityDatabase {

	protected static ArrayList<Action> actions;
	protected static ArrayList<ConstantAction> constantActions;
	
	public ActivityDatabase(){
		actions = new ArrayList<Action>();
	}

    /**
     * Reads in all of the Actions that take place within a certain number
     * of days specified by daysBack
     * @param daysBack
     * @throws FileNotFoundException
     */
    public static void readAllAfter(int daysBack) throws FileNotFoundException {
        File exportDir = Environment.getExternalStoragePublicDirectory("MyCVS");
        Scanner in = new Scanner(new FileReader(exportDir.getAbsolutePath() + "/activity.csv"));
        long today = System.currentTimeMillis();
        long msBack = Analyze.DAY_IN_MILLIS * daysBack;
        if(actions == null)
            actions = new ArrayList<Action>();

        String line = in.nextLine();
        while(in.hasNextLine()) {
            int confidence;
            int type;
            long timestamp;
            String[] parts = line.split(",");
            timestamp = Long.parseLong(parts[2]);
            if(timestamp + msBack < today){
                line = in.nextLine();
                continue;
            }
            type = Integer.parseInt(parts[1]);
            confidence = Integer.parseInt(parts[0]);

            Action action = new Action(confidence, type, timestamp);
            actions.add(action);
            line = in.nextLine();
        }
    }

    /**
     * should not be used regularly
     * this reads in everything that there is in the database
     *
     * @throws FileNotFoundException
     */
	public static void readAll() throws FileNotFoundException{
        File exportDir = Environment.getExternalStoragePublicDirectory("MyCVS");
        Scanner in = new Scanner(new FileReader(exportDir.getAbsolutePath()+"/activity.csv"));
		if(actions == null)
			actions = new ArrayList<Action>();
		
		String line = in.nextLine();
		while(in.hasNextLine()){
			int confidence;
			int type;
			long timestamp;
			String[] parts = line.split(",");
			confidence = Integer.parseInt(parts[0]);
			type = Integer.parseInt(parts[1]);
			timestamp = Long.parseLong(parts[2]);
			
			Action action = new Action(confidence, type, timestamp);
			actions.add(action);
			line = in.nextLine();
		}
	}
	
	/**
	 * finds all sequences of repeated activities and creates a ConstantAction object
	 * for each
	 * Constant Actions may be long or very short. they should be cleaned or concatenated
	 * for further analysis
	 * @return
	 * @throws FileNotFoundException
	 */
	public static ArrayList<ConstantAction> findConstantActions() throws FileNotFoundException{
        long start = System.currentTimeMillis();
		if(actions == null)
			readAllAfter(10);
        long stop = System.currentTimeMillis();
        Log.d("Timer", "Read in Actions took: " + (stop - start));

		ArrayList<ConstantAction> constantActions = new ArrayList<ConstantAction>();
		Action previous, current = null;
		ArrayList<Action> sameActions = new ArrayList<Action>();
		for(int i = 0; i < actions.size(); i++){
			previous = current;
			current = actions.get(i);
			
			if(previous == null)
				continue;
			
			if(previous.equals(current)){
				sameActions.add(previous);
			}
			else if(!previous.equals(current) && sameActions.size() != 0){
				sameActions.add(previous);
				ConstantAction  constAction = new ConstantAction(sameActions);
				constantActions.add(constAction);
				sameActions = new ArrayList<Action>();
			}
		}
		
		return constantActions;
	}

	/**
	 * often just one or two readings may appear of a different type that would 
	 * have otherwise continued a constant action
	 * this will bridge these disconnected Constant Actions together
	 * @param constActions
	 * @return
	 */
	public static ArrayList<ConstantAction> concatenateConstantActions(ArrayList<ConstantAction> constActions){
		ArrayList<ConstantAction> concatenated = new ArrayList<ConstantAction>();
		for(int i = 2; i < constActions.size(); i++){
			ConstantAction twoback = constActions.get(i-2);
			ConstantAction oneback = constActions.get(i-1);
			ConstantAction current = constActions.get(i);
			
			//if the first and last match each other and the middle is very short concatenate
			if(twoback.type == current.type && oneback.actions.size() < 4){
				ArrayList<Action> c = new ArrayList<Action>(twoback.actions);
				c.addAll(oneback.actions);
				c.addAll(current.actions);
				concatenated.add(new ConstantAction(c));
			}
		}
		
		return concatenated;
	}
	
	/**
	 * returns an ArrayList<ConstantAction> that has been concatenated and 
	 * only includes ConstantActions with impurities that are below 25 percent and 
	 * average confidences that are abouve 60
	 */
	public static ArrayList<ConstantAction> cleanedAfterConcatenation(ArrayList<ConstantAction> constActions){
		ArrayList<ConstantAction> constantActions = new ArrayList<ConstantAction>();
		for(ConstantAction ca : constActions){
			if(ca.averageConfidence > 60 && ca.impurities < 25)
				constantActions.add(ca);
		}
		
		return constantActions;
	}
	
	/**
	 * returns the safe ConstantAction
	 * if they have not been found yet then the following will be called
	 * findConstantActions()
	 * concatenateConstantActions()
	 * cleanedAfterConcatenation()
	 * @throws FileNotFoundException 
	 */
	public static ArrayList<ConstantAction> getConstantActions() throws FileNotFoundException{
		if(constantActions == null)
			return cleanedAfterConcatenation(concatenateConstantActions(findConstantActions()));
		
		return constantActions;
	}
}
