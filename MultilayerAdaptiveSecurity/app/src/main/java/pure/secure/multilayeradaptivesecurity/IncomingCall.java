package pure.secure.multilayeradaptivesecurity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

/**
 * Created by jwtrueb on 10/9/14.
 */
public class IncomingCall extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent){
        TelephonyManager tmgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        PhoneStateListener psl = new PhoneStateListener(){
            public void onCallStateChanged(int state, String incomingNumber){
                Log.d(ActivityUtils.APPTAG, "State: " + state + " Number: " + incomingNumber);
                //TODO update Call History
                if(state == 1){
                    //ringing
                    Log.d(ActivityUtils.APPTAG, "Incoming Call");

                    //insert single call to log
                }
            }
        };

        tmgr.listen(psl, PhoneStateListener.LISTEN_CALL_STATE);
    }
}
