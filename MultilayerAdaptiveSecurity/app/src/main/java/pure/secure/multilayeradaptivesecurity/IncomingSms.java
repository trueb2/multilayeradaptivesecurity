package pure.secure.multilayeradaptivesecurity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

import java.util.ArrayList;

import pure.secure.multilayeradaptivesecurity.ActivityUtils;

/**
 * Created by jwtrueb on 10/9/14.
 */
public class IncomingSms extends BroadcastReceiver {

    private int count;
    private final SmsManager mSmsManager = SmsManager.getDefault();
//    private ArrayList<SmsMessage> messages; //size 10

    @Override
    public void onReceive(Context context, Intent intent){
        Bundle bundle = intent.getExtras();

        try{
            if(bundle != null){
                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for(int i = 0; i < pdusObj.length; i++){
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String senderNum = currentMessage.getDisplayOriginatingAddress();

                    String message = currentMessage.getDisplayMessageBody();

//                    Log.d(ActivityUtils.APPTAG, "Sender: " + senderNum + " Message: " + message);
                }
            }
        } catch(Exception e){
            Log.e(ActivityUtils.APPTAG, "IncomingSms threw an exception");
        }
    }

}
