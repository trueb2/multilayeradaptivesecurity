package pure.secure.multilayeradaptivesecurity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by jwtrueb on 11/6/14.
 * BroadcastReceiver for the BOOT_COMPLETED
 * signal
 * this starts the logging service on boot
 */
public class AutoStart extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent i){
        Intent intent = new Intent(context, LoggingService.class);
        context.startService(intent);
        Intent intent1 = new Intent(context, RecognitionService.class);
        context.startService(intent1);
        Log.d(ActivityUtils.APPTAG, "Received BOOT_COMPLETED signal and started LoggingService and RecognitionService");
    }
}
