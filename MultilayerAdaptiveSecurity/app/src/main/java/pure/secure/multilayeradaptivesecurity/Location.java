package pure.secure.multilayeradaptivesecurity;

import android.location.Address;
import android.util.Log;

/**
 * Created by Qiuhua on 10/22/14.
 */
public class Location {
    int id;
    String locationString;
    String address;
    String createdAt;
    double latitude;
    double longitude;

    public Location(){}

    public Location(double lat, double lng, String address, String createdAt) {
        latitude = lat;
        longitude = lng;
        this.address = address;
        this.createdAt = createdAt;
        locationString = latitude + "," + longitude + "," + address + "," + createdAt;
        Log.d(ActivityUtils.APPTAG, locationString);
    }

    public Location(String locationString) {
        this.locationString = locationString;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLocationString() {
        return locationString;
    }

    public void setLocationString(String locationString) {
        this.locationString = locationString;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public void setLatitude(double lat){
        latitude = lat;
    }

    public void setLongitude(double lng){
        longitude = lng;
    }

    public double getLongitude(){
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public String getAddress(){
        return address;
    }

    public void setAddress(String address){
        this.address = address;
    }
}
