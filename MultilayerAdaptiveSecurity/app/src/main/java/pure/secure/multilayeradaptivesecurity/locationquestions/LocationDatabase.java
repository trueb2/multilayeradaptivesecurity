package pure.secure.multilayeradaptivesecurity.locationquestions;

import android.os.Environment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeMap;

/**
 * Created by jwtrueb on 10/22/14.
 * This will be the medium between the data stored in CVS file
 * on the phone and the algorithms that need access to the information
 * to develop useful questions
 *
 * Functionality:
 * 1. Read all Locations from file
 * 2. Has a string representation of the database
 *
 */

public class LocationDatabase {

    protected ArrayList<LocationData> places; //all logged locations
    protected ArrayList<LocationData> time; //sorted by visits
    protected TreeMap<Integer, LocationData> visits; //sorted by visits
    protected static HashMap<LocationData,String> addresses;

    /**
     * Parses the CSV file back into Location objects stored in a HashMap
     */
    public void readAll() throws FileNotFoundException {
        readAllAfter(Integer.MAX_VALUE);
    }
    /**
     * Parses the CSV file back into Location objects stored in a HashMap
     * if their timestamps are new enough
     */
    public void readAllAfter(int daysBack) throws FileNotFoundException {
        //now
        long now = System.currentTimeMillis();

        //get file
        File exportDir = Environment.getExternalStoragePublicDirectory("MyCVS");
        Scanner file = new Scanner(new FileReader(exportDir.getAbsolutePath()+"/location.csv"));
//        Scanner file = new Scanner(new FileReader("location.csv"));
        places = new ArrayList<LocationData>(20000);
        addresses = new HashMap<LocationData, String>();

        String line = file.nextLine(); // skipping first line
        while(file.hasNextLine()){
            line = file.nextLine();
            //id , lat, lng, timestamp, date
            String[] parts = line.split(",");
            int id=-1;
            double lat = -181, lng = -181;
            long timestamp = -1;
            String date = "";
            String address = "";

            try {
                for (int i = 0; i < 5; i++) { //parts.length
                    switch (i) {
                        //new way
                        case 0:
                            lat = Double.parseDouble(parts[0]);
                            break;
                        case 1:
                            lng = Double.parseDouble(parts[1]);
                            break;
                        case 2:
                            address = parts[2];
                            break;
                        case 3:
                            timestamp = Long.parseLong(parts[3]);
                            if(timestamp < now - daysBack*24*60*60*1000)
                                continue;
                        default:
                            break;
                    }
                }
            } catch(Exception e){
                continue; //log corruptions
            }

            LocationData loc = new LocationData(lat, lng, timestamp, address);

            places.add(loc);
            addresses.put(loc, loc.address);
        }
    }
    
    /**
     * sorts places by everything
     * @throws FileNotFoundException 
     */
    public void sortAll() throws FileNotFoundException{
    	if(places == null)
    		readAll();
    	sortByTime(places);
    	sortByVisits(places);
    }

    /**
     * sorts all of the places visited by the number of times that 
     * the location was visited
     * 
     * returns a TreeMap ordered by the number of visits to each location
     * @return
     */
    public TreeMap<Integer, LocationData> sortByVisits(ArrayList<LocationData> locs) {
    	TreeMap<Integer, LocationData> sorted = new TreeMap<Integer, LocationData>();
    	HashMap<LocationData, Integer> temp = new HashMap<LocationData, Integer>();

    	//put all locations in HashMap to easily count the number of visits
    	for(LocationData loc : locs){
    		Integer integer = temp.get(loc);
    		
    		if(integer == null){
    			integer = new Integer(1);
    		}
    		
    		temp.put(loc, integer+1);
    	}
    	
    	//move all locations from HashMap into TreeMap where they will be ordered
    	for(LocationData loc : temp.keySet()){
    		sorted.put(temp.get(loc), loc);
    	}
    	
    	visits = sorted;
    	return sorted;
    }
    
    /**
     * sorts all of the places visited by the time that each location
     * was visited
     * 
     * returns an ArrayList in the order that the locations were visited
     */
    public ArrayList<LocationData> sortByTime(ArrayList<LocationData> locs){
    	try{
    		if(locs == null)
    			readAll();
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	
    	@SuppressWarnings("unchecked") //no reason to check this
		ArrayList<LocationData> sorted = (ArrayList<LocationData>) locs.clone();
    	Sort.mergesort(sorted); //sorts sorted using mergesort
    	time = sorted;
    	return sorted;
    }
    
    /**
	 * returns several different ways to look at the dataset in a string format
	 */
	public String toString(){
		if(places == null){
			try {
				readAll();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} //make sure that everything has been initialized properly
		}
		
	    StringBuffer sb = new StringBuffer();
	    
	    //sorted by number of visits and time
	    sb.append("\nSorted by timestamp\n");
	    sb.append(toStringTime(places));
	    sb.append("Sorted by number of visits\n");
	    sb.append(toStringVisits(places));
	    
	    return sb.toString();
	}
	
	/**
	 * returns serveral different ways to look at the dataset in a string format
	 * without looking at the entire dataset
	 */
	public String toString(ArrayList<LocationData> locs){
		if(locs == null)
			return null;
		
		StringBuffer sb = new StringBuffer();
		
		//sorted by number of visits and time
		sb.append("Sorted by number of visits\n");
		sb.append(toStringVisits(locs));
		sb.append("\nSorted by timestamp\n");
		sb.append(toStringTime(locs));
		
		return sb.toString();
	}

	/**
     * Returns a string representation of the locations sorted by time
     * if the locations have not already been sorted then they will be
     * sorted
     */
    public String toStringTime(ArrayList<LocationData> locs){
		StringBuffer sb = new StringBuffer();
		
		if(locs == null)
			return "There were no places in the list";
		
		//if not already sorted, sort; otherwise use what has already been sorted
		if (time == null || locs != places)
			time = sortByTime(locs);

		for (LocationData loc : time) {
			String stime = convertStamp(loc.getTimestamp());
			sb.append("Location " + loc + " was visited at " + stime + "\n");
		}

		sb.append("Total Locations Visited : " + time.size());
		
		return sb.toString();
	}
    
    /**
     * Returns a string representation of the locations sorted by
     * the number of visits to each locations
     * if the locations have not already been sorted by such criteria
     * then they will be sorted
     */
    public String toStringVisits(ArrayList<LocationData> locs){
    	StringBuffer sb = new StringBuffer();
    	
		if(locs == null)
			return "There were no places in the list";
    	
    	//if the locations have not been sorted then sort them
    	if(visits == null || locs != places)
    		visits = sortByVisits(locs);
    	
        Iterator<Integer> itr = visits.keySet().iterator();
        int sum = 0;
        for(Integer i : visits.keySet()){
            LocationData loc = visits.get(i);
            sum += i;
            sb.append("Location " + loc + " was visited " + i + " time(s).   \t" +loc.getAddress()+"\n");
        }
        
        sb.append("That is " + sum + " visits.\n");
        
        return sb.toString();
    }
    
    

    /**
     * returns a human readable version of the timestamp
     */
    protected static String convertStamp(long stamp){
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
        String date = sdf.format(new Date(stamp));
        return date;
    }

	/**
	 * returns the day of the week given a timestamp
	 * @param timestamp
	 * @return hour
	 */
	protected static int dayOfWeekByStamp(long timestamp){
		Calendar c = Calendar.getInstance();
		c.setTime(new Date(timestamp));
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		return dayOfWeek;
	}
	
	/**
	 * returns the hour of the day given a timestamp
	 * @param timestamp
	 * @return hour
	 */
	protected static int hourOfDayByStamp(long timestamp){
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date(timestamp));
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		return hour;
	}
	
    /**
     * returns places
     */
    public ArrayList<LocationData> getPlaces(){
    	return places;
    }
    
    /**
     * replaces null addresses in csv file with an SQL safe version
     * of the actual address
     */
    protected boolean assignAddresses(){
    	PrintWriter pw = null;
    	boolean nonull = true;
    	try{
    		pw = new PrintWriter(new FileWriter("location.csv"));
    	} catch(Exception e){
    		return false;
    	}
    	
    	for(LocationData loc : places){
    		String address = loc.address;
    		if(address == null)
    			address = LocationData.getRoadName(loc.lat+"", loc.lng+"");
    		
    		if(address == null){
    			nonull = false;
    			continue;
    		}
    		
    		address = address .replaceAll(",", " |");
    		String s = loc.lat + "," + loc.lng + "," + address + "," + loc.timestamp;
    		pw.println(s);
    	}
    	
    	pw.close();
    	return nonull;
    }
}