package pure.secure.multilayeradaptivesecurity.locationquestions;

import java.util.ArrayList;
import java.util.Calendar;


public class TripDatabase {

	/**
	 * TripDatabase will contain Trip history so that 
	 * trips from previous days and weeks may be compared to 
	 * current trips in order to establish new trends and outliers
	 */
	
	//separates all trips taken into the days of the week
	private ArrayList<ArrayList<Trip>> week;
	private Calendar cal;
	
	/**
	 * Constructor initializes the ArrayLists that represent
	 * the week
	 */
	public TripDatabase(){
		week = new ArrayList<ArrayList<Trip>>(7); //there are exactly 7 days in a week
		week.add(new ArrayList<Trip>()); //monday
		week.add(new ArrayList<Trip>()); //tuesday
		week.add(new ArrayList<Trip>()); //wednesday
		week.add(new ArrayList<Trip>()); //thursday
		week.add(new ArrayList<Trip>()); //friday
		week.add(new ArrayList<Trip>()); //saturday
		week.add(new ArrayList<Trip>()); //sunday
		cal = Calendar.getInstance();
	}
	
	/**
	 * automatically adds the additional trips to the correct 
	 * day of the week
	 * @param moreTrips
	 */
	public void addTrips(ArrayList<Trip> moreTrips){
		switch(cal.get(Calendar.DAY_OF_WEEK)){
			case Calendar.MONDAY: addMondayTrips(moreTrips);
				break;
			case Calendar.TUESDAY: addTuesdayTrips(moreTrips);
				break;
			case Calendar.WEDNESDAY: addWednesdayTrips(moreTrips);
				break;
			case Calendar.THURSDAY: addThursdayTrips(moreTrips);
				break;
			case Calendar.FRIDAY: addFridayTrips(moreTrips);
				break;
			case Calendar.SATURDAY: addSaturdayTrips(moreTrips);
				break;
			case Calendar.SUNDAY: addSundayTrips(moreTrips);
				break;
			default: break;
		}
	}
	
	/**
	 * Concatenates the already present Monday trips with
	 * the additional trips passed in moreTrips
	 * @param moreTrips
	 */
	public void addMondayTrips(ArrayList<Trip> moreTrips){
		ArrayList<Trip> monday = week.get(0);
		monday.addAll(moreTrips);
	}
	
	/**
	 * Concatenates the already present Tuesday trips with
	 * the additional trips passed in moreTrips
	 * @param moreTrips
	 */
	public void addTuesdayTrips(ArrayList<Trip> moreTrips){
		ArrayList<Trip> tuesday = week.get(1);
		tuesday.addAll(moreTrips);
	}
	
	/**
	 * Concatenates the already present Wednesday trips with 
	 * the additional trips passed in moreTrips
	 * @param moreTrips
	 */
	public void addWednesdayTrips(ArrayList<Trip> moreTrips){
		ArrayList<Trip> wednesday = week.get(2);
		wednesday.addAll(moreTrips);
	}
	
	/**
	 * Concatenates the already present Thursday trips with
	 * the additional trips passed in moreTrips
	 * @param moreTrips
	 */
	public void addThursdayTrips(ArrayList<Trip> moreTrips){
		ArrayList<Trip> thursday = week.get(3);
		thursday.addAll(moreTrips);
	}
	
	/**
	 * Concatenates the already present Friday trips with
	 * the additional trips passed in moreTrips
	 * @param moreTrips
	 */
	public void addFridayTrips(ArrayList<Trip> moreTrips){
		ArrayList<Trip> friday = week.get(4);
		friday.addAll(moreTrips);
	}
	
	/**
	 * Concatenates the already present Saturday trips with
	 * the additional trips passed in moreTrips
	 * @param moreTrips
	 */
	public void addSaturdayTrips(ArrayList<Trip> moreTrips){
		ArrayList<Trip> saturday = week.get(5);
		saturday.addAll(moreTrips);
	}
	
	/**
	 * Concatenates the already present Sunday trips with
	 * the additional trips passed in moreTrips
	 * @param moreTrips
	 */
	public void addSundayTrips(ArrayList<Trip> moreTrips){
		ArrayList<Trip> sunday = week.get(6);
		sunday.addAll(moreTrips);
	}
}
