package pure.secure.multilayeradaptivesecurity;

import android.util.Log;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import pure.secure.multilayeradaptivesecurity.activityquestions.AnalyzeAction;
import pure.secure.multilayeradaptivesecurity.locationquestions.Analyze;

/**
 * Created by jwtrueb on 11/24/14.
 */
public class QuestionHandler {


    ArrayList<Question> locationQuestions, callQuestions,
            activityQuestions, smsQuestions, browserQuestions;

    public QuestionHandler(){
        locationQuestions = new ArrayList<Question>();
        callQuestions = new ArrayList<Question>();
        activityQuestions = new ArrayList<Question>();
        smsQuestions = new ArrayList<Question>();
        browserQuestions = new ArrayList<Question>();
    }

    public boolean generateLocationQuestions(){
        long today = System.currentTimeMillis();
        long yesterday = today - 24*60*60*1000;
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(yesterday));
        int day  = cal.get(Calendar.DAY_OF_WEEK);
        long daybeforeyesterday = yesterday -24*60*60*1000;
        cal.setTime(new Date(daybeforeyesterday));
        int day1 = cal.get(Calendar.DAY_OF_WEEK);

        try {
            long start = System.currentTimeMillis();
             locationQuestions = Analyze.generateAllQuestions(10, 0, day, day1);
            long stop = System.currentTimeMillis();
            Log.d("Timer", "Location Questions took: " + (stop - start));
        } catch (FileNotFoundException e){
            Log.d(ActivityUtils.APPTAG, "File Not Found");
            return false;
        }
        return true;
    }

    public boolean generateActivityQuestions() {
        long start = System.currentTimeMillis();
        activityQuestions = AnalyzeAction.generateAllQuestions(10, 0);
        long stop = System.currentTimeMillis();
        Log.d("Timer", "Activity Questions took: " + (stop - start));
        return activityQuestions != null;
    }

    //TODO Make it so that call questions compile
    public boolean generateCallQuestion(){
//        callQuestions = analyzeCallHistory.generateCallQuestion();
//        return callQuestions != null;
        return false;
    }

    public ArrayList<Question> getLocationQuestions() {
        return locationQuestions;
    }

    public ArrayList<Question> getCallQuestions() {
        return callQuestions;
    }

    public ArrayList<Question> getActivityQuestions() {
        return activityQuestions;
    }

    public ArrayList<Question> getSmsQuestions() {
        return smsQuestions;
    }

    public ArrayList<Question> getBrowserQuestions() {
        return browserQuestions;
    }
}
