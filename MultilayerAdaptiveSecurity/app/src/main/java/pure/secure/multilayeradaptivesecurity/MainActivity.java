package pure.secure.multilayeradaptivesecurity;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Browser;
import android.provider.CallLog;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationServices;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;

import pure.secure.multilayeradaptivesecurity.locationquestions.LocationData;

public class MainActivity extends FragmentActivity implements
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    /* A fragment to display an error dialog */
    public static class ErrorDialogFragment extends DialogFragment {
        private Dialog mDialog;

        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }

        public void setDialog(Dialog dialog){
            mDialog = dialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            if(mDialog == null)
                super.setShowsDialog(false);

            return mDialog;
        }
    }


    private GoogleApiClient mGoogleApiClient;
    private LocationClient mLocationClient;
    private DatabaseHandler mDatabaseHandler;
    private AlertDialog mAlertDialog;
    private QuestionHandler mQuestionHandler;
    private String mAnswer;
    private ArrayList<String> mOptions;
    private DetermineActivity mDetermineActivity;
    private int scratch;
    private ArrayList<Question> mQuestions;

    private boolean mResolvingError; //for connecting to play services
    private static final int REQUEST_RESOLVE_ERROR = 1001; // Request code to use when launching the resolution activity
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private static final String DIALOG_ERROR = "dialog_error"; // Unique tag for the error dialog fragment
    private static final String STATE_RESOLVING_ERROR = "resolving_error";



    @Override
    protected void onStart(){
        super.onStart();
        if(!mResolvingError){
            mGoogleApiClient.connect();
        }
        mLocationClient.connect();

//        mDetermineActivity = new DetermineActivity(this); //needs to call its connect method
    }

    @Override
    protected void onStop(){
        mGoogleApiClient.disconnect();
        mLocationClient.disconnect();
        super.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(STATE_RESOLVING_ERROR, mResolvingError);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.symposium);

        mResolvingError = savedInstanceState != null && savedInstanceState.getBoolean(STATE_RESOLVING_ERROR, false);

        mLocationClient = new LocationClient(this, this, this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mDatabaseHandler = new DatabaseHandler(this);

        startAService(null); //start the services
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        // Connected to Google Play services!
        // The good stuff goes here.
        Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
        mResolvingError = false;

    }

    @Override
    public void onDisconnected() {
        // Display the connection status
        Toast.makeText(this, "Disconnected. Please re-connect.",
                Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onConnectionSuspended(int cause) {
        // The connection has been interrupted.
        // Disable any UI components that depend on Google APIs
        // until onConnected() is called.
        mResolvingError = true;
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // This callback is important for handling errors that
        // may occur while attempting to connect with Google.
        //
        //  if you run this code, there's a good chance it will
        // fail and your app will receive a call to onConnectionFailed()
        // with the SIGN_IN_REQUIRED error because the user account
        // has not been specified.
        if (mResolvingError) {
            // Already attempting to resolve an error.
            return;
        } else if (result.hasResolution()) {
            try {
                mResolvingError = true;
                result.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
            } catch (SendIntentException e) {
                // There was an error with the resolution intent. Try again.
                mGoogleApiClient.connect();
            }
        } else {
            // Show dialog using GooglePlayServicesUtil.getErrorDialog()
            showErrorDialog(result.getErrorCode());
            mResolvingError = true;
        }

    }

    /* Creates a dialog for an error message */
    private void showErrorDialog(int errorCode) {
        // Create a fragment for the error dialog
        ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
        // Pass the error that should be displayed
        Bundle args = new Bundle();
        args.putInt(DIALOG_ERROR, errorCode);
        dialogFragment.setArguments(args);
        dialogFragment.show(getFragmentManager(), "errordialog"); //this is how it is supposed to be done
    }

    /* Called from ErrorDialogFragment when the dialog is dismissed. */
    public void onDialogDismissed() {
        mResolvingError = false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_RESOLVE_ERROR) {
            mResolvingError = false;
            if (resultCode == RESULT_OK) {
                // Make sure the app is not already connected or attempting to connect
                if (!mGoogleApiClient.isConnecting() &&
                        !mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.connect();
                }
            }
        }

        if(requestCode == CONNECTION_FAILURE_RESOLUTION_REQUEST){
            if(resultCode == Activity.RESULT_OK) {
                //try the request again
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    /**
     * Important: Because it is hard to anticipate the state of each device,
     * you must always check for a compatible Google Play services APK before
     * you access Google Play services features. For many apps, the best time
     * to check is during the onResume() method of the main activity.
     */
    public void onResume(){
        //Verifies that Google Play services is installed and enabled on this device,
        //and that the version installed on this device is no older than the one required by this client.
       if(GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext()) == ConnectionResult.SUCCESS){
           super.onResume();
       }
       super.onResume(); //does nothing right now but should do something in the future
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * The onclick attribute of the Log In button in activity_main.xml
     * uses this method whenever the button is clicked
     * @param v
     */
    public void logIn(View v){
        Intent i = new Intent(this,LoginActivity.class);
        startActivity(i);
    }

    /**
     * The onclick attribute of the Print Calls button in activity_main.xml
     * uses this method whenever the button is clicked
     *
     *
     * reference the following link for information about accessing call history:
     * http://developer.android.com/reference/android/provider/CallLog.Calls.html
     *
     * @param v
     */
    public void printCalls(View v) {
        Log.d("Print Calls", "Trying to print the call history");

        //straight off of StackOverflow for the most part
        Uri allCalls = Uri.parse("content://call_log/calls");
        Cursor c = managedQuery(allCalls, null, null, null, null); //Cursor is not synchronized >> Random order

        // cleanup table
        mDatabaseHandler.overwriteTable("callHistory");

        //throws errors if no entries
        while (c.moveToNext()) { //not sure how to trigger this or even if this is the best way of going through call history
            int id = Integer.parseInt(c.getString(c.getColumnIndex(CallLog.Calls._ID)));
            String num = c.getString(c.getColumnIndex(CallLog.Calls.NUMBER));// for  number
            String name = c.getString(c.getColumnIndex(CallLog.Calls.CACHED_NAME));// for name
            String duration = c.getString(c.getColumnIndex(CallLog.Calls.DURATION));// for duration in milliseconds since the epoch
            String date = c.getString(c.getColumnIndex(CallLog.Calls.DATE));// for date called occurred
            int type = Integer.parseInt(c.getString(c.getColumnIndex(CallLog.Calls.TYPE)));// for call type, Incoming or out going

//            Log.d("Print Calls", "_id " + id);
//            Log.d("Print Calls", "num " + num);
//            Log.d("Print Calls", "name " + name);
//            Log.d("Print Calls", "duration " + duration);
//            Log.d("Print Calls", "date " + date);
//            Log.d("Print Calls", "type " + type);
//            Toast.makeText(this, "Number : "+ num, Toast.LENGTH_SHORT).show();

            //create CallHistory object and add to data base
            CallHistory ch = new CallHistory(id, name, num, duration, date, type);
            mDatabaseHandler.addCallHistory(ch);
        }

        // test printing call history using db.getAllCallHistory
        Log.d("Reading: ", "Reading all call history..");
        List<CallHistory> callHistory = mDatabaseHandler.getAllCallHistory();
        for (CallHistory ch : callHistory) {
            String log = "Id: "+ch.getId()+" ,Name: "+ch.getName() + " ,Phone: "+ch.getPhoneNumber()
                    +" ,Duration: "+ch.getDuration()+" ,Date: "+ch.getDate()+" ,Type: "+ch.getType()
                    +", Timestamp: " + ch.getCreatedAt();
            Log.d("Name: ", log);
        }

    }

    public void printSMSes(View v){
        Log.d("Print SMSes", "Trying to print SMS history");
        Uri uri = Uri.parse("content://sms");
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);

        // cleanup old version of table
        mDatabaseHandler.overwriteTable("sms");

        if(cursor.moveToFirst()) {
            for(int i=0; i<cursor.getCount(); i++){
                String number = cursor.getString(cursor.getColumnIndexOrThrow("address"));
                String date = cursor.getString(cursor.getColumnIndexOrThrow("date"));
                Date smsDayTime = new Date(Long.valueOf(date));
                String type = cursor.getString(cursor.getColumnIndexOrThrow("type"));
                String typeOfSMS = null;
                switch (Integer.parseInt(type)) {
                    case 1:
                        typeOfSMS = "INBOX";
                        break;

                    case 2:
                        typeOfSMS = "SENT";
                        break;

                    case 3:
                        typeOfSMS = "DRAFT";
                        break;
                }
                Log.d("Print SMSes", "Phone Number: " + number);
                Log.d("Print SMSes", "Date: " + smsDayTime);
                Log.d("Print SMSes", "Type: " + typeOfSMS);

                // create SMS and insert it into database
                SMS sms = new SMS(number, smsDayTime.toString(), typeOfSMS);
                mDatabaseHandler.addSMS(sms);

                cursor.moveToNext();
            }
        }
        cursor.close();
    }

    /**
     * The onclick attribute of the Get Your Location button in activity_main.xml
     * uses this method whenever the button is clicked
     *
     * uses the Google Play Location Services to determine location
     * check out http://developer.android.com/google/play-services/setup.html  and
     * https://developer.android.com/training/location/retrieve-current.html for more info
     * about how to setup the library
     * @param v
     */
    public void getLocation(View v){
        if(mLocationClient.isConnected() && GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext()) == ConnectionResult.SUCCESS) {
            final Location mCurrentLocation = mLocationClient.getLastLocation();
            final String strLoc = mCurrentLocation.toString();
            //may not have location
            if(mCurrentLocation != null) {
                Thread t = new Thread(new Runnable(){
                    public void run(){

                        Log.d("Check Location", "Location " + strLoc);

                        // insert into datointabase
                        Double lat = mCurrentLocation.getLatitude();
                        Double lng = mCurrentLocation.getLongitude();
                        String address = LocationData.getRoadName(lat.toString(), lng.toString());
                        pure.secure.multilayeradaptivesecurity.Location loc = new pure.secure.multilayeradaptivesecurity.Location(lat, lng, address, System.currentTimeMillis()+"");
                        mDatabaseHandler.addLocation(loc);
                        Log.d(ActivityUtils.APPTAG, "Just Added Location to Database");
                    }
                });

                t.start();
                Toast.makeText(this, "Location " + strLoc, Toast.LENGTH_LONG).show();

            } else {
                Log.d(ActivityUtils.APPTAG, "Current Location is null");
            }
            return;
        }

        Toast.makeText(this, "You are not connected to Google Play Services",  Toast.LENGTH_LONG).show();
        Log.d("Check Location", "Method called");
    }

    /**
     * The onclick atribute of the List Installed Apps button in activity_main.xml
     * uses this method whenever the button is clicked
     *
     * This technique is pulled from :
     * http://stackoverflow.com/questions/2695746/how-to-get-a-list-of-installed-android-applications-and-pick-one-to-run
     */
    public void listApps(View v){
        final PackageManager pm = getPackageManager();
        //get a list of installed apps.
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);

        TreeSet<String> appName = new TreeSet<String>();

        //cleanup table
        mDatabaseHandler.overwriteTable("listApps");

        for (ApplicationInfo packageInfo : packages) {
            appName.add(packageInfo.packageName);
            Log.d("Listing Apps", "Installed package :" + packageInfo.packageName);
            Log.d("Listing Apps", "Source dir : " + packageInfo.sourceDir);
            Log.d("Listing Apps", "Launch Activity :" + pm.getLaunchIntentForPackage(packageInfo.packageName));

            ListApps listApps = new ListApps(packageInfo.packageName,packageInfo.sourceDir,null);
                    //(pm.getLaunchIntentForPackage(packageInfo.packageName)).toString());
            mDatabaseHandler.addListApps(listApps);
        }

        for(String name : appName)
            Log.d("Listing Apps", name);
    }

    /**
     * The onclick attribute of the Put Me on the Map button in activity_main.xml
     * uses this method whenever the button is clicked
     *
     * The set up followed here follows along with the one that can be found at
     * http://mobiforge.com/design-development/using-google-maps-android
     */
    public void mapMe(View v){
        Toast.makeText(this, "In the future this will take the Location from Google Play Services and use geocoding to find out where the Location is.", Toast.LENGTH_LONG);
    }

    /**
     * The onclick attribute of What Am I Doing button in activity_main.xml
     * uses this method whenever the button is clicked
     */
    public void detActivity(View v) {
        Intent intent = new Intent(this, DetermineActivity.class);
        startActivity(intent);
    }

    /**
     * check whether there is a writable external storage or not
     * @return
     */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /**
     * naive implementation of export database
     * ideally, I would prefer using a seperate class, but need to handle global access to database
     * @param v
     */
    public boolean exportDatabase(View v) {
        Log.d("Start Export", "Starting to export the database");
        if (!isExternalStorageWritable()) {
            return false;
        } else {
            File exportDir = Environment.getExternalStoragePublicDirectory("MyCVS");
            if (!exportDir.exists()) {
                exportDir.mkdirs();
            }
            File callHistoryFile, listAppsFile, historyAndBookmarksFile, processesFile,
                    activityFile, locationFile, smsFile;
            PrintWriter printWriter = null;
            try
            {
                /**
                 * export callHistory table
                 */
                callHistoryFile = new File(exportDir, "callHistory.csv");
                if (callHistoryFile.exists()) callHistoryFile.delete();
                // clean up old data first
                List<CallHistory> callHistoryList = mDatabaseHandler.getAllCallHistory();
                printWriter = new PrintWriter(new FileWriter(callHistoryFile,false));
                printWriter.println("ID,NAME,PHONE,DURATION,DATE,TYPE,TIMESTAMP");
                for (CallHistory ch : callHistoryList) {
                    String record = ch.getId() + "," + ch.getName() + "," + ch.getPhoneNumber()
                            + "," + ch.getDuration() + "," + ch.getDate() + "," + ch.getType() + ","
                            + ch.getCreatedAt();
                    printWriter.println(record);
                }
                /**
                 * export listApps table
                 */
                listAppsFile = new File(exportDir, "listApps.csv");
                List<ListApps> listAppsList = mDatabaseHandler.getAllListApps();
                printWriter = new PrintWriter(new FileWriter(listAppsFile, false));
                printWriter.println("ID,NAME,SRCDIR,LAUNACT");
                for (ListApps la : listAppsList) {
                    String record = la.getId() + "," + la.getName() + "," + la.getSrcDir() +
                            "," + la.getLaunchAct();
                    printWriter.println(record);
                }
                /**
                 * export historyAndBookmarks table
                 */
                historyAndBookmarksFile = new File(exportDir, "historyAndBookmarks.csv");
                List<HistoryAndBookmarks> historyAndBookmarksList = mDatabaseHandler.getAllHistoryAndBookmarks();
                printWriter = new PrintWriter(new FileWriter(historyAndBookmarksFile, false));
                printWriter.println("ID,TITLE,URL,TIMESTAMP");
                for (HistoryAndBookmarks hnb : historyAndBookmarksList) {
                    String record = hnb.getId() + "," + hnb.getTitle() + "," + hnb.getUrl() +
                            "," + hnb.getCreatedAt();
                    printWriter.println(record);
                }
                /**
                 * export processes table
                 */
                processesFile = new File(exportDir, "processes.csv");
                List<Processes> processesList = mDatabaseHandler.getAllProcesses();
                printWriter = new PrintWriter(new FileWriter(processesFile, false));
                printWriter.println("ID,PROCNAME,IMPORTANCE,TIMESTAMP");
                for (Processes processes : processesList) {
//                    Log.d(ActivityUtils.APPTAG,processes.getProcessName());
                    String record = processes.getId() + "," + processes.getProcessName() + "," + processes.getImportance() +
                            "," + processes.getCreatedAt();
                    printWriter.println(record);
                }
                /**
                 * export activity table
                 */
                activityFile = new File(exportDir, "activity.csv");
                List<pure.secure.multilayeradaptivesecurity.Activity> activityList = mDatabaseHandler.getAllActivity();
                printWriter = new PrintWriter(new FileWriter(activityFile, false));
                printWriter.println("ID,CONFIDENCE,TYPE,TIMESTAMP");
                for (pure.secure.multilayeradaptivesecurity.Activity activity : activityList) {
                    String record = activity.getId() + "," + activity.getConfidence() + "," + activity.getType() +
                            "," + activity.getCreatedAt();
                    printWriter.println(record);
                }
                /**
                 * export location table
                 */
                locationFile = new File(exportDir, "location.csv");
                List<pure.secure.multilayeradaptivesecurity.Location> locationList = mDatabaseHandler.getAllLocation();
                printWriter = new PrintWriter(new FileWriter(locationFile, false));
                printWriter.println("ID,LOCATIONSTR,TIMESTAMP");
                for (pure.secure.multilayeradaptivesecurity.Location location : locationList) {
                    String record = location.getId() + "," + location.getLocationString() + "," + location.getCreatedAt();
                    printWriter.println(record);
                }
                /**
                 * export sms table
                 */
                smsFile = new File(exportDir, "sms.csv");
                List<SMS> smsList = mDatabaseHandler.getAllSMS();
                printWriter = new PrintWriter(new FileWriter(smsFile, false));
                printWriter.println("ID,NUMBER,DATE,TYPE");
                for(SMS sms : smsList){
                    String record = sms.getId() + "," + sms.getNumber() + "," + sms.getDate() + "," + sms.getType();
                    printWriter.println(record);
                }
            }
            catch (Exception exc) {
                return false;
            }
            finally {
                if (printWriter != null) printWriter.close();
            }
            return true;
        }
    }

    /**
     * prints out all of the running processes
     */
    public void printProcesses(View v){
        Log.d(ActivityUtils.APPTAG, "Trying to print processes");
        ActivityManager mActivityManager = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> processInfos = mActivityManager.getRunningAppProcesses();
        //could get list of services and tasks here too but I dont think that will be helpful

        // clean up previous table
        mDatabaseHandler.overwriteTable("processes");

        for(ActivityManager.RunningAppProcessInfo p : processInfos){
            int importance = p.importance;
//            String i = "";
//            switch(importance){
//                case RunningAppProcessInfo.IMPORTANCE_FOREGROUND:
//                    i = "Foreground";
//                    break;
//                case RunningAppProcessInfo.IMPORTANCE_VISIBLE:
//                    i = "Visible";
//                    break;
//                case RunningAppProcessInfo.IMPORTANCE_PERCEPTIBLE:
//                    i = "Perceptible";
//                    break;
//                case RunningAppProcessInfo.IMPORTANCE_BACKGROUND:
//                    i = "Background";
//                    break;
//                case RunningAppProcessInfo.IMPORTANCE_SERVICE:
//                    i = "Service";
//                    break;
//                case RunningAppProcessInfo.IMPORTANCE_EMPTY:
//                    i = "Empty";
//                    break;
//                default:
//                    i = "Unknown";
//                    break;
//            }

            Processes processes = new Processes(p.processName, importance);
            mDatabaseHandler.addProcesses(processes);

            Log.d(ActivityUtils.APPTAG, "Process: "+ p.processName + " " + importance);
        }
    }

    public void printHistoryAndBookmarks(View v){
        String[] proj = new String[] { Browser.BookmarkColumns.TITLE, Browser.BookmarkColumns.URL };
        String sel = Browser.BookmarkColumns.BOOKMARK + " = 0"; // 0 = history, 1 = bookmark
        Cursor mCur = getContentResolver().query(Browser.BOOKMARKS_URI, proj, sel, null, null);
        mCur.moveToFirst();
        @SuppressWarnings("unused")
        String title = "";
        @SuppressWarnings("unused")
        String url = "";
        String date = "";

        // clean up previous historyAndBookmarks table
        mDatabaseHandler.overwriteTable("historyAndBookmarks");

        if (mCur.moveToFirst() && mCur.getCount() > 0) {
            boolean cont = true;
            while (mCur.isAfterLast() == false && cont) {
                title = mCur.getString(mCur.getColumnIndex(Browser.BookmarkColumns.TITLE));
                url = mCur.getString(mCur.getColumnIndex(Browser.BookmarkColumns.URL));
//                date = mCur.getString(mCur.getColumnIndex(Browser.BookmarkColumns.DATE));
                // Do something with title and url

                // insert into database
                HistoryAndBookmarks hnb = new HistoryAndBookmarks(title,url);
                mDatabaseHandler.addHistoryAndBookmarks(hnb);

                Log.d(ActivityUtils.APPTAG, "Title: " + title + " URL: "+url + " Data: " + date);
                mCur.moveToNext();
            }
        }
    }

    public void startAService(View v){
        Log.d(ActivityUtils.APPTAG, "About to start a service");
        Intent intent = new Intent(this, LoggingService.class);
        this.startService(intent);
        Intent intent1 = new Intent(this, RecognitionService.class);
        this.startService(intent1);
    }


    public void questionDialog(View v){
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View questionLayout = layoutInflater.inflate(R.layout.question, null);
        mAlertDialog = new AlertDialog.Builder(this).setIcon(R.drawable.ic_launcher).setView(questionLayout).setCancelable(false).create();
        mAlertDialog.show();
    }

    public void locationQuestion(View v){
        if(mQuestionHandler == null)
            mQuestionHandler = new QuestionHandler();
        mQuestionHandler.generateLocationQuestions();
        ArrayList<Question> questions = mQuestionHandler.getLocationQuestions();
        mQuestions = questions;
        scratch = 0;

        Question question = questions.get(0);
        scratch++;

        askQuestion(question);
    }

    public void activityQuestion(View v){
        if(mQuestionHandler == null)
            mQuestionHandler = new QuestionHandler();
        mQuestionHandler.generateActivityQuestions();
        ArrayList<Question> questions = mQuestionHandler.getActivityQuestions();
        mQuestions = questions;
        scratch = 0;

        Question question = questions.get(0);
        scratch++;

        askQuestion(question);
    }

    public void callQuestion(View v){
        if(mQuestionHandler == null)
            mQuestionHandler = new QuestionHandler();
        mQuestionHandler.generateCallQuestion();
        ArrayList<Question> questions = mQuestionHandler.getCallQuestions();
        mQuestions = questions;
        scratch = 0;

        Question question = questions.get(0);
        scratch++;

        askQuestion(question);
    }

    public void selectedOption0(View v){
        mAlertDialog.cancel();

        //check to see if option0 is the answer
        if(mOptions.get(0).equals(mAnswer))
            announce("Option0 selected. Correct");
        else
            announce("Option0 selected. Incorrect");

        //ask every question
        if(scratch != mQuestions.size())
            askQuestion(mQuestions.get(scratch++));
    }

    public void selectedOption1(View v){
        mAlertDialog.cancel();

        //check to see if option1 is the answer
        if(mOptions.get(1).equals(mAnswer))
            announce("Option1 selected. Correct");
        else
            announce("Option1 selected. Incorrect");

        //ask every question
        if(scratch != mQuestions.size())
            askQuestion(mQuestions.get(scratch++));
    }

    public void selectedOption2(View v){
        mAlertDialog.cancel();

        //check to see if option1 is the answer
        if(mOptions.get(2).equals(mAnswer))
            announce("Option2 selected. Correct");
        else
            announce("Option2 selected. Incorrect");

        //ask every question
        if(scratch != mQuestions.size())
            askQuestion(mQuestions.get(scratch++));
    }

    public void selectedOption3(View v){
        mAlertDialog.cancel();

        //check to see if option1 is the answer
        if(mOptions.get(3).equals(mAnswer))
            announce("Option3 selected. Correct");
        else
            announce("Option3 selected. Incorrect");

        //ask every question
        if(scratch != mQuestions.size())
            askQuestion(mQuestions.get(scratch++));
    }

    public void askQuestion(Question q){
        if(!q.COMPLETE){
            if(scratch != mQuestions.size())
                askQuestion(mQuestions.get(scratch++));
            Log.d(ActivityUtils.APPTAG, "Incomplete Question:" + q.query);
            return;
        }

        //inlfate question dialog view
        LayoutInflater layInflater = LayoutInflater.from(this);
        View questionLayout = layInflater.inflate(R.layout.question, null);

        //set query
        TextView query = (TextView) questionLayout.findViewById(R.id.query);
        query.setText(q.query);

        ArrayList<String> options = q.options;
        Collections.shuffle(options);

        Log.d(ActivityUtils.APPTAG, "Answer : " + q.answer);
        //set options
        TextView opt0 = (TextView) questionLayout.findViewById(R.id.option0);
        opt0.setText(options.get(0));
        Log.d(ActivityUtils.APPTAG, options.get(0));
        TextView opt1 = (TextView) questionLayout.findViewById(R.id.option1);
        opt1.setText(options.get(1));
        Log.d(ActivityUtils.APPTAG, options.get(1));
        TextView opt2 = (TextView) questionLayout.findViewById(R.id.option2);
        opt2.setText(options.get(2));
        Log.d(ActivityUtils.APPTAG, options.get(2));
        TextView opt3 = (TextView) questionLayout.findViewById(R.id.option3);
        opt3.setText(options.get(3));
        Log.d(ActivityUtils.APPTAG, options.get(3));

        mAnswer = q.answer;
        mOptions = options;

        mAlertDialog = new AlertDialog.Builder(this).setView(questionLayout).setCancelable(false).create();
        mAlertDialog.show();
    }

    public void announce(String announcement){
        Log.d(ActivityUtils.APPTAG, announcement);
        Toast.makeText(this, announcement, Toast.LENGTH_LONG).show();
    }
}
