package uiuc.research.posttoserver;

import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.w3c.dom.Text;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void submitContent(View v)
    {
        AsyncTask post = new AsyncTask<Object, Void, Void>(){

            private String string;

            protected Void doInBackground(Object ... voids)
            {
                EditText et = (EditText) findViewById(R.id.editText);
                String[] split = et.getText().toString().split(" ");

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost("http://ttat-control.iti.illinois.edu:5900/webfiles/log.php");

                try {
                    ArrayList<NameValuePair> nameValuePairs = new ArrayList<>();
                    nameValuePairs.add(new BasicNameValuePair("qType", split[0]));
                    nameValuePairs.add(new BasicNameValuePair("correct", split[1]));
                    nameValuePairs.add(new BasicNameValuePair("inScreen", split[2]));
                    nameValuePairs.add(new BasicNameValuePair("timeAsked", split[3]));
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    HttpResponse response = httpClient.execute(httpPost);
                    Log.d("Response" , response.getStatusLine().toString());

                    string = "You just sent " + split[0] + " " + split[1] + " " + split[2] + " " + split[3] + " to the server to be logged";
                } catch (IndexOutOfBoundsException e) {
                    string = "Enter 4 space separated numbers next time";
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            protected void onPostExecute(Void v)
            {
                TextView tv = (TextView) findViewById(R.id.textView);
                tv.setText(string);
            }
        };
        post.execute();
    }
}
