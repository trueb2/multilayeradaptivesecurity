import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYSeries;


public class AnalyzeActions {
	
	private Calendar cal;
	
	public AnalyzeActions(){
		cal = Calendar.getInstance();
	}
		
	public static void main(String[] args) throws FileNotFoundException{
		ArrayList<ConstantAction> cas = ActivityDatabase.getConstantActions(0);
		ArrayList<ConstantAction> cas1 = ActivityDatabase.getConstantActions(1);
		for(ConstantAction ca : cas)
			System.out.println(ca);
		
		System.out.println("Size : " + cas.size());
		
		AnalyzeActions aa  = new AnalyzeActions();
		for(int i = 0; i < 8; i++)
			aa.graphConstantActions(i, cas, cas1);
		
		Question q = aa.whendYouWalkMost(20, 0);
		System.out.println(q);
	}
	
	public void graphConstantActions(int type, ArrayList<ConstantAction> constantActions, ArrayList<ConstantAction> constantActions1){
		String stringType;
		
		switch(type){
		case 0: stringType = "IN_VEHICLE";
		break;
		case 1: stringType = "ON_BICYCLE";
		break;
		case 2: stringType = "ON_FOOT";
		break;
		case 8: stringType = "RUNNING";
		break;
		case 3: stringType = "STILL";
		break;
		case 5: stringType = "TILTING";
		break;
		case 4: stringType = "UNKNOWN";
		break;
		case 7: stringType = "WALKING";
		break;
		default: stringType = "UNKNOWN"; System.out.println("NOOO");
		return;
		}
		
		//x axis is the time of day that the activity is occurring and 
		//y axis is the number of times that the activity occured
//		ArrayList<Integer> sums = actionsPerHour(constantActions, type);
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
//		XYSeries series = new XYSeries(stringType);
//		XYSeries series = new XYSeries("Jacob");
		ArrayList<Integer> actionsPerHour = actionsPerHour(constantActions, type);
		ArrayList<Integer> actionsPerHour1 = actionsPerHour(constantActions1, type);
		for(int i = 0; i < 24; i++){
//			dataset.addValue((Number)actionsPerHour.get(i), stringType+ " Action", i);
			dataset.addValue((Number)actionsPerHour.get(i), "Jacob", i);
			dataset.addValue((Number)actionsPerHour1.get(i), "Qiuhua", i);
		}
				
		
		
		JFreeChart chart = ChartFactory.createBarChart("Number of " + stringType + " Actions By Hour",
				"Hours of Day", "Number of Occurences", dataset);
		chart.setBackgroundPaint(Color.WHITE);
		CategoryPlot plot = (CategoryPlot) chart.getPlot();
		BarRenderer renderer = (BarRenderer) plot.getRenderer();
		renderer.setSeriesPaint(0, Color.BLUE);
		renderer.setSeriesPaint(1, new Color(244,127,36));
		renderer.setDrawBarOutline(false);
		renderer.setShadowVisible(false);
//		renderer.setMaximumBarWidth(0.5);
		renderer.setGradientPaintTransformer(null);
		renderer.setBarPainter(new StandardBarPainter());
		ChartFrame frame = new ChartFrame("Action By Hour", chart);
		frame.pack();
		frame.setVisible(true);
		
		try{
			ChartUtilities.saveChartAsPNG(new File("Action by Hour "+ stringType), chart, 600,480);
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public ArrayList<Integer> actionsPerHour(ArrayList<ConstantAction> constantActions,int type){
		ArrayList<Integer> counts = new ArrayList<Integer>(24);
		for(int j = 0; j < 24; j++)
			counts.add(0);
		
		//total the actions per hour
		for(ConstantAction ca : constantActions){
			if(ca.type != type) //not the type that we are trying total
				continue;
			
			ArrayList<Boolean> hours = findHours(ca.actions);
			for(int i = 0; i < hours.size(); i++){
				boolean b = hours.get(i);
				if(b)
					counts.set(i, counts.get(i)+1);//increment count
			}
		}
		return counts;
	}
	
	
	/**
	 * the arraylist returned will have 24 indexes each representing an hour in the day
	 * if the hour did not contain the action then its index will be set to false
	 * if true then the action was ongoing during the hour
	 * @param actions
	 * @return actions ongoing hours
	 */
	public ArrayList<Boolean> findHours(ArrayList<Action> actions){
		//find out what hours are between the first and last timestamp
		long first = actions.get(0).timestamp;
		long last = actions.get(actions.size()-1).timestamp;
		
		cal.setTime(new Date(first));
		int hour0 = cal.get(Calendar.HOUR_OF_DAY);
		cal.setTime(new Date(last));
		int hour1 = cal.get(Calendar.HOUR_OF_DAY);
		ArrayList<Boolean> hours = new ArrayList<Boolean>(24);
		for(int i = 0; i < 24; i++){
			if(i < hour0)
				hours.add(false);
			else if(i >= hour0 && i <= hour1)
				hours.add(true);
			else if(i > hour1)
				hours.add(false);	
		}
		
		return hours;
	}
	
	/**
	 * generates a Questions that asks when the user has been on foot the 
	 * most with his or her phone over the given time period 
	 * @param time0 the number of days back to consider
	 * @param time1 the number of days back to stop considering
	 * @return Question when were you on foot the most
	 * @throws FileNotFoundException 
	 */
	public Question whendYouWalkMost(int time0, int time1) throws FileNotFoundException{
		ArrayList<ConstantAction> cas = ActivityDatabase.getConstantActions(0);
		ArrayList<Integer> foot = actionsPerHour(cas, 2); //onFoot
		ArrayList<Integer> walk = actionsPerHour(cas, 7);//walking
		
		for(int i = 0; i < 24; i++){
			walk.set(i, walk.get(i) + foot.get(i));
		}
		
		int hour = 0; //hour with the greatest count
		int high = 0; //highest count so far
		int low = Integer.MAX_VALUE; //lowest count so far
		int lowHour = 0; //hour with the lowest count
		for(int j = 0; j < 24; j++)
		{
			int count = walk.get(j);
			if(count > high)
			{
				high = count;
				hour = j;
			}
			
			if(count < low)
			{
				low = count;
				lowHour = j;
			}
		}
		
		String query;
		if(time1 == 0)
			query = "During what hour of the day have you walked the most over the past " + time0 +" days?";
		else
			query = "What hour of the day did you walk the most during " + time0 + " and " + time1 + " day(s) ago?";
		String answer = hourto12(hour);
		ArrayList<String> options = new ArrayList<String>();
		ArrayList<Integer> intOptions = new ArrayList<Integer>();
		intOptions.add(hour); //integer version of the options for comparison
		options.add(answer); //string version of the options that will be presented to the users
		//make sure that options are not too close together
		if(Math.abs(lowHour - hour) > 2){
			options.add(hourto12(lowHour));
			intOptions.add(lowHour);
		}
		
		int doubleCheck = 0;
		ArrayList<Boolean> considered = new ArrayList<Boolean>(); //hours that have not been checked
		for(int i = 0; i < 24; i++)
			considered.add(false);
		
		considered.set(hour, true);
		considered.set(lowHour, true);
		
		while(options.size() < 4){
			//loop through the hours to find the next lowest
			low = Integer.MAX_VALUE;
			lowHour = 0;
			for(int i = 0; i < 23; i++){
				int j = (int)(Math.random() * 1000) % 24;
				if(options.contains(hourto12(j)))
					continue;
				
				if(considered.get(j))
					continue;
				
				int count = walk.get(j);
				
				if(count < low)
				{
					lowHour = i;
					low = count;
				}
			}
			
			//check for any conflicts with previously found options
			boolean add = true;
			for(int j = 0; j < options.size(); j++){
				Integer i = intOptions.get(j);
				if(Math.abs(i - lowHour) <= 2){
					add = false;
				}
			}
			
			if(add){
				intOptions.add(lowHour);
				options.add(hourto12(lowHour));
			}
			
			considered.set(lowHour, true);
			
			boolean stop = true;
			for(Boolean bool : considered)
				if(!bool)
					stop = false;
			
			if(stop && doubleCheck++ > 150)
				return null; //all hours have been considered yet this has not moved on
		}
		
		//now we have a query, an answer, and 4 options
		Collections.shuffle(options);
		
		Question question = new Question(query, answer, options);
		return question;
	}
	
	/**
	 * takes a number between 0 and 23 inclusive and returns a number between 1 and 12
	 * inclusive followed by AM or PM
	 * @param hour of day
	 * @return string of 12 hour format
	 */
	public String hourto12(int hour){
		if(hour == 0)
			return "12 AM";
		else if(hour == 12)
			return "12 PM";
		else
			return hour % 12 + ((hour - 12 > 0) ? " PM" : " AM");
	}

}