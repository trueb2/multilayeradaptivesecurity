import net.sf.jsefa.Deserializer;
import net.sf.jsefa.common.lowlevel.filter.HeaderAndFooterFilter;
import net.sf.jsefa.csv.CsvIOFactory;
import net.sf.jsefa.csv.annotation.CsvDataType;
import net.sf.jsefa.csv.annotation.CsvField;
import net.sf.jsefa.csv.config.CsvConfiguration;

import java.io.IOException;
import java.io.StringReader;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.nio.file.Files.readAllBytes;
import static java.nio.file.Paths.get;

/**
 * Created by Qiuhua on 11/6/14.
 */

@CsvDataType
class smsRow{
    @CsvField(pos = 1)
    int id;

    @CsvField(pos = 2)
    String number;

    @CsvField(pos = 3)
    String date;

    @CsvField(pos = 4)
    String type;
}

public class analyzeSMS {

    public static void main(String[] args){
        String simpleTime;
        Format formatter;
        formatter = new SimpleDateFormat("MM/dd/yyyy");

        String contact;
        List<String> contactList = new ArrayList<String>();

        TreeMap<String, List<String>> smsHistory = new TreeMap<String, List<String>>();

        String cvsData = null;
        try {
            cvsData = new String(readAllBytes(get("sms.csv")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        // parse the csv file and insert it into TreeMap
        CsvConfiguration csvConfiguration = new CsvConfiguration();
        csvConfiguration.setFieldDelimiter(',');
        csvConfiguration.setLineFilter(new HeaderAndFooterFilter(1, false, false));

        Deserializer deserializer = CsvIOFactory.createFactory(csvConfiguration, smsRow.class).createDeserializer();

        deserializer.open(new StringReader(cvsData));
        while (deserializer.hasNext()) {
            List<String> temp = new ArrayList<String>();
            smsRow row = deserializer.next();
            Date time = new Date(row.date); // convert unix time to readable time
            simpleTime = formatter.format(time);

            contact = row.number;
            // generate a contact list for random pick when generating questions
            if (!contactList.contains(contact)) {
                contactList.add(contact);
            }

            if (smsHistory.containsKey(simpleTime)) {
                temp = smsHistory.get(simpleTime);
                temp.add(contact);
                smsHistory.put(simpleTime, temp);
            }
            else {
                temp.add(contact);
                smsHistory.put(simpleTime, temp);
            }
        }
        deserializer.close(true);

//        for(Map.Entry<String, List<String>> entry : smsHistory.entrySet()){
//            System.out.println(entry);
//            System.out.println("most occurring contact is:" + analyzeCallHistory.getMostOccurrence(entry.getValue()));
//            System.out.println("======================= Question ========================");
//            System.out.println("Who did you text most on " + entry.getKey() + "?");
//            List<String> candidateList = new ArrayList<String>();
//            // get first element of mostOccurrence as my correct answer
//            String correctAnswer = analyzeCallHistory.getMostOccurrence(entry.getValue()).get(0);
//            candidateList.add(correctAnswer);
//            // get three random wrong answer
//            List<String> contactListCopy = new ArrayList<String>(contactList);
//            contactListCopy.removeAll(candidateList); // making sure we will not pick correct answer from contactList
//            for(int i = 0; i < 3; i++){
//                String tmp = analyzeCallHistory.pickRandom(contactListCopy);
//                candidateList.add(tmp);
//                contactListCopy.remove(tmp); // make sure there is no duplicate
//            }
//            // shuffle
//            Collections.shuffle(candidateList);
//            System.out.println("A: " + candidateList.get(0) + " B: " + candidateList.get(1)
//                    + " C: " + candidateList.get(2) + " D: " + candidateList.get(3));
//            System.out.println("Correct answer is: " + correctAnswer);
//            System.out.println("==================== End of Question ====================");
//        }
    }

}
