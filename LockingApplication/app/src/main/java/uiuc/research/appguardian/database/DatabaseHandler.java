package uiuc.research.appguardian.database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;
import java.util.HashMap;

import uiuc.research.appguardian.questions.screenquestions.Off;
import uiuc.research.appguardian.questions.screenquestions.On;
import uiuc.research.appguardian.questions.screenquestions.Switch;
import uiuc.research.appguardian.util.Util;

/**
 * Created by qiuding on 9/30/2014.
 * <p/>
 * reference: http://www.androidhive.info/2011/11/android-sqlite-database-tutorial/
 */
public class DatabaseHandler extends SQLiteOpenHelper
{

    private static DatabaseHandler sInstance;

    /**
     * bunch of static constant
     */
    // database version
    private static final int DATABASE_VERSION = 1;

    // database name
    private static final String DATABASE_NAME = "userHistoryManager";

    // callHistory table name
    public static final String TABLE_CALLHISTORY = "callHistory";

    // callHistory table columns names
    private static final String CALLHISTORY_KEY_ID = "id";
    private static final String CALLHISTORY_KEY_NAME = "name";
    private static final String CALLHISTORY_KEY_PH_NO = "phoneNumber";
    private static final String CALLHISTORY_KEY_DURATION = "duration";
    private static final String CALLHISTORY_KEY_DATE = "date";
    private static final String CALLHISTORY_KEY_TYPE = "type";

    // listApps table name
    private static final String TABLE_LISTAPPS = "listApps";

    // listApps table columns names
    private static final String LISTAPPS_KEY_ID = "id";
    private static final String LISTAPPS_KEY_NAME = "name";
    private static final String LISTAPPS_KEY_SRCDIR = "srcDir";
    private static final String LISTAPPS_KEY_LAUNACT = "launchAct";

    // historyAndBookmarks table name
    public static final String TABLE_HISNBKMK = "historyAndBookmarks";

    // historyAndBookmarks table columns names
    private static final String HISNBKMK_KEY_ID = "id";
    private static final String HISNBKMK_KEY_TITLE = "title";
    private static final String HISNBKMK_KEY_TIMESTAMP = "timestamp";
    private static final String HISNBKMK_KEY_URL = "url";
    private static final String HISNBKMK_KEY_VISITS = "visits";

    // processes table
    public static final String TABLE_PROCESSES = "processes";
    private static final String PROCESSES_KEY_ID = "id";
    private static final String PROCESSES_KEY_PROCNAME = "processName";
    private static final String PROCESSES_KEY_IMPORTANCE = "importance";
    private static final String PROCESSES_KEY_TIMESTAMP = "timestamp";

    // action table
    public static final String TABLE_ACTION = "action";
    private static final String ACTION_KEY_ID = "id";
    private static final String ACTION_KEY_CONFIDENCE = "confidence";
    private static final String ACTION_KEY_TYPE = "type";
    private static final String ACTION_KEY_TIMESTAMP = "timestamp";

    // location table
    public static final String TABLE_LOCATION = "location";
    private static final String LOCATION_KEY_ID = "id";
    private static final String LOCATION_KEY_LAT = "latitude";
    private static final String LOCATION_KEY_LNG = "longitude";
    private static final String LOCATION_KEY_ADDRESS = "address";
//    private static final String LOCATION_KEY_LOCATIONSTR = "locationString";
    private static final String LOCATION_KEY_TIMESTAMP = "timestamp";

    // SMS table
    private static final String TABLE_SMS = "sms";
    private static final String SMS_KEY_ID = "id";
    private static final String SMS_KEY_NUMBER = "number";
    private static final String SMS_KEY_DATE = "date";
    private static final String SMS_KEY_TYPE = "type";

    //Screen Switch table
    public static final String TABLE_SCREEN = "screenSwitch";
    private static final String SCREEN_KEY_ID = "id";
    private static final String SCREEN_KEY_ON_TIMESTAMP = "onstamp";
    private static final String SCREEN_KEY_OFF_TIMESTAMP = "offstamp";

    // timestamp
    private static final String TIME_STAMP = "createdAt";

    private static final String CREATE_CALLHISTORY_TABLE = "CREATE TABLE " + TABLE_CALLHISTORY + "(" + CALLHISTORY_KEY_ID +
            " INTEGER PRIMARY KEY," + CALLHISTORY_KEY_NAME + " TEXT," + CALLHISTORY_KEY_PH_NO + " TEXT," + CALLHISTORY_KEY_DURATION +
            " TEXT," + CALLHISTORY_KEY_DATE + " TEXT," + CALLHISTORY_KEY_TYPE + " TEXT," + TIME_STAMP + " DATETIME DEFAULT CURRENT_TIMESTAMP" + ")";

    private static final String CREATE_LISTAPPS_TABLE = "CREATE TABLE " + TABLE_LISTAPPS + "(" + LISTAPPS_KEY_ID + " INTEGER PRIMARY KEY," +
            LISTAPPS_KEY_NAME + " TEXT," + LISTAPPS_KEY_SRCDIR + " TEXT," + LISTAPPS_KEY_LAUNACT + " TEXT" + TIME_STAMP +
            " DATETIME DEFAULT CURRENT_TIMESTAMP" + ")";

    private static final String CREATE_HISNBKMK_TABLE = "CREATE TABLE " + TABLE_HISNBKMK + "(" + HISNBKMK_KEY_ID + " INTEGER PRIMARY KEY," +
            HISNBKMK_KEY_TITLE + " TEXT," + HISNBKMK_KEY_URL + " TEXT," + HISNBKMK_KEY_VISITS + " INTEGER," + HISNBKMK_KEY_TIMESTAMP + " INTEGER" + ")";

//    private static final String CREATE_PROCESSES_TABLE = "CREATE TABLE " + TABLE_PROCESSES + "(" + PROCESSES_KEY_ID + " INTEGER PRIMARY KEY," +
//            PROCESSES_KEY_PROCNAME + " TEXT," + PROCESSES_KEY_IMPORTANCE + " TEXT," + TIME_STAMP + " DATETIME DEFAULT CURRENT_TIMESTAMP" + ")";
    private static final String CREATE_PROCESSES_TABLE = "CREATE TABLE " + TABLE_PROCESSES + "(" + PROCESSES_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            PROCESSES_KEY_PROCNAME + " TEXT," + PROCESSES_KEY_IMPORTANCE + " INTEGER," + PROCESSES_KEY_TIMESTAMP + " INTEGER)";
//    private static final String CREATE_ACTION_TABLE = "CREATE TABLE " + TABLE_ACTION + "(" + ACTION_KEY_ID + " INTEGER PRIMARY KEY," +
//            ACTION_KEY_CONFIDENCE + " TEXT," + ACTION_KEY_TYPE + " TEXT," + TIME_STAMP + " DATETIME DEFAULT CURRENT_TIMESTAMP" + ")";

    private static final String CREATE_ACTION_TABLE = "CREATE TABLE " + TABLE_ACTION + "(" + ACTION_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + ACTION_KEY_TIMESTAMP + " INTEGER," +
            ACTION_KEY_CONFIDENCE + " INTEGER," + ACTION_KEY_TYPE + " INTEGER" + ")";

//    private static final String CREATE_LOCATION_TABLE = "CREATE TABLE " + TABLE_LOCATION + "(" + LOCATION_KEY_ID + " INTEGER PRIMARY KEY," +
//            LOCATION_KEY_LOCATIONSTR + " TEXT," + TIME_STAMP + " DATETIME DEFAULT CURRENT_TIMESTAMP" + ")";

    private static final String CREATE_LOCATION_TABLE = "CREATE TABLE " + TABLE_LOCATION + "(" + LOCATION_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + LOCATION_KEY_TIMESTAMP + " INTEGER," +
            LOCATION_KEY_LAT + " REAL," + LOCATION_KEY_LNG + " REAL," + LOCATION_KEY_ADDRESS + " TEXT" + ")";

    private static final String CREATE_SMS_TABLE = "CREATE TABLE " + TABLE_SMS + "(" + SMS_KEY_ID + " INTEGER PRIMARY KEY," +
            SMS_KEY_NUMBER + " TEXT," + SMS_KEY_DATE + " TEXT," + SMS_KEY_TYPE + " TEXT," + TIME_STAMP + " DATETIME DEFAULT CURRENT_TIMESTAMP" + ")";

    private static final String CREATE_SCREEN_CHANGES_TABLE = "CREATE TABLE " + TABLE_SCREEN + "(" + SCREEN_KEY_ID + " INTEGER PRIMARY KEY, " +
            SCREEN_KEY_ON_TIMESTAMP + " INTEGER, " + SCREEN_KEY_OFF_TIMESTAMP + " INTEGER)";

    private ArrayList<Location> locStaging;
    private ArrayList<Action> actStaging;
    private ArrayList<LongOn> scrStaging;
    private Context mContext;

//    public DatabaseHandler(Context context)
//    {
//        super(context, DATABASE_NAME, null, DATABASE_VERSION);
//        locStaging = new ArrayList<>();
//        actStaging = new ArrayList<>();
//        scrStaging = new ArrayList<>();
//        mContext = context;
//    }

    private DatabaseHandler(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        locStaging = new ArrayList<>();
        actStaging = new ArrayList<>();
        scrStaging = new ArrayList<>();
        mContext = context;
    }

    public static synchronized DatabaseHandler getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DatabaseHandler(context);
        }
        return sInstance;
    }

    /**
     * main part of database handler
     *
     * @param db database
     */
    // creating tables
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(CREATE_CALLHISTORY_TABLE);
//        db.execSQL(CREATE_LISTAPPS_TABLE);
        db.execSQL(CREATE_HISNBKMK_TABLE);
        db.execSQL(CREATE_PROCESSES_TABLE);
        db.execSQL(CREATE_ACTION_TABLE);
        db.execSQL(CREATE_LOCATION_TABLE);
        db.execSQL(CREATE_SCREEN_CHANGES_TABLE);
        db.execSQL(CREATE_SMS_TABLE);
    }

    // upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        // drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CALLHISTORY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LISTAPPS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HISNBKMK);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROCESSES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACTION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SMS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCREEN);

        // create tables again
        onCreate(db);
    }

    // naive way to cleanup database
    public void overwriteTable(String name)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + name);

        if (name.equals(TABLE_CALLHISTORY))
        {
            db.execSQL(CREATE_CALLHISTORY_TABLE);
        }
        if (name.equals(TABLE_LISTAPPS))
        {
            db.execSQL(CREATE_LISTAPPS_TABLE);
        }
        if (name.equals(TABLE_HISNBKMK))
        {
            db.execSQL(CREATE_HISNBKMK_TABLE);
        }
        if (name.equals(TABLE_PROCESSES))
        {
            db.execSQL(CREATE_PROCESSES_TABLE);
        }
        if (name.equals(TABLE_ACTION))
        {
            db.execSQL(CREATE_ACTION_TABLE);
        }
        if (name.equals(TABLE_LOCATION))
        {
            db.execSQL(CREATE_LOCATION_TABLE);
        }
        if (name.equals(TABLE_SMS))
        {
            db.execSQL(CREATE_SMS_TABLE);
        }
        if (name.equals(TABLE_SCREEN))
        {
            db.execSQL(CREATE_SCREEN_CHANGES_TABLE);
        }
//        db.close();
    }

    /**
     * callHistory table
     * All CRUD(Create, Read, Update, Delete) Operations
     */
    // adding new call history
    public void addCallHistory(CallHistory callHistory)
    {
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            // values.put(CALLHISTORY_KEY_ID, callHistory.getId());
            values.put(CALLHISTORY_KEY_NAME, callHistory.getName());
            values.put(CALLHISTORY_KEY_PH_NO, callHistory.getPhoneNumber());
            values.put(CALLHISTORY_KEY_DURATION, callHistory.getDuration());
            values.put(CALLHISTORY_KEY_DATE, callHistory.getDate());
            values.put(CALLHISTORY_KEY_TYPE, callHistory.getType());
            //values.put(KEY_TIME_STAMP, System.currentTimeMillis()); //maybe

            // inserting row
            db.insert(TABLE_CALLHISTORY, null, values);
//            db.close();
        }
        catch(SQLiteDatabaseLockedException e)
        {
            return; //database is locked
        }
    }

    public ArrayList<CallHistory> getCallHistory(int daysBack) {

            ArrayList<CallHistory> callHistoryArrayList = new ArrayList<>(30 * daysBack);
        try {
            SQLiteDatabase db = getWritableDatabase();
            long back = System.currentTimeMillis() - Util.DAY_IN_MILLIS * daysBack;
            String selectQuery = "SELECT * FROM " + TABLE_CALLHISTORY + " WHERE " + CALLHISTORY_KEY_DATE + " > " + back;
            Cursor cursor = db.rawQuery(selectQuery, null);
            cursor.moveToFirst();
            while (cursor.moveToNext()) {
                CallHistory callHistory = new CallHistory();
                callHistory.setId(Integer.parseInt(cursor.getString(0)));
                callHistory.setName(cursor.getString(1));
                callHistory.setPhoneNumber(cursor.getString(2));
                callHistory.setDuration(cursor.getString(3));
                callHistory.setDate(cursor.getString(4));
                callHistory.setType(cursor.getInt(5));
                callHistory.setCreatedAt(cursor.getString(6));
                // Add to output list
                callHistoryArrayList.add(callHistory);
//                ////Log.d(Util.TAG, callHistory.getDate());
            }
            cursor.close();
        }catch (SQLiteException e)
        {
            overwriteTable(TABLE_CALLHISTORY);
        }

        return callHistoryArrayList;
    }

    public ArrayList<CallHistory> getAllCallHistory()
    {
        ArrayList<CallHistory> callHistoryList = new ArrayList<CallHistory>();

        String selectQuery = "SELECT * FROM " + TABLE_CALLHISTORY;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst())
        {
            do
            {
                CallHistory callHistory = new CallHistory();
                callHistory.setId(Integer.parseInt(cursor.getString(0)));
                callHistory.setName(cursor.getString(1));
                callHistory.setPhoneNumber(cursor.getString(2));
                callHistory.setDuration(cursor.getString(3));
                callHistory.setDate(cursor.getString(4));
                callHistory.setType(cursor.getInt(5));
                callHistory.setCreatedAt(cursor.getString(6));

                // add to output ArrayList
                callHistoryList.add(callHistory);
            }
            while (cursor.moveToNext());
        }

        cursor.close();

        return callHistoryList;
    }

    /**
     * listApps table
     * All CRUD(Create, Read, Update, Delete) Operations
     */
    void addListApps(ListApps listApps)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(LISTAPPS_KEY_NAME, listApps.getName());
        values.put(LISTAPPS_KEY_SRCDIR, listApps.getSrcDir());
        values.put(LISTAPPS_KEY_LAUNACT, listApps.getLaunchAct());
        db.insert(TABLE_LISTAPPS, null, values);
//        db.close();
    }

    public ArrayList<ListApps> getAllListApps()
    {
        ArrayList<ListApps> listAppsList = new ArrayList<ListApps>();
        String selectQuery = "SELECT * FROM " + TABLE_LISTAPPS;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToNext())
        {
            do
            {
                ListApps listApps = new ListApps();
                listApps.setId(Integer.parseInt(cursor.getString(0)));
                listApps.setName(cursor.getString(1));
                listApps.setSrcDir(cursor.getString(2));
                listApps.setLaunchAct(cursor.getString(3));
                listAppsList.add(listApps);
            }
            while (cursor.moveToNext());
        }

        cursor.close();
        return listAppsList;
    }

    /**
     * historyAndBookmarks table
     *
     */
    public void addHistoryAndBookmarks(HistoryAndBookmarks historyAndBookmarks)
    {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(HISNBKMK_KEY_TITLE, historyAndBookmarks.getTitle());
            values.put(HISNBKMK_KEY_URL, historyAndBookmarks.getUrl());
            values.put(HISNBKMK_KEY_TIMESTAMP, historyAndBookmarks.getDate());
            values.put(HISNBKMK_KEY_VISITS, historyAndBookmarks.getVisits());
            db.insert(TABLE_HISNBKMK, null, values);
//            db.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public ArrayList<HistoryAndBookmarks> getAllHistoryAndBookmarksSince(int daysBack)
    {
        try {
            ArrayList<HistoryAndBookmarks> historyAndBookmarksList = new ArrayList<HistoryAndBookmarks>();
            long validTime = System.currentTimeMillis() - Util.DAY_IN_MILLIS * daysBack;
            String selectQuery = "SELECT * FROM " + TABLE_HISNBKMK + " WHERE " + HISNBKMK_KEY_TIMESTAMP + " > " + validTime;
            ;
            SQLiteDatabase db = getReadableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToNext()) {
                do {
                    HistoryAndBookmarks historyAndBookmarks = new HistoryAndBookmarks();
                    historyAndBookmarks.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(HISNBKMK_KEY_ID))));
                    historyAndBookmarks.setTitle(cursor.getString(cursor.getColumnIndex(HISNBKMK_KEY_TITLE)));
                    historyAndBookmarks.setUrl(cursor.getString(cursor.getColumnIndex(HISNBKMK_KEY_URL)));
                    historyAndBookmarks.setCreatedAt(cursor.getString(cursor.getColumnIndex(HISNBKMK_KEY_TIMESTAMP)));
                    historyAndBookmarks.setVisits(cursor.getString(cursor.getColumnIndex(HISNBKMK_KEY_VISITS)));
                    historyAndBookmarksList.add(historyAndBookmarks);
                }
                while (cursor.moveToNext());
            }

            cursor.close();
            ////Log.d(Util.TAG, "There were " + historyAndBookmarksList.size() + " history items in the database");
            return historyAndBookmarksList;
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * processes table
     *
     * @param processes
     */
    public void addProcesses(Processes processes)
    {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(PROCESSES_KEY_PROCNAME, processes.getProcessName());
            values.put(PROCESSES_KEY_IMPORTANCE, processes.getImportance());
            values.put(PROCESSES_KEY_TIMESTAMP, processes.getCreatedAt());
            db.insert(TABLE_PROCESSES, null, values);
//            db.close();
        }
        catch(SQLiteDatabaseLockedException e)
        {
            e.printStackTrace();
            return; //database locked
        }
        catch(SQLiteException e)
        {
            //Table has not been created yet, create it
            overwriteTable(TABLE_PROCESSES);
        }
    }

    public ArrayList<Processes> getAllProcessesSince(int daysBack)
    {
        try {
            ArrayList<Processes> processesList = new ArrayList<Processes>();
            long validTime = System.currentTimeMillis() - Util.DAY_IN_MILLIS * daysBack;
            String selectQuery = "SELECT * FROM " + TABLE_PROCESSES + " WHERE " + PROCESSES_KEY_TIMESTAMP + " > " + validTime;
//            final String path = mContext.getDatabasePath(DATABASE_NAME).getPath();
//            SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
            SQLiteDatabase db = getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToNext()) {
                do {
                    Processes processes = new Processes();
                    processes.setId(Integer.parseInt(cursor.getString(0)));
                    processes.setProcessName(cursor.getString(1));
                    processes.setImportance((Integer.parseInt(cursor.getString(2))));
                    processes.setCreatedAt(Long.parseLong(cursor.getString(3)));
                    processesList.add(processes);
                }
                while (cursor.moveToNext());
            }

            cursor.close();
            return processesList;
        }
        catch(SQLiteDatabaseLockedException e)
        {
            e.printStackTrace();
            return null;
        }
        catch(SQLiteException e)
        {
            return null;
        }
    }

    /**
     * action table
     *
     * @param action
     */
    public void addAction(Action action)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ACTION_KEY_CONFIDENCE, action.getConfidence());
        values.put(ACTION_KEY_TYPE, action.getType());
        values.put(ACTION_KEY_TIMESTAMP, action.getTimestamp());
        db.insert(TABLE_ACTION, null, values);
//        db.close();
    }

    public void addActionFast(Action action, boolean transaction)
    {
        actStaging.add(action);

        if (!transaction)
        {
            return;
        }

        String sql = "INSERT INTO " + TABLE_ACTION + " (" + ACTION_KEY_TIMESTAMP + ", " +
                ACTION_KEY_CONFIDENCE + ", " + ACTION_KEY_TYPE + ") values (?,?,?);";

        SQLiteDatabase database = this.getWritableDatabase();
        this.getWritableDatabase();
        database.beginTransaction();
        SQLiteStatement stmt = database.compileStatement(sql);

        for (Action a : actStaging)
        {
            stmt.bindLong(1, a.getTimestamp());
            stmt.bindLong(2, a.getConfidence());
            stmt.bindLong(3, a.getType());

//            long entryID = stmt.executeInsert();
            stmt.executeInsert();
            stmt.clearBindings();
        }

        database.setTransactionSuccessful();
        database.endTransaction();
//        database.close();

//        String ns = Context.NOTIFICATION_SERVICE;
//        NotificationManager mNotificationManager = (NotificationManager) mContext.getSystemService(ns);
//        long when = System.currentTimeMillis();
//        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
//        String date = sdf.format(new Date(when));
//
//        Notification notification = new Notification(R.drawable.walking, actStaging.size() + " Logged at : " + date, when);
//        notification.setLatestEventInfo(mContext, "Recognition Service", "T: " + action.getType() + " C: " + action.getConfidence() + " Logged: " + date, null);
//        mNotificationManager.notify(113373, notification);
        actStaging = new ArrayList<Action>();
    }

    public ArrayList<Action> getActions(int daysBack)
    {
        ArrayList<Action> actionList = new ArrayList<Action>(2000 * daysBack);
        SQLiteDatabase db = this.getReadableDatabase();
        long back = System.currentTimeMillis() - Util.DAY_IN_MILLIS * daysBack;
        String selectQuery = "SELECT * FROM " + TABLE_ACTION + " WHERE " + ACTION_KEY_TIMESTAMP + " > " + back;
        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        if(cursor.isAfterLast() || cursor.isBeforeFirst())
            return new ArrayList<Action>();
        double s = 0;
        long start = System.currentTimeMillis();
        double[] ss = new double[6];
        double[] diffs = new double[3];
        do
        {
            s++;
            ss[0] = System.currentTimeMillis();
            long i = cursor.getLong(1); //timestamp
            ss[1] = System.currentTimeMillis();
            diffs[0] += ss[1] - ss[0];
            ss[2] = System.currentTimeMillis();
            int j =  cursor.getInt(2); //confidence
            ss[3] = System.currentTimeMillis();
            diffs[1] += ss[3] - ss[2];
            ss[4] = System.currentTimeMillis();
            int l = cursor.getInt(3); // type
            ss[5] = System.currentTimeMillis();
            diffs[2] += ss[5] - ss[4];
            Action act = new Action(j, l, i);
            actionList.add(act);
        }
        while (cursor.moveToNext());
        long stop = System.currentTimeMillis();
        ////Log.d(Util.TAG, (stop-start)/s + " average action " + diffs[0]/s + " " + diffs[1]/s + " " + diffs[2]/s);
        cursor.close();
//        do
//        {
//            ////Log.d(Util.TAG, cursor.getString(0) + "," + cursor.getString(1) + "," + cursor.getString(2) + "," + cursor.getString(3));
//        }
//        while (cursor.moveToNext());

        return actionList;
    }
//    public List<Action> getAllAction()
//    {
//        List<Action> actionList = new ArrayList<Action>();
//        String selectQuery = "SELECT * FROM " + TABLE_ACTION;
//        SQLiteDatabase db = getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        if (cursor.moveToNext())
//        {
//            do
//            {
//                Action action = new Action();
//                action.setConfidence(cursor.getString(1));
//                action.setType(cursor.getString(2));
//                action.setTimestamp(Long.parseLong(cursor.getString(3)));
//                actionList.add(action);
//            }
//            while (cursor.moveToNext());
//        }
//        return actionList;
//    }

    /**
     * location table
     *
     * @param location
     */
    public void addLocation(Location location)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(LOCATION_KEY_LAT, location.getLatitude());
        values.put(LOCATION_KEY_LNG, location.getLongitude());
        values.put(LOCATION_KEY_ADDRESS, location.getAddress());
        values.put(LOCATION_KEY_TIMESTAMP, location.getTimestamp());

        db.insert(TABLE_LOCATION, null, values);
//        values.put(LOCATION_KEY_LOCATIONSTR, location.getLocationString());
//        try
//        {
//            db.insert(TABLE_LOCATION, null, values);
//        }
//        catch (Exception e)
//        {
//            db.execSQL(CREATE_LOCATION_TABLE);
//            db.insert(TABLE_LOCATION, null, values);
//        }
//        db.close();
    }

    public void addLocationFast(Location location, boolean transaction)
    {
        locStaging.add(location);

        if (!transaction)
        {
            return;
        }

        String sql = "INSERT INTO " + TABLE_LOCATION + " (" + LOCATION_KEY_ID + "," + LOCATION_KEY_TIMESTAMP +
                ", " + LOCATION_KEY_LAT + ", " + LOCATION_KEY_LNG + "," + LOCATION_KEY_ADDRESS + ") values (?,?,?,?,?);";

        SQLiteDatabase database = this.getWritableDatabase();
        database.beginTransaction();
        SQLiteStatement stmt = database.compileStatement(sql);

        for (Location loc : locStaging)
        {
            stmt.bindNull(1);
            stmt.bindLong(2, loc.getTimestamp());
            stmt.bindDouble(3, loc.getLatitude());
            stmt.bindDouble(4, loc.getLongitude());
            stmt.bindString(5, loc.getAddress());

            long entryID = stmt.executeInsert();
            stmt.clearBindings();
        }

        database.setTransactionSuccessful();
        database.endTransaction();

//        database.close();

//        String ns = Context.NOTIFICATION_SERVICE;
//        NotificationManager mNotificationManager = (NotificationManager) mContext.getSystemService(ns);
//        long when = System.currentTimeMillis();
//        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
//        String date = sdf.format(new Date(when));
//
//        Notification notification = new Notification(R.drawable.graph, locStaging.size() + " Logged at : " + date, when);
//        notification.setLatestEventInfo(mContext, "Logging Service", "Logged at: " + date, null);
//        mNotificationManager.notify(7331, notification);
        locStaging = new ArrayList<Location>();
    }

    public ArrayList<Location> getLocations(int daysBack)
    {
        ArrayList<Location> locationList = new ArrayList<Location>(3300 * daysBack);
        SQLiteDatabase db = getReadableDatabase();
        long back = System.currentTimeMillis() - Util.DAY_IN_MILLIS * daysBack;
        String selectQuery = "SELECT * FROM " + TABLE_LOCATION + " WHERE " + LOCATION_KEY_TIMESTAMP + " > " + back;
        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        if(cursor.isAfterLast() || cursor.isBeforeFirst())
            return new ArrayList<Location>();
        ////Log.d(Util.TAG, "Getting Locations: " + new Location(cursor.getDouble(2), cursor.getDouble(3), cursor.getString(4), cursor.getLong(1)).toString());
        double s = 0;
        long start = System.currentTimeMillis();
        double[] ss = new double[8];
        double[] diffs = new double[4];
        do
        {
            s++;
            ss[0] = System.currentTimeMillis();
            double d = cursor.getDouble(2);      //.01
            ss[1] = System.currentTimeMillis();
            diffs[0] += ss[1] - ss[0];
            ss[2] = System.currentTimeMillis();
            double d1 = cursor.getDouble(3);     //.01
            ss[3] = System.currentTimeMillis();
            diffs[1] += ss[3] - ss[2];
            ss[4] = System.currentTimeMillis();
            String address = cursor.getString(4); //.02 ms
            //String address = "dummy null";
            ss[5] = System.currentTimeMillis();
            diffs[2] += ss[5] - ss[4];
            ss[6] = System.currentTimeMillis();
            long l = cursor.getLong(1);           //.01
            ss[7] = System.currentTimeMillis();
            diffs[3] += ss[7] - ss[6];
            Location loc = new Location(d,d1,address,l);
            locationList.add(loc);
        }
        while (cursor.moveToNext());
        long stop = System.currentTimeMillis();
        ////Log.d(Util.TAG, (stop-start)/s + " average location " + diffs[0]/s + " " + diffs[1]/s + " " + diffs[2]/s + " " + diffs[3]/s);
        cursor.moveToPrevious();
        ////Log.d(Util.TAG, "Got Location: " + new Location(cursor.getDouble(2), cursor.getDouble(3), cursor.getString(4), cursor.getLong(1)).toString());
        cursor.close();

        return locationList;
    }

    /**
     * replaces all location entries that have a null address with one that is readable
     */
    public void replaceAddresses()
    {
        ////Log.d(Util.TAG, "Replacing Addresses");
        HashMap<Location, String> addresses = new HashMap<Location, String>();
        SQLiteDatabase db = getWritableDatabase();
        long back = System.currentTimeMillis() - Util.DAY_IN_MILLIS * 5;
        String selectQuery = "SELECT * FROM " + TABLE_LOCATION + " WHERE " + LOCATION_KEY_TIMESTAMP + " > " + back;
        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        do
        {
            if (cursor.getString(4) == null || cursor.getString(4).contains(",") || cursor.getString(4).isEmpty() || cursor.getString(4).equals("null"))
            {
                Location loc = new Location(cursor.getDouble(2), cursor.getDouble(3), cursor.getString(4), cursor.getLong(1));
                String address;
                if (addresses.containsKey(loc) && addresses.get(loc) != null && addresses.get(loc).length() > 20)
                {
                    address = addresses.get(loc);
                } else
                {
                    address = Location.getRoadName(cursor.getString(2), cursor.getString(3));
                    ////Log.d(Util.TAG, "Replace ...");
                    addresses.put(loc, address);
                }

                ContentValues vals = new ContentValues();
                vals.put(LOCATION_KEY_TIMESTAMP, cursor.getLong(1));
                vals.put(LOCATION_KEY_LAT, cursor.getDouble(2));
                vals.put(LOCATION_KEY_LNG, cursor.getDouble(3));
                vals.put(LOCATION_KEY_ADDRESS, address);
                vals.put(LOCATION_KEY_ID, cursor.getInt(0));
                db.replace(TABLE_LOCATION, null, vals);
            }
        }
        while (cursor.moveToNext());
        cursor.close();
        ////Log.d(Util.TAG, "Replaced Addresses");
    }
//    public List<Location> getAllLocation()
//    {
//        List<Location> locationList = new ArrayList<Location>();
//        SQLiteDatabase db = getWritableDatabase();
//        String selectQuery;
//        try
//        {
//            selectQuery = "SELECT * FROM " + TABLE_LOCATION;
//        }
//        catch (Exception e)
//        {
//            db.execSQL(CREATE_LOCATION_TABLE);
//            selectQuery = "SELECT * FROM " + TABLE_LOCATION;
//        }
//
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//
////        int i = 1;
//
//        if (cursor.moveToNext())
//        {
//            do
//            {
//                Location location = new Location();
//                location.setId(Integer.parseInt(cursor.getString(0)));
//                String str = cursor.getString(1); //I was doing something here for  a while
//                location.setLocationString(str);
////                location.setId(i++);
//                location.setTimestamp(Long.parseLong(cursor.getString(2)));
//                locationList.add(location);
//            }
//            while (cursor.moveToNext());
//        }
//        return locationList;
//    }


    public void addSMS(SMS sms)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SMS_KEY_NUMBER, sms.getNumber());
        values.put(SMS_KEY_DATE, sms.getDate());
        values.put(SMS_KEY_TYPE, sms.getType());
        db.insert(TABLE_SMS, null, values);
//        db.close();
    }

    public ArrayList<SMS> getAllSMS()
    {
        ArrayList<SMS> smsList = new ArrayList<SMS>();
        String selectQuery = "SELECT * FROM " + TABLE_SMS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToNext())
        {
            do
            {
                SMS sms = new SMS();
                sms.setId(Integer.parseInt(cursor.getString(0)));
                sms.setNumber(cursor.getString(1));
                sms.setDate(cursor.getString(2));
                sms.setType(cursor.getString(3));
                smsList.add(sms);
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        return smsList;
    }

    /**
     * getSMS gets an ArrayList of SMS from sms history
     * @param daysBack length of history want to retrieve
     * @return ArrayList of SMS
     */
    public ArrayList<SMS> getSMS(int daysBack) {
        ArrayList<SMS> smsArrayList = new ArrayList<>(30 * daysBack);
        SQLiteDatabase db = getReadableDatabase();
        long back = System.currentTimeMillis() - Util.DAY_IN_MILLIS * daysBack;
        String selectQuery = "SELECT * FROM " + TABLE_SMS + " WHERE " + SMS_KEY_DATE + " > " + back;
        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        while(cursor.moveToNext()) {
            SMS sms = new SMS();
            sms.setId(Integer.parseInt(cursor.getString(0)));
            sms.setNumber(cursor.getString(1));
            sms.setDate(cursor.getString(2));
            sms.setType(cursor.getString(3));
            smsArrayList.add(sms);
            // Add to output list
//            smsArrayList.add(sms);
            //////Log.d(Util.TAG, sms.getDate());
        }
        cursor.close();
        return smsArrayList;
    }

    public void addSwitch(Switch s, boolean transaction)
    {
        ////Log.d(Util.TAG, "Called addSwitch, scrStaging is of size " + scrStaging.size());
        discardOrKeepSwitch(s);
        ////Log.d(Util.TAG, "scrStaging size is now " + scrStaging.size());

        if (!transaction)
        {
            return;
        }

        String sql = "INSERT INTO " + TABLE_SCREEN + " (" + SCREEN_KEY_ID + "," + SCREEN_KEY_ON_TIMESTAMP +
                ", " + SCREEN_KEY_OFF_TIMESTAMP + ") values (?,?,?);";

        SQLiteDatabase database = this.getWritableDatabase();
        database.beginTransaction();
        SQLiteStatement stmt = database.compileStatement(sql);

        for (LongOn lo : scrStaging)
        {
            if(!(lo.hasOn() && lo.hasOff())) {
                ////Log.d(Util.TAG, " Not adding longOn to database" + lo.hasOn() + " " + lo.hasOff());
                break;
            }

            stmt.bindNull(1);
            stmt.bindLong(2, lo.getOnTimestamp());
            stmt.bindLong(3, lo.getOffTimestamp());

            long entryID = stmt.executeInsert();
            stmt.clearBindings();
        }

        database.setTransactionSuccessful();
        database.endTransaction();

//        database.close();

        //clear scrStaging of recorded elements
        LongOn longOn = null;
        if(scrStaging.size() > 0) {
            longOn = scrStaging.get(scrStaging.size() - 1);
        }
        scrStaging = new ArrayList<>(); //reset to zero elements
        if(longOn != null && longOn.hasOn() && !longOn.hasOff()) {
            //was not added because it only has an On
            scrStaging.add(longOn);
        }
    }

    public ArrayList<LongOn> getScreenChanges(int daysBack)
    {
        ArrayList<LongOn> switchList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        long back = System.currentTimeMillis() - Util.DAY_IN_MILLIS * daysBack;
        String selectQuery = "SELECT * FROM " + TABLE_SCREEN + " WHERE " + SCREEN_KEY_ON_TIMESTAMP + " > " + back;
        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        ////Log.d(Util.TAG, "Getting Screen Switches (LongOns)");
        if(cursor.isAfterLast() || cursor.isBeforeFirst()) {
            ////Log.d(Util.TAG, "Cursor does not know where to start");
            return new ArrayList<>();
        }

        do
        {
            On on = new On(cursor.getLong(1));
            Off off = new Off(cursor.getLong(2));
            LongOn longOn = new LongOn(on, off);
            switchList.add(longOn);
        }
        while (cursor.moveToNext());
        cursor.close();

        ////Log.d(Util.TAG, "SwitchList is of size " + switchList.size());
        return switchList;
    }

    /**
     * check if a table exists
     * @param tableName name of the table to be checked
     * @return exists or not
     */
    public boolean isTableExists(String tableName) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '"+tableName+"'", null);
        if(cursor!=null) {
            if(cursor.getCount() > 1) {
                cursor.close();
                return true;
            }
            else {
                cursor.close();
            }
        }
        return false;
    }


    /**
     * Takes care of keeping track of whether or not an on or an off should be
     * logged
     * @param s
     */
    private void discardOrKeepSwitch(Switch s)
    {
        //check and see if you are completing a longOn or starting a new one
        if(scrStaging.size() > 0) {
            LongOn longOn = scrStaging.get(scrStaging.size() - 1);
            //check if the last LongOn in staging is complete
            if(longOn.hasOn() && longOn.hasOff())
            {
                //create a new long on and set it's on
                if(s.getType() == s.ON)
                {
                    LongOn newLongOn = new LongOn();
                    newLongOn.setOn((On)s);
                    scrStaging.add(newLongOn);
                }
                else
                {   //you received an off even though we havent received an on
                    return; //ignore it
                }
            }
            else if(longOn.hasOn() && s.getType() == s.ON)
            {
                //received another on without receiving an off lose the old one and start again
                longOn.setOn((On) s);
            }
            else if(longOn.hasOn() && s.getType() == s.OFF)
            {
                //have an on and received an off  good!
                longOn.setOff((Off) s);
            }
        }
        else
        {
            //scrStaging is empty add something if you received an On
            if(s.getType() == s.ON)
            {
                 LongOn longOn = new LongOn();
                longOn.setOn((On) s);
                scrStaging.add(longOn);
            }
            //else you got an Off forget about it
        }
    }
}
