package uiuc.research.appguardian.questions.screenquestions;

import java.util.ArrayList;

import uiuc.research.appguardian.database.LongOn;

/**
 * Created by Jacob on 2/26/2015.
 */
public class ScreenDatabase
{
    private ArrayList<On> ons = new ArrayList<On>();
    private ArrayList<Off> offs = new ArrayList<Off>();
    private static boolean ready = false;

    public void setSwitches(ArrayList<LongOn> switches)
    {
        if(switches == null || switches.size() == 0)
            return;

        ons = new ArrayList<>();
        offs = new ArrayList<>();
        Switch prev = null;
        for(LongOn s : switches)
        {
            ons.add(new On(s.getOnTimestamp()));
            offs.add(new Off(s.getOffTimestamp()));
        }

        ready = true;
    }


    public ArrayList<On> getOns(){
        return ons;
    }

    public ArrayList<Off> getOffs(){
        return offs;
    }

    public ArrayList<LongOn> getLongOns() {
        ArrayList<LongOn> los = new ArrayList<>(ons.size());
        for(int i = 0; i < ons.size(); i++)
        {
            los.add(new LongOn(ons.get(i), offs.get(i)));
        }
        return los;
    }

    public boolean isReady()
    {
        return ready;
    }
}
