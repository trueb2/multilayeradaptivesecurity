package uiuc.research.appguardian.database;

import uiuc.research.appguardian.util.Util;

public class Action implements Loggable
{

    int confidence;
    int type;
    long timestamp;

    public Action(int confidence, int type, long timestamp)
    {
        this.confidence = confidence;
        this.type = type;
        this.timestamp = timestamp;
    }

    public int getConfidence()
    {
        return confidence;
    }

    public int getType()
    {
        return type;
    }

    public long getTimestamp()
    {
        return timestamp;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof Action))
        {
            return false;
        }

        Action action = (Action) o;

        return action.type == type;
    }

    @Override
    public String toString(){
        return "T: " + type + " C: " + confidence + " Logged: " + Util.convertStamp(timestamp);
    }

    public String logLine()
    {
        return timestamp + ", " + confidence + ", " + type + "\n";
    }
}
