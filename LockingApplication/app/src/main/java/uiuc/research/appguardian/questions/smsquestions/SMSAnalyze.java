package uiuc.research.appguardian.questions.smsquestions;

import android.net.Uri;
import android.provider.ContactsContract;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.TreeMap;

import uiuc.research.appguardian.questions.Question;
import uiuc.research.appguardian.services.QuestionService;

/**
 * generate sms questions
 */
public class SMSAnalyze {
    private static SMSDataSet smsDataSet;
    private static ArrayList<String> contactList;

    /**
     * @return
     */
    public static Question whoIsMostOccurringSMSContact(int daysBack, int qType) {
        if (smsDataSet.getMostOccurringSmsContactTreeMap(daysBack).lastEntry() != null) {
            TreeMap<Integer, String> mostOccurringContactTreeMap = smsDataSet.getMostOccurringSmsContactTreeMap(daysBack);

            String query = "Who did you SMS most in the past " + Integer.toString(daysBack) +
                    " days?";
            String answer = mostOccurringContactTreeMap.lastEntry().getValue();
            ArrayList<String> choices = new ArrayList<>();

            choices.add(answer);
            while (choices.size() < 4) {
                String randomContact = pickRandomContact(contactList);
                if (!choices.contains(randomContact)) {
                    choices.add(randomContact);
                }
            }

            Collections.shuffle(choices);

            Question question = new Question(query, answer, choices, qType);
            return question;
        }
        else {
            Question question = new Question(null, null, null, qType);
            return question;
        }
    }

    /**
     * load data from DataSet
     *
     * @param inputSMSDataSet
     */
    public static void prepareDataSet(SMSDataSet inputSMSDataSet) {
        smsDataSet = inputSMSDataSet;
        contactList = smsDataSet.getContactList();
    }

    /**
     * @param contactList
     * @return
     */
    private static String pickRandomContact(ArrayList<String> contactList) {
        Random random = new Random();
        return contactList.get(random.nextInt(contactList.size()));
    }
}
