package uiuc.research.appguardian.questions.screenquestions;

import java.util.ArrayList;

import uiuc.research.appguardian.database.LongOn;
import uiuc.research.appguardian.questions.Question;
import uiuc.research.appguardian.util.Util;

/**
 * Created by Jacob on 2/26/2015.
 */
public class ScreenAnalyze
{
    private static ScreenDatabase screenDatabase = new ScreenDatabase();
    private static boolean printed = false;

    public static Question longOn(int daysBack, int questionType)
    {
        if(!screenDatabase.isReady())
            return new Question(null, null, null, questionType);

        ArrayList<LongOn> screenWoken = getLongOns();
//        if(!printed) {
//            for (LongOn lo : screenWoken)
//                //Log.d(Util.TAG, lo.toString());
//            printed = true;
//        }
        if(screenWoken.size() < 4)
        {
            //Log.d(Util.TAG, "screenWoken size is less than 4");
            return new Question(null, null, null, questionType);
        }

        String query = "What time of day did you use your phone for ";
        ArrayList<String> options = new ArrayList<String>();
        LongOn longest = new LongOn();
        ArrayList<LongOn> loOptions = new ArrayList<>();
        for(LongOn lo : screenWoken)
        {
            if(lo.getDuration() > 1000 * 60 && lo.getDuration() < 1000 * 60 * 60 * 5 && lo.getDuration() > longest.getDuration())
                longest = lo;
            else if(lo.getDuration() > 1000 * 60 * 5 && lo.getDuration() < 1000 * 60 * 60 * 5)
                loOptions.add(lo);
        }

        if(loOptions.size() < 3 || longest.getDuration() == 0)
        {
            //Log.d(Util.TAG, "Not enough options for screenon " + loOptions.size());
            return new Question(null, null, null, questionType);
        }
        //set the options, question, and answer
        options.add(Util.convertStamp(longest.getStart()));
        String answer = options.get(0);
        query += Util.convertMillis(longest.getDuration()) + " yesterday";
        options.add(Util.convertStamp(loOptions.get(0).getStart()));
        options.add(Util.convertStamp(loOptions.get(1).getStart()));
        options.add(Util.convertStamp(loOptions.get(2).getStart()));

        return new Question(query, answer, options, questionType);
//        String query = "What time of day did you use your phone for " + Util.convertMillis(screenWoken.get(0).getDuration()) + " yesterday";
//        ArrayList<String> options = new ArrayList<String>();
//        options.add(Util.convertStamp(screenWoken.get(0).getStart()));
//        String answer = options.get(0); //set just in case
//
//        int ansIndex = (int) Math.random() * screenWoken.size();
//        for(int i = 0; i < screenWoken.size(); i++)
//        {
//            LongOn lo = screenWoken.get(i);
//            if(lo.getDuration() > 1000 * 60 * 15 && lo.getDuration() < 1000 * 60 * 60 * 5)
//            {
//                if(ansIndex == i)
//                {
//                    options.set(0, Util.convertStamp(lo.getStart()));
//                    query = "What time of day did you use your phone (or have the screen on) for " + Util.convertMillis(lo.getDuration()) + " yesterday";
//                    answer = options.get(0);
//                }
//                else
//                    options.add(Util.convertStamp(lo.getStart()));
//            }
//            else if(ansIndex == i)
//                ansIndex++; //move along to see if you can get a randomly chosen answer
//        }
//
//        Collections.shuffle(options);
//        while(options.size() > 4)
//        {
//            options.remove(1);
//        }
//        Collections.shuffle(options);

//        return new Question(query, answer, options);
    }

    /**
     * pairs ons and offs
     * @return
     */
    public static ArrayList<LongOn> getLongOns()
    {
        return screenDatabase.getLongOns();
    }
    public static void prepareDatabase(ArrayList<LongOn> switches)
    {
        screenDatabase.setSwitches(switches);
    }
}
