package uiuc.research.appguardian.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import uiuc.research.appguardian.services.LoggingService;
import uiuc.research.appguardian.services.QuestionService;
import uiuc.research.appguardian.services.RecognitionService;
import uiuc.research.appguardian.util.Util;

/**
 * Created by jwtrueb on 11/6/14.
 * BroadcastReceiver for the BOOT_COMPLETED
 * signal
 * this starts the logging service on boot
 */
public class AutoStartReceiver extends BroadcastReceiver
{

    @Override
    public void onReceive(Context context, Intent i)
    {
//        Intent intent = new Intent(context, LoggingService.class);
//        context.startService(intent);
//        Intent intent1 = new Intent(context, RecognitionService.class);
//        context.startService(intent1);
//        Intent intent2 = new Intent(context, QuestionService.class);
//        context.startService(intent2);
        Util.loadServicesInBackground(context);
        //Log.d(Util.TAG, "Received BOOT_COMPLETED signal and started LoggingService and RecognitionService and QuestionService");
    }
}
