package uiuc.research.appguardian.questions.locationquestions;

import java.util.ArrayList;

import uiuc.research.appguardian.database.Location;
import uiuc.research.appguardian.util.Util;

/**
 * Created by jwtrueb on 11/4/14.
 * <p/>
 * A Trip has:
 * <p/>
 * 1. Source
 * 2. Path
 * 3. Destination
 * <p/>
 * These will be in the form of locations:
 * Starting Locations, Locations logged on the
 * way to the destination, and the destination
 * Location.
 */
public class Trip
{

    protected Location source;
    protected ArrayList<Location> path;
    protected Location destination;
    protected long duration;
    protected boolean started = false;
    protected boolean staycation = false;

    /**
     * new trip hasnt started yet
     */
    public Trip()
    {
        source = null;
        path = new ArrayList<Location>();
        destination = null;
        duration = Long.MAX_VALUE;
    }

    /**
     * called when starting a trip
     *
     * @param source
     */
    public Trip(Location source)
    {
        this.source = source;
        path = new ArrayList<Location>();
        destination = null;
        started = true;
    }

    /**
     * probably wont ever be used
     *
     * @param source
     * @param pathTaken
     * @param destination
     */
    public Trip(Location source, ArrayList<Location> pathTaken, Location destination)
    {
        this.source = source;
        this.path = pathTaken;
        this.destination = destination;
        started = true; //did the whole trip already
        started = false;
    }

    /**
     * adds a location to the path that describes your journey
     *
     * @param loc
     */
    public void addToPath(Location loc)
    {
        path.add(loc);
    }

    @Override
    /**
     * String representation of the trip
     * @return String Trip
     */
    public String toString()
    {
        if (destination == null || source == null)
        {
            return "This was not a real trip";
        }
        String trip = "Started at : " + source.toString() + " at " + Util.convertStamp(source.getTimestamp()) + "\n";
        if (!staycation)
        {
            int i = 1;
            for (Location loc : path)
            {
                trip += i++ + ") " + loc.toString() + " \n";
            }
        }
        trip += "Ended at : " + destination.toString() + " at " + Util.convertStamp(destination.getTimestamp()) + "\n";
        return trip;
    }

    public Location getSource()
    {
        return source;
    }

    public ArrayList<Location> getPath()
    {
        return path;
    }

    public Location getDestination()
    {
        return destination;
    }

    public boolean isStarted()
    {
        return started;
    }

    public void setSource(Location source)
    {
        this.source = source;
    }

    public void setPath(ArrayList<Location> path)
    {
        this.path = path;
    }

    public void setDestination(Location destination)
    {
        this.destination = destination;

        //determine the duration
        duration = destination.getTimestamp() - source.getTimestamp();
    }

    public void setStarted(boolean started)
    {
        this.started = started;
    }

    public void setStaycation()
    {
        staycation = true;

    }
}
