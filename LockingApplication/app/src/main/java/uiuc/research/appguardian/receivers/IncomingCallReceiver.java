package uiuc.research.appguardian.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import uiuc.research.appguardian.database.CallHistory;
import uiuc.research.appguardian.database.DatabaseHandler;
import uiuc.research.appguardian.util.Util;

/**
 * IncomingCallReceiver updates the database when user finishes
 * an incoming call
 */
public class IncomingCallReceiver extends BroadcastReceiver {
    private Cursor cursor;

    @Override
    public void onReceive(final Context context, Intent intent) {
        TelephonyManager tmgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        PhoneStateListener psl = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                // FIXME onCallStateChanged is logging same call more than once
                //Log.d(Util.TAG, "State: " + state + " Number: " + incomingNumber);
                if (state == 1) {
                    //ringing
                    //Log.d(Util.TAG, "Incoming Call");
                }
                // retrieve new call info from call log after user hangs up
                // this should work for both incoming and outgoing call
                if (state == 0) {
                    //Log.d(Util.TAG, "Hang up");
                    Uri allCalls = Uri.parse("content://call_log/calls");
                    cursor = context.getContentResolver().query(allCalls, null, null, null, null);
                    DatabaseHandler mDatabaseHandler = DatabaseHandler.getInstance(context);
                    cursor.moveToLast();
                    //Log.d(Util.TAG, "last from cursor: " + cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER)));

                    String num = cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER));
                    String name = cursor.getString(cursor.getColumnIndex(CallLog.Calls.CACHED_NAME));
                    String duration = cursor.getString(cursor.getColumnIndex(CallLog.Calls.DURATION));
                    String date = cursor.getString(cursor.getColumnIndex(CallLog.Calls.DATE));
                    int callType = Integer.parseInt(cursor.getString(cursor.getColumnIndex(CallLog.Calls.TYPE)));
                    CallHistory callHistory = new CallHistory(name, num, duration, date, callType);
                    mDatabaseHandler.addCallHistory(callHistory);
                    //Log.d(Util.TAG, "insert new call info: number: " + num + ", type: " + callType);
                }
            }
        };
        tmgr.listen(psl, PhoneStateListener.LISTEN_CALL_STATE);
//        if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(TelephonyManager.EXTRA_STATE_RINGING)) {
//            // This code will execute when the phone has an incoming call
//
//            // get the phone number
//            String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
//            Toast.makeText(context, "Call from:" + incomingNumber, Toast.LENGTH_LONG).show();
//            //Log.d(Util.TAG, incomingNumber);
//
//        } else if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(
//                TelephonyManager.EXTRA_STATE_IDLE)
//                || intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(
//                TelephonyManager.EXTRA_STATE_OFFHOOK)) {
//            // This code will execute when the call is disconnected
//            Toast.makeText(context, "Detected call hangup event", Toast.LENGTH_LONG).show();
//            //Log.d(Util.TAG, "hang up");
//        }
    }
}
