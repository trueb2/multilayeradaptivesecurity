package uiuc.research.appguardian.questions.processesquestions;

import android.app.ActivityManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.TreeSet;

import uiuc.research.appguardian.database.DatabaseHandler;
import uiuc.research.appguardian.database.Processes;
import uiuc.research.appguardian.util.Util;

/**
 * Created by jwtrueb on 4/29/15.
 */
public class ProcDatabase {
    private ArrayList<Processes> procs = null;
    private ArrayList<Long> logFrequency;
    private TreeSet<Processes> foreground;
    private DatabaseHandler mDatabaseHandler;
    private boolean empty;

    public void fillDatabase(ArrayList<Processes> procs)
    {
        this.procs = procs;
        //Log.d(Util.TAG, "There were " + procs.size() + " processes logs");
        empty = false;
        findForeground();
    }

    public void findForeground()
    {
        if(empty || procs == null || procs.size() == 0)
            return;

        logFrequency = new ArrayList<Long>();
        foreground = new TreeSet<Processes>(new Comparator<Processes>() {
            @Override
            public int compare(Processes lhs, Processes rhs) {
                if(lhs.getCreatedAt() < rhs.getCreatedAt())
                    return -1;
                else if(lhs.getCreatedAt() > rhs.getCreatedAt())
                    return 1;
                else
                    return 0;
            }
        });

        for(Processes p : procs)
        {
            //if the process is running in the foreground put it in set
            if(p.getImportance() == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND)
                foreground.add(p);

            //add consecutive log times but nothing within a second of the previous
            if(logFrequency.size() == 0 ||
                    Math.abs(logFrequency.get(logFrequency.size()-1) - p.getCreatedAt()) > 1000)
                logFrequency.add(p.getCreatedAt());
        }
    }

    public boolean isEmpty()
    {
        return empty;
    }

    public TreeSet<Processes> getForeground()
    {
        return foreground;
    }

    public ArrayList<Long> getLogFrequency()
    {
        return logFrequency;
    }
}
