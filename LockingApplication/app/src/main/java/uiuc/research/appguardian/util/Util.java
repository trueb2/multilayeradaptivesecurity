package uiuc.research.appguardian.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Browser;
import android.provider.CallLog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import org.apache.http.impl.client.DefaultTargetAuthenticationHandler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import uiuc.research.appguardian.R;
import uiuc.research.appguardian.database.CallHistory;
import uiuc.research.appguardian.database.DatabaseHandler;
import uiuc.research.appguardian.database.HistoryAndBookmarks;
import uiuc.research.appguardian.questions.locationquestions.LocationAnalyze;
import uiuc.research.appguardian.services.LoggingService;
import uiuc.research.appguardian.services.QuestionService;
import uiuc.research.appguardian.services.RecognitionService;

public abstract class Util
{

    public static final String TAG = "App Guardian";
    public static final long DAY_IN_MILLIS = 86400000;


    /**
     * Sets the background {@link android.graphics.drawable.Drawable} of a view.<br>
     * On API level 16+ {@link android.view.View#setBackgroundDrawable(android.graphics.drawable.Drawable)} is
     * deprecated, so we use the new method {@link android.view.View#setBackground(android.graphics.drawable.Drawable)}
     *
     * @param v  The {@link android.view.View} on which to set the background
     * @param bg The background
     */
    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    public static final void setBackgroundDrawable(View v, Drawable bg)
    {
        if (v == null)
        {
            return;
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
        {
            v.setBackgroundDrawable(bg);
        } else
        {
            v.setBackground(bg);
        }
    }

    /**
     * SWAR Algorithm<br>
     *
     * @param i
     * @return The number of set bits in a 32bit integer
     */
    public static final int numberOfSetBits(int i)
    {
        i = i - ((i >> 1) & 0x55555555);
        i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
        return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
    }

    /**
     * Get a {@link android.graphics.Bitmap} from a {@link android.graphics.drawable.Drawable}
     *
     * @param drawable
     * @return The {@link android.graphics.Bitmap} representing this {@link android.graphics.drawable.Drawable}
     */
    public static Bitmap drawableToBitmap(Drawable drawable)
    {
        if (drawable instanceof BitmapDrawable)
        {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    /**
     * Utility method to get an {@link android.content.pm.ActivityInfo} for a packageName.
     *
     * @param packageName
     * @return an {@link android.content.pm.ActivityInfo} or null if not found. (or if packageName
     * or context are null)
     */
    public static final ApplicationInfo getaApplicationInfo(String packageName,
            Context c)
    {
        if (packageName == null || c == null)
        {
            return null;
        }
        try
        {
            return c.getPackageManager().getApplicationInfo(packageName, 0);
        }
        catch (NameNotFoundException e)
        {
            return null;
        }
    }

    public static float dpToPx(float dp, Context context)
    {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    /**
     * calculates yesterdays day of the week for use with Calendar
     */
    public static int yesterday()
    {
        long today = System.currentTimeMillis();
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getDefault());
        cal.setTimeInMillis(today - DAY_IN_MILLIS);
        return cal.get(Calendar.DAY_OF_WEEK);
    }

    /**
     * calculates the day before yesterdays day of the week
     * for use with Calendar
     */
    public static int dayBeforeYesterday()
    {
        long today = System.currentTimeMillis();
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getDefault());
        cal.setTimeInMillis(today - 2 * DAY_IN_MILLIS);
        return cal.get(Calendar.DAY_OF_WEEK);
    }

    /**
     * returns a human readable version of the timestamp
     */
    public static String convertStamp(long stamp)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
        String date = sdf.format(new Date(stamp));
        return date;
    }

    public static String convertMillis(long millis)
    {
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        long hour = minutes / 60;
        String duration = "";
        if(hour > 0)
        {
            duration += hour == 1 ? "1 hour " : hour + " hours ";
        }

        if(minutes > 8 && minutes < 23)
        {
            duration += " and 15 minutes";
        }
        else if(minutes > 22 && minutes < 38)
        {
            duration += " and 30 minutes";
        }
        else if(minutes > 37)
        {
            duration += " and 45 minutes";
        }

        if(!duration.contains("hour") && duration.contains("and"))
            duration = duration.substring(4);

        duration += duration.length() == 0 ? millisToMinutes(millis) : "";
        return duration;
    }

    public static String millisToMinutes(long millis)
    {
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        return minutes + " minutes";
    }

    public static void loadServicesInBackground(Context context)
    {
        final Context c = context;
        AsyncTask asyncTask = new AsyncTask<Object, Object, Object>()
        {
            @Override
            protected Void doInBackground(Object... voids)
            {
                //Log.d(Util.TAG, "Loading Services from AsyncTask");

                //start logging services
                if (!LoggingService.isRunning())
                {
                    //Log.d(Util.TAG, "Logging Service was not running");
                    Intent location = new Intent(c, LoggingService.class);
                    c.startService(location);
                }
                if (!RecognitionService.isRunning())
                {
                    //only start if we are allowed too
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
                    try {
                        if (prefs.getBoolean(c.getString(R.string.nav_action), true)) {
                            //Log.d(Util.TAG, "Recognition Service was not running");
                            Intent activityRecognition = new Intent(c, RecognitionService.class);
                            c.startService(activityRecognition);
                        }
                    }catch(ClassCastException e)
                    {
                        SharedPreferences.Editor ed = prefs.edit();
                        ed.putBoolean(c.getString(R.string.nav_action), true);
                        ed.commit();
                    }
                }
                if (!QuestionService.isRunning())
                {
                    //Log.d(Util.TAG, "Question Service was not running");
                    Intent question = new Intent(c, QuestionService.class);
                    c.startService(question);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object v)
            {
                super.onPostExecute(v);
                //Log.d(Util.TAG, "Finished Loading Services");
            }
        };
        asyncTask.execute(new Object());
    }

    public static void fillCallHistory(Context context) {
        final Context c = context;
        new Thread(new Runnable() {
            @Override
            public void run() {
                DatabaseHandler mDatabaseHandler = DatabaseHandler.getInstance(c);
                mDatabaseHandler.overwriteTable(DatabaseHandler.TABLE_CALLHISTORY);
                Uri allCalls = Uri.parse("content://call_log/calls");
                Cursor cursor = c.getContentResolver().query(allCalls, null, null, null, null);
                mDatabaseHandler.overwriteTable("callHistory");
                while (cursor.moveToNext()) {
//                    int id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(CallLog.Calls._ID)));
                    String num = cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER));
                    String name = cursor.getString(cursor.getColumnIndex(CallLog.Calls.CACHED_NAME));
                    String duration = cursor.getString(cursor.getColumnIndex(CallLog.Calls.DURATION));
                    String date = cursor.getString(cursor.getColumnIndex(CallLog.Calls.DATE));
                    int callType = Integer.parseInt(cursor.getString(cursor.getColumnIndex(CallLog.Calls.TYPE)));
                    CallHistory callHistory = new CallHistory(name, num, duration, date, callType);
                    mDatabaseHandler.addCallHistory(callHistory);
                    //Log.d(Util.TAG, "Insert call history: " + num + ", " + date);
                }
                cursor.close();
                // Get call history from database and generate questions
                ArrayList<CallHistory> callHistoryArrayList;
                callHistoryArrayList = mDatabaseHandler.getCallHistory(5);
            }
        }).start();
    }

    public static void fillBrowserHistory(final Context context)
    {
        final Context c = context;
        new Thread(new Runnable() {
            @Override
            public void run() {
                Cursor mCur = c.getContentResolver().query(Browser.BOOKMARKS_URI,
                        Browser.HISTORY_PROJECTION, null, null, null);
                DatabaseHandler mDatabaseHandler = DatabaseHandler.getInstance(c);
                mDatabaseHandler.overwriteTable(DatabaseHandler.TABLE_HISNBKMK);
                //Log.d(Util.TAG, "Loading Browser History for first time user");

                mCur.moveToFirst();
                if (mCur.moveToFirst() && mCur.getCount() > 0) {
                    while (!mCur.isAfterLast()) {
                        String title = mCur.getString(mCur.getColumnIndex(Browser.BookmarkColumns.TITLE));
                        String url = mCur.getString(mCur.getColumnIndex(Browser.BookmarkColumns.URL));
                        String visits = mCur.getString(mCur.getColumnIndex(Browser.BookmarkColumns.VISITS));
                        String date = mCur.getString(mCur.getColumnIndex(Browser.BookmarkColumns.DATE));

                        HistoryAndBookmarks hb = new HistoryAndBookmarks(title, url, visits, date);
                        mDatabaseHandler.addHistoryAndBookmarks(hb);

                        //Log.d(Util.TAG, "Browser history: " + mCur.getString(Browser.HISTORY_PROJECTION_TITLE_INDEX));
                        mCur.moveToNext();
                    }
                }
            }
        }).start();
    }
}
