package uiuc.research.appguardian.questions.smsquestions;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

import uiuc.research.appguardian.database.SMS;
import uiuc.research.appguardian.questions.Question;
import uiuc.research.appguardian.services.QuestionService;
import uiuc.research.appguardian.util.Util;

/**
 * data set for generating sms questions
 */
public class SMSDataSet {
    // all CallHistory
    private ArrayList<SMS> smsHistories;
    private TreeMap<Integer, String> mostOccurringContactTreeMap = new TreeMap<>();
    private ArrayList<String> contactList = new ArrayList<>();
    private HashMap<String, String> namesByNumber = new HashMap<>();

    /**
     * instead of making a bunch of static methods, I'm trying to make
     * this class more like a normal oop class
     * @param smsHistories data from database
     */
    public SMSDataSet(ArrayList<SMS> smsHistories) {
        this.smsHistories = smsHistories;

        for(SMS s : smsHistories)
        {
            if(namesByNumber.containsKey(s.getNumber()))
                continue;

            ContentResolver cr = QuestionService.getInstance().getContentResolver();
            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(s.getNumber()));
            Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
            if (cursor == null) {
                namesByNumber.put(s.getNumber(), s.getNumber());
            }
            String contactName = null;
            if(cursor.moveToFirst()) {
                contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
            }

            if(cursor != null && !cursor.isClosed()) {
                cursor.close();
            }

            if(contactName == null)
                contactName = s.getNumber();

            namesByNumber.put(s.getNumber(), contactName);
        }
    }

    /**
     *
     * @return TreeMap containing (Number of calls, Name)
     */
    public TreeMap<Integer, String> getMostOccurringSmsContactTreeMap(int daysBack) {
        calculateMostOccurringSmsContact(daysBack);
        return mostOccurringContactTreeMap;
    }

    /**
     *
     * @return a recent contact list
     */
    public ArrayList<String> getContactList() {
        return contactList;
    }

    /**
     * calculate most occurring contact based on callHistories
     */
    private void calculateMostOccurringSmsContact(int daysBack) {
        HashMap<String, Integer> callerFrequencyHashMap = new HashMap<>();
        ArrayList<SMS> localSmsHistories = new ArrayList<>();
        long backTimeLimit = System.currentTimeMillis() - Util.DAY_IN_MILLIS * daysBack;

        // get a subset of callhistories for question generation
        for (SMS smsHistory : smsHistories) {
            if (Long.parseLong(smsHistory.getDate()) > backTimeLimit) {
                localSmsHistories.add(smsHistory);
            }
        }

        ContentResolver cr = QuestionService.getInstance().getContentResolver();
        // count frequency
        for (SMS smsHistory : localSmsHistories) {
//            String name = smsHistory.getName();
            String num = getContactName(smsHistory.getNumber());
//            String num = smsHistory.getNumber();
            // if name is not in the HashMap
            if (callerFrequencyHashMap.get(num) == null) {
                callerFrequencyHashMap.put(num, 1);
                this.contactList.add(num);
            }
            // it's already in the HashMap
            else {
                int currentCount = callerFrequencyHashMap.get(num);
                currentCount += 1;
                callerFrequencyHashMap.put(num, currentCount);
            }
        }
        // sort using a TreeMap
        for (String num : callerFrequencyHashMap.keySet()) {
            this.mostOccurringContactTreeMap.put(callerFrequencyHashMap.get(num), num);
        }
    }

    public String getContactName(String phoneNumber) {
        return namesByNumber.get(phoneNumber);
    }
}
