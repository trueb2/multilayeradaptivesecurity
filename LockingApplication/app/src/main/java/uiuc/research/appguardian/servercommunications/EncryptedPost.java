package uiuc.research.appguardian.servercommunications;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import uiuc.research.appguardian.database.Action;
import uiuc.research.appguardian.database.CallHistory;
import uiuc.research.appguardian.database.DatabaseHandler;
import uiuc.research.appguardian.database.HistoryAndBookmarks;
import uiuc.research.appguardian.database.Location;
import uiuc.research.appguardian.database.LongOn;
import uiuc.research.appguardian.database.Processes;
import uiuc.research.appguardian.database.SMS;
import uiuc.research.appguardian.util.Util;

/**
 * Created by jwtrueb on 5/21/15.
 */
public class EncryptedPost {

    private Context mContext;
    private boolean ready = true;
    private ArrayList<String> files;

    public EncryptedPost(Context c)
    {
        mContext = c;
        files = new ArrayList<String>();
        preparePost();
        Log.d(Util.TAG, "Posts prepared");
    }

    private void preparePost()
    {
        //Get an instance of the DatabaseHandler
        DatabaseHandler mDatabaseHandler = DatabaseHandler.getInstance(mContext);

        //Get the Unique device ID
        TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        String id = tm.getDeviceId();
        int hashedID = id.hashCode();

        //Use all of the get all methods and store results
        //Hash each individual piece of data and store it
        //Write hashed info to file

        ArrayList<CallHistory> ch = mDatabaseHandler.getCallHistory(10);
        ArrayList<Integer> chi = new Hasher<CallHistory>().hash(ch);
        ch = null;
        ready = ready && write(chi, "ch");

        ArrayList<HistoryAndBookmarks> hb = mDatabaseHandler.getAllHistoryAndBookmarksSince(10);
        ArrayList<Integer> hbi = new Hasher<HistoryAndBookmarks>().hash(hb);
        hb = null;
        ready = ready && write(hbi, "hb");

        ArrayList<Processes> p = mDatabaseHandler.getAllProcessesSince(10);
        ArrayList<Integer> pi = new Hasher<Processes>().hash(p);
        p = null;
        ready = ready && write(pi, "p");

        ArrayList<Action> a = mDatabaseHandler.getActions(10);
        ArrayList<Integer> ai = new Hasher<Action>().hash(a);
        a = null;
        ready = ready && write(ai, "a");

        ArrayList<Location> l = mDatabaseHandler.getLocations(10);
        ArrayList<Integer> li = new Hasher<Location>().hash(l);
        l = null;
        ready = ready && write(li, "l");

        ArrayList<SMS> s = mDatabaseHandler.getSMS(10);
        ArrayList<Integer> si = new Hasher<SMS>().hash(s);
        s = null;
        ready = ready && write(si, "s");

        ArrayList<LongOn> lo = mDatabaseHandler.getScreenChanges(10);
        ArrayList<Integer> loi = new Hasher<LongOn>().hash(lo);
        lo = null;
        ready = ready && write(loi, "lo");

    }

    public void execute()
    {
        if(!ready) {
            Log.d(Util.TAG, "Not ready to execute for EncryptedPost");
            return;
        }

        for(String filePath : files) {
            new AsyncTask<String, Void, Void>() {

                @Override
                protected Void doInBackground(String... params) {
                    String url = "http://ttat-control.iti.illinois.edu:5900/webfiles/logEncryptedFiles.php";
                    File file = new File(params[0]);
                    try {
                        HttpClient httpclient = new DefaultHttpClient();

                        HttpPost httppost = new HttpPost(url);

                        FileBody fileBody = new FileBody(file);
                        Log.d(Util.TAG, params[0] + " is of length " + fileBody.getContentLength());
                        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
                        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
                        builder.addPart("file", fileBody);
                        HttpEntity reqEntity = builder.build();
                        httppost.setEntity(reqEntity);

                        HttpResponse response = httpclient.execute(httppost);
                        //Do something with response...
                        Log.d(Util.TAG, "Sent " + params[0] + " to server received " +
                                response.getStatusLine().toString());

                    } catch (Exception e) {
                        // show error
                        e.printStackTrace();
                        file.delete();
                    } finally {
                        //clean up files
                        file.delete();
                    }
                    return null;
                }

            }.execute(filePath);
        }

        ready = false;
    }

    private class Hasher<E>
    {
        public ArrayList<Integer> hash(ArrayList<E> stuffs)
        {
            ArrayList<Integer> hashes = new ArrayList<Integer>(stuffs.size());

            for(E e : stuffs)
            {
                hashes.add(e.hashCode());
            }

            Log.d(Util.TAG, "hashes is of size " + hashes.size());
            return hashes;
        }
    }

    public boolean write(ArrayList<Integer> stuffs, String append)
    {
        if(stuffs == null || stuffs.size() == 0)
            return false;

        if(!isExternalStorageWritable())
            return false;

        try {
            File exportDir = Environment.getExternalStoragePublicDirectory("MyCVS");
            String filePath = exportDir.getAbsolutePath() + "/hashedstaging"+append;
            File file = new File(filePath);
            files.add(filePath);
            FileOutputStream fOut = new FileOutputStream(file);
            OutputStreamWriter writer = new OutputStreamWriter(fOut);

            file.createNewFile();

            //write device identifier
            TelephonyManager tm = (TelephonyManager)mContext.getSystemService(Context.TELEPHONY_SERVICE);
            String id = tm.getDeviceId();
            int hashedID = id.hashCode();
            writer.append("BEGIN " + append + " " + hashedID + "\n");

            //write hashed content
            for(Integer e : stuffs)
            {
                writer.append(e + "\n");
            }

            writer.close();
            fOut.close();
        }
        catch(IOException e)
        {
            return false;
        }
        return true;
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }



}
