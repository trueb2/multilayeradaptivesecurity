package uiuc.research.appguardian.questions.locationquestions;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import uiuc.research.appguardian.database.Location;


public class Week
{

    protected ArrayList<Location> monday, tuesday, wednesday,
            thursday, friday, saturday, sunday;
    protected Calendar cal;

    public Week(ArrayList<Location> locs)
    {
        monday = new ArrayList<Location>();
        tuesday = new ArrayList<Location>();
        wednesday = new ArrayList<Location>();
        thursday = new ArrayList<Location>();
        friday = new ArrayList<Location>();
        saturday = new ArrayList<Location>();
        sunday = new ArrayList<Location>();

        cal = Calendar.getInstance();

        for (Location loc : locs)
        {
            //find out what day it is in and put it in the associated list
            cal.clear();
            cal.setTimeInMillis(loc.getTimestamp());
            cal.setTimeZone(TimeZone.getTimeZone("CMT"));
            int day = cal.get(Calendar.DAY_OF_WEEK);
            switch (day)
            {
                case Calendar.MONDAY:
                    monday.add(loc);
                    break;
                case Calendar.TUESDAY:
                    tuesday.add(loc);
                    break;
                case Calendar.WEDNESDAY:
                    wednesday.add(loc);
                    break;
                case Calendar.THURSDAY:
                    thursday.add(loc);
                    break;
                case Calendar.FRIDAY:
                    friday.add(loc);
                    break;
                case Calendar.SATURDAY:
                    saturday.add(loc);
                    break;
                case Calendar.SUNDAY:
                    sunday.add(loc);
                    break;
                default:
                    System.out.println("That is not a day of the week ??? ");
                    break;
            }
        }
    }

    public ArrayList<Location> allWeek()
    {
        ArrayList<Location> week = new ArrayList<Location>();
        week.addAll(monday);
        week.addAll(tuesday);
        week.addAll(wednesday);
        week.addAll(thursday);
        week.addAll(friday);
        week.addAll(saturday);
        week.addAll(sunday);
        return week;
    }


}
