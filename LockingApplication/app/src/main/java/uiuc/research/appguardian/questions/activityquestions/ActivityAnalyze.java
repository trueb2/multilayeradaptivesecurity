package uiuc.research.appguardian.questions.activityquestions;

import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

import uiuc.research.appguardian.database.Action;
import uiuc.research.appguardian.questions.Question;
import uiuc.research.appguardian.services.QuestionService;
import uiuc.research.appguardian.util.Util;

/**
 * Created by jwtrueb on 11/25/14.
 */
public class ActivityAnalyze
{

    private static Calendar cal = Calendar.getInstance();
    private static ActivityDatabase ad = new ActivityDatabase();
    private static ArrayList<ConstantAction> cas = null;

//    /**
//     * Reads the database into memory
//     */
//    public static void prepare(int daysBack)
//    {
//        try
//        {
//            ad.readAllAfter(daysBack);
//        }
//        catch (FileNotFoundException fnfe)
//        {
//            fnfe.printStackTrace();
//        }
//    }

    /**
     * Will generate the questions "When did you walk the most" and
     * "When did you drive the most"
     * <p/>
     * This should not be called unless the databases are already
     * loaded otherwise the would be a lengthy delay
     *
     * @param daysBack
     * @param upto
     * @return returns the questions
     */
//    public static ArrayList<Question> generateAllQuestions(int daysBack, int upto)
//    {
//
//        if (!ad.isReady())
//        {
//            //Log.d(Util.TAG, "Activity Database is not ready");
//        }
//
//        long start = System.currentTimeMillis();
//        Question question = whendYouWalkMost(daysBack, upto);
//        long stop = System.currentTimeMillis();
//        //Log.d("Timer", "Walk Most: " + (stop - start));
//        start = System.currentTimeMillis();
//        Question question1 = whendYouDriveMost(daysBack, upto);
//        stop = System.currentTimeMillis();
//        //Log.d("Timer", "Drive Most: " + (stop - start));
//
//        ArrayList<Question> questions = new ArrayList<Question>();
//        questions.add(question);
//        questions.add(question1);
//
//        return questions;
//    }

    /**
     * generates a Questions that asks when the user has driven the most
     * over the given period of time
     *
     * @param time0 the number of days back to consider
     * @param time1 the number of days back to stop considering
     * @return Question when drove the most
     * @throws java.io.FileNotFoundException
     */
    public static Question whendYouDriveMost(int time0, int time1, int questionType)
    {
        long start = System.currentTimeMillis();
        cas = ad.findConstantActions();
        long stop = System.currentTimeMillis();
        //Log.d("Timer", "Read in Constant Actions took: " + (stop - start));
        ArrayList<Integer> drive = actionsPerHour(cas, 0); //driving
        int enoughData = 0;
        for(int i = 0; i < 24; i++)
        {
            enoughData += drive.get(i);
        }
        if(enoughData < 5)
            return new Question(null, null, null, questionType);

        int hour = 0; //hour with the greatest count
        int high = 0; //highest count so far
        int low = Integer.MAX_VALUE; //lowest count so far
        int lowHour = 0; //hour with the lowest count
        for (int j = 0; j < 24; j++)
        {
            int count = drive.get(j);
            if (count > high)
            {
                high = count;
                hour = j;
            }

            if (count < low)
            {
                low = count;
                lowHour = j;
            }
        }

        String query;
        if (time1 == 0)
        {
            query = "During what hour of the day have you driven the most over the past " + time0 + " days?";
        } else
        {
            query = "What hour of the day did you drive the most during " + time0 + " and " + time1 + " day(s) ago?";
        }
        String answer = hourto12(hour);

        ArrayList<String> options = loopForOptions(drive, answer, hour, lowHour);

        //now we have a query, an answer, and 4 options
        Collections.shuffle(options);

        Question question = new Question(query, answer, options, questionType);
        return question;
    }

    /**
     * generates a Questions that asks when the user has been on foot the
     * most with his or her phone over the given time period
     *
     * @param time0 the number of days back to consider
     * @param time1 the number of days back to stop considering
     * @return Question when were you on foot the most
     * @throws java.io.FileNotFoundException
     */
    public static Question whendYouWalkMost(int time0, int time1, int questionType)
    {
        long start = System.currentTimeMillis();
        cas = ad.findConstantActions();
        long stop = System.currentTimeMillis();
        //Log.d("Timer", "Read in Constant Actions took: " + (stop - start));
        ArrayList<Integer> foot = actionsPerHour(cas, 2); //onFoot
        ArrayList<Integer> walk = actionsPerHour(cas, 7);//walking

        for (int i = 0; i < 24; i++)
        {
            walk.set(i, walk.get(i) + foot.get(i));
//            //Log.d(Util.TAG, walk.get(i) + " walking");
        }

        int enoughData = 0;
        for(int i = 0; i < 24; i++)
        {
            enoughData += walk.get(i);
        }
        if(enoughData < 5)
            return new Question(null, null, null, questionType);

        int hour = 0; //hour with the greatest count
        int high = 0; //highest count so far
        int low = Integer.MAX_VALUE; //lowest count so far
        int lowHour = 0; //hour with the lowest count
        for (int j = 0; j < 24; j++)
        {
            int count = walk.get(j);
            if (count > high)
            {
                high = count;
                hour = j;
            }

            if (count < low)
            {
                low = count;
                lowHour = j;
            }
        }
        String query;
        if (time1 == 0)
        {
            query = "During what hour of the day have you walked the most over the past " + time0 + " days?";
        } else
        {
            query = "What hour of the day did you walk the most during " + time0 + " and " + time1 + " day(s) ago?";
        }
        String answer = hourto12(hour);
        ArrayList<String> options = loopForOptions(walk, answer, hour, lowHour);
        //Log.d(Util.TAG, "Looped for options");
        //now we have a query, an answer, and 4 options
        Collections.shuffle(options);
        //Log.d(Util.TAG, "Shuffled options");
        Question question = new Question(query, answer, options, questionType);
        return question;
    }

    /**
     * takes a number between 0 and 23 inclusive and returns a number between 1 and 12
     * inclusive followed by AM or PM
     *
     * @param hour of day
     * @return string of 12 hour format
     */
    public static String hourto12(int hour)
    {
        if (hour == 0)
        {
            return "12 AM";
        } else if (hour == 12)
        {
            return "12 PM";
        } else
        {
            return hour % 12 + ((hour - 12 > 0) ? " PM" : " AM");
        }
    }

    // does searching for good options that would normally just go in the question generating method but it was lengthy
    private static ArrayList<String> loopForOptions(ArrayList<Integer> walk, String answer, int hour, int lowHour)
    {
        ArrayList<String> options = new ArrayList<String>(); //list of options to be returned
        HashSet<Integer> assessed = new HashSet<Integer>();  //keep track of options to prevent ones that are too close
        options.add(hourto12(hour)); //put the highest hour as an option
        assessed.add(hour); //note that an option has already been added
        if(Math.abs((lowHour % 12) - (hour % 12)) > 2 && Math.abs(lowHour - hour) > 2) { //put the lowest hour in if it isnt too close
            options.add(hourto12(lowHour));
            assessed.add(lowHour);
        }

        optionSearch:   //randomly choose an hour, if it isnt too close use it as an option
        while(options.size() < 4)
        {
            int rando = (int) (Math.random() * 1000);
            rando = rando % 24;
            if(assessed.contains(rando) || options.contains(hourto12(rando)))    //already used before
                continue;
            else {
                for (Integer i : assessed) {
                    if (Math.abs(i - rando) < 3) { //3 plus hours difference
                        continue optionSearch;
                    }
                }
                options.add(hourto12(rando));
                assessed.add(rando);
            }

        }

        return options;
    }

    public static ArrayList<Integer> actionsPerHour(ArrayList<ConstantAction> constantActions, int type)
    {
        ArrayList<Integer> counts = new ArrayList<Integer>(24);
        for (int j = 0; j < 24; j++)
        {
            counts.add(0);
        }


        for(ConstantAction ca : constantActions)
        {
//            //Log.d(Util.TAG, ca.toString());
            if(ca.type != type)
                continue;

            long first = ca.actions.get(0).getTimestamp();
            long last = ca.actions.get(ca.actions.size() - 1).getTimestamp();

            cal.setTime(new Date(first));
            int hour0 = cal.get(Calendar.HOUR_OF_DAY);
            cal.setTime(new Date(last));
            int hour1 = cal.get(Calendar.HOUR_OF_DAY);

            //works so just dont touch
            if(hour0 > hour1)
                hour1 += 24;

            for(int i = 0; i < 48; i++)
            {
                if(hour0 <= i && i <= hour1)
                {
                    counts.set(i%24, counts.get(i%24)+1);
                }
            }
        }
//        //total the actions per hour
//        for (ConstantAction ca : constantActions)
//        {
//            if (ca.type != type) //not the type that we are trying total
//            {
//                continue;
//            }
//
//            ArrayList<Boolean> hours = findHours(ca.actions);
//            for (int i = 0; i < hours.size(); i++)
//            {
//                boolean b = hours.get(i);
//                if (b)
//                {
//                    counts.set(i, counts.get(i) + 1);//increment count
//                }
//            }
//        }
        return counts;
    }


    /**
     * the arraylist returned will have 24 indexes each representing an hour in the day
     * if the hour did not contain the action then its index will be set to false
     * if true then the action was ongoing during the hour
     *
     * @param actions
     * @return actions ongoing hours
     */
    public static ArrayList<Boolean> findHours(ArrayList<Action> actions)
    {
        //find out what hours are between the first and last timestamp
        long first = actions.get(0).getTimestamp();
        long last = actions.get(actions.size() - 1).getTimestamp();

        cal.setTime(new Date(first));
        int hour0 = cal.get(Calendar.HOUR_OF_DAY);
        cal.setTime(new Date(last));
        int hour1 = cal.get(Calendar.HOUR_OF_DAY);
        ArrayList<Boolean> hours = new ArrayList<Boolean>(24);
        for (int i = 0; i < 24; i++)
        {
            if (i < hour0)
            {
                hours.add(false);
            } else if (i >= hour0 && i <= hour1)
            {
                hours.add(true);
            } else if (i > hour1)
            {
                hours.add(false);
            }
        }

        return hours;
    }

    public static void prepareDatabase(ArrayList<Action> actions)
    {
        ad.setActions(actions);
    }
}
