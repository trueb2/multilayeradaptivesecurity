package uiuc.research.appguardian.questions.activityquestions;

import android.util.Log;

import java.util.ArrayList;

import uiuc.research.appguardian.database.Action;
import uiuc.research.appguardian.util.Util;

public class ActivityDatabase
{

    private static ArrayList<Action> actions;
    private static ArrayList<ConstantAction> constantActions;
    private static boolean ready = false;

    public ActivityDatabase()
    {
        actions = new ArrayList<Action>();
        ready = false;
    }

//    /**
//     * Reads in all of the Actions that take place within a certain number
//     * of days specified by daysBack
//     *
//     * @param daysBack
//     * @throws java.io.FileNotFoundException
//     */
//    public void readAllAfter(int daysBack) throws FileNotFoundException
//    {
//        File exportDir = Environment.getExternalStoragePublicDirectory("MyCVS");
//        Scanner in = new Scanner(new FileReader(exportDir.getAbsolutePath() + "/activity.csv"));
//        long today = System.currentTimeMillis();
//        long msBack = LocationAnalyze.DAY_IN_MILLIS * daysBack;
//        if (actions == null)
//        {
//            actions = new ArrayList<Action>();
//        }
//
//        String line = in.nextLine();
//        while (in.hasNextLine())
//        {
//            int confidence;
//            int type;
//            long timestamp;
//            String[] parts = line.split(",");
//            timestamp = Long.parseLong(parts[2]);
//            if (timestamp < today - msBack)
//            {
//                line = in.nextLine();
//                continue;
//            }
//            type = Integer.parseInt(parts[1]);
//            confidence = Integer.parseInt(parts[0]);
//
//            Action action = new Action(confidence, type, timestamp);
//            actions.add(action);
//            line = in.nextLine();
//        }
//        ready = true;
//    }
//
//    /**
//     * should not be used regularly
//     * this reads in everything that there is in the database
//     *
//     * @throws java.io.FileNotFoundException
//     */
//    public void readAll() throws FileNotFoundException
//    {
//        readAllAfter(Integer.MAX_VALUE);
//    }

    /**
     * finds all sequences of repeated activities and creates a ConstantAction object
     * for each
     * Constant Actions may be long or very short. they should be cleaned or concatenated
     * for further analysis
     *
     * @return
     * @throws java.io.FileNotFoundException
     */
    public ArrayList<ConstantAction> findConstantActions()
    {
        long start = System.currentTimeMillis();
        if (actions == null)
        {
            //Log.d(Util.TAG, "Crap");
        }


/*        ArrayList<ConstantAction> constantActions = new ArrayList<ConstantAction>();
        Action pprev, previous = null, current = null;
        int addedPrev = -1;
        ArrayList<Action> sameActions = new ArrayList<Action>();
        for (int i = 0; i < actions.size(); i++)
        {
            pprev = previous;
            previous = current;
            current = actions.get(i);

            if (previous == null || pprev == null)
            {
                continue;
            }

            //.equals is overridden for Action
            if (previous.equals(current))
            {
                //if the previous one was the same type then record it
                sameActions.add(previous);
                addedPrev = i;
            }
            else if(pprev != null && pprev.equals(current) && i-1 != addedPrev)
            {
                //if there was one different type inbetween two of the same still record it
                sameActions.add(pprev);
            } else if (sameActions.size() != 0)
            {
                sameActions.add(previous); //the last current that had at least one pair
                ConstantAction constAction = new ConstantAction(sameActions);
                constantActions.add(constAction);
                sameActions = new ArrayList<Action>();
            }
        }
*/

        //Log.d(Util.TAG, "There are " + actions.size() + " different actions to be analyzed");
        ArrayList<ConstantAction> constantActions = new ArrayList<>();
        ArrayList<Action> sameActions = new ArrayList<>();
        int state  = 0;
        for(int i = 1; i < actions.size(); i++)
        {
            if(actions.get(i).getType() == actions.get(i-1).getType())
            {
                if(state == 0)
                {
                    sameActions = new ArrayList<>();
                    sameActions.add(actions.get(i-1));
                    state++;
                }
                else if(state == 1)
                {
                    state++;
                }
                sameActions.add(actions.get(i));
            }
            else
            {
                if(state == 2)
                {
                    state--;
                    sameActions.add(actions.get(i));
                }
                else if(state == 1)
                {
                    ConstantAction constAction = new ConstantAction(sameActions);
                    constantActions.add(constAction);
                    state--;
                }
            }
        }
        long stop = System.currentTimeMillis();
        int avg = 0;
        if(constantActions.size() == 0)
        {
            //Log.d(Util.TAG, "findConstantActions could not find any ConstantActions");
            return constantActions;
        }

        for(ConstantAction ca : constantActions)
        {
            avg += ca.actions.size();
        }
        avg = avg / constantActions.size();
        //Log.d(Util.TAG, "Calculating " + constantActions.size() + " constant actions of size " + avg + " took " + (stop-start));


        return constantActions;
    }

    /**
     * often just one or two readings may appear of a different type that would
     * have otherwise continued a constant action
     * this will bridge these disconnected Constant Actions together
     *
     * @param constActions
     * @return
     */
    public ArrayList<ConstantAction> concatenateConstantActions(ArrayList<ConstantAction> constActions)
    {
        ArrayList<ConstantAction> concatenated = new ArrayList<ConstantAction>();
        for (int i = 2; i < constActions.size(); i++)
        {
            ConstantAction twoback = constActions.get(i - 2);
            ConstantAction oneback = constActions.get(i - 1);
            ConstantAction current = constActions.get(i);

            //if the first and last match each other and the middle is very short concatenate
            if (twoback.type == current.type && oneback.actions.size() < 4)
            {
                ArrayList<Action> c = new ArrayList<Action>(twoback.actions);
                c.addAll(oneback.actions);
                c.addAll(current.actions);
                concatenated.add(new ConstantAction(c));
            }
        }

        return concatenated;
    }

    /**
     * returns an ArrayList<ConstantAction> that has been concatenated and
     * only includes ConstantActions with impurities that are below 25 percent and
     * average confidences that are abouve 60
     */
    public ArrayList<ConstantAction> cleanedAfterConcatenation(ArrayList<ConstantAction> constActions)
    {
        ArrayList<ConstantAction> constantActions = new ArrayList<ConstantAction>();
        for (ConstantAction ca : constActions)
        {
            if (ca.averageConfidence > 60 && ca.impurities < 25)
            {
                constantActions.add(ca);
            }
        }

        return constantActions;
    }

    /**
     * returns the safe ConstantAction
     * if they have not been found yet then the following will be called
     * findConstantActions()
     * concatenateConstantActions()
     * cleanedAfterConcatenation()
     *
     * @throws java.io.FileNotFoundException
     */
    public ArrayList<ConstantAction> getConstantActions()
    {
        if (constantActions == null)
        {
//            return cleanedAfterConcatenation(concatenateConstantActions(findConstantActions()));
//            return cleanedAfterConcatenation(findConstantActions());
            return findConstantActions();
        }

        return constantActions;
    }

    public static boolean isReady(){
        return ready;
    }

    public void setActions(ArrayList<Action> actions){
        this.actions = actions;
        ready = (actions == null) ? false : true;
    }
}
