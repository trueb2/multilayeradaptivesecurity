package uiuc.research.appguardian.ui;

public class NavigationElement
{

    public static final int TYPE_STATUS = 0;
    public static final int TYPE_APPS = 1;
    public static final int TYPE_CHANGE = 2;
    public static final int TYPE_SETTINGS = 3;
    public static final int TYPE_STATISTICS = 4;
    public static final int TYPE_TEST = 6;
    public static final int TYPE_QUESTIONS = 7;
    public static final int TYPE_DATABASE = 8;
    public static final int TYPE_LOG_TYPES = 9;
    public static final int TYPE_LOG_LOC = 10;
    public static final int TYPE_LOG_ACT = 11;
    public static final int TYPE_LOG_BROWSE = 12;
    public static final int TYPE_LOG_SCREEN = 13;
    public static final int TYPE_LOG_PROC = 14;
    public static final int TYPE_LOG_CALL = 15;
    public static final int TYPE_LOG_SMS = 16;
    public static final int TYPE_POSE_Q = 17;
    public static final int TYPE_D_TO_S = 18;

    public int type;

    public int title;

}
