package uiuc.research.appguardian.ui;

import android.app.Activity;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

import uiuc.research.appguardian.R;
import uiuc.research.appguardian.database.Action;
import uiuc.research.appguardian.database.DatabaseHandler;
import uiuc.research.appguardian.servercommunications.EncryptedPost;
import uiuc.research.appguardian.services.AppLockService;
import uiuc.research.appguardian.services.LockService;
import uiuc.research.appguardian.services.LoggingService;
import uiuc.research.appguardian.services.QuestionService;
import uiuc.research.appguardian.services.RecognitionService;
import uiuc.research.appguardian.ui.fragments.AppsFragment;
import uiuc.research.appguardian.ui.fragments.NavigationFragment;
import uiuc.research.appguardian.ui.fragments.SettingsFragment;
import uiuc.research.appguardian.ui.fragments.StatisticsFragment;
import uiuc.research.appguardian.util.DialogSequencer;
import uiuc.research.appguardian.util.PrefUtils;
import uiuc.research.appguardian.util.Util;

public class MainActivity extends ActionBarActivity
        implements NavigationFragment.NavigationListener
{

    public static final String EXTRA_UNLOCKED = "com.twinone.locker.unlocked";
    public static String PACKAGE_NAME;
    public static final String FIRST_TIME_USER = "first_time_user";
    int mCurrentFragmentType;
    int mNavPendingType = -1;
    private DialogSequencer mSequencer;
    private Fragment mCurrentFragment;
    /**
     * Fragment managing the behaviors, interactions and presentation of the
     * navigation drawer.
     */
    private NavigationFragment mNavFragment;
    /**
     * Used to store the last screen title. For use in
     * restoreActionBar()
     */
    private CharSequence mTitle;
    private Thread mThread;
    private ActionBar mActionBar;
    private BroadcastReceiver mReceiver;
    private IntentFilter mFilter;
    private DatabaseHandler mDatabaseHandler = DatabaseHandler.getInstance(this);
    private boolean mNavPending;

    /**
     * Provide a way back to {@link uiuc.research.appguardian.ui.MainActivity} without having to provide a
     * password again. It finishes the calling {@link android.app.Activity}
     *
     * @param context
     */
    public static final void showWithoutPassword(Context context)
    {
        Intent i = new Intent(context, MainActivity.class);
        i.putExtra(EXTRA_UNLOCKED, true);
        if (!(context instanceof Activity))
        {
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        context.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        //Log.d(Util.TAG, "MainActivity onCreate");
        setContentView(R.layout.activity_main);
        handleIntent();

        PACKAGE_NAME = getApplicationContext().getPackageName();

        mReceiver = new ServiceStateReceiver();
        mFilter = new IntentFilter();
        mFilter.addCategory(AppLockService.CATEGORY_STATE_EVENTS);
        mFilter.addAction(AppLockService.BROADCAST_SERVICE_STARTED);
        mFilter.addAction(AppLockService.BROADCAST_SERVICE_STOPPED);

        // Access the default SharedPreferences
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean first_time = preferences.getBoolean(FIRST_TIME_USER, true);
        if(first_time)
        {

            // TODO: preferences not working correctly, inserting call history every time takes too much resources
            Util.fillCallHistory(this);
            Util.fillBrowserHistory(this);

            SharedPreferences.Editor editor = preferences.edit();

            editor.putBoolean(FIRST_TIME_USER, false);
            editor.putBoolean(getString(R.string.nav_location), true);
            editor.putBoolean(getString(R.string.nav_action), true);
            editor.putBoolean(getString(R.string.nav_browser_hist), true);
            editor.putBoolean(getString(R.string.nav_screen), true);
            editor.putBoolean(getString(R.string.nav_processes), true);
            editor.commit();
        }

        mNavFragment = (NavigationFragment) getSupportFragmentManager()
                .findFragmentById(R.id.navigation_drawer);
        // Set up the drawer.
        mNavFragment.setUp(R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        mTitle = getTitle();

        mActionBar = getSupportActionBar();
        mCurrentFragment = new AppsFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.container, mCurrentFragment).commit();
        mCurrentFragmentType = NavigationElement.TYPE_APPS;

        mSequencer = new DialogSequencer();
        showDialogs();
        showLockerIfNotUnlocked(false);

        Util.loadServicesInBackground(this);

        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

//        if(mWifi.isConnected())
//        {
//            mDatabaseHandler.replaceAddresses();
//        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        //Log.d(Util.TAG, "MainActivity onResume");
        showLockerIfNotUnlocked(true);
        registerReceiver(mReceiver, mFilter);
        updateLayout();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        // mSequencer.stop();
        LockService.hide(this);
        unregisterReceiver(mReceiver);
        mSequencer.stop();

        // We have to finish here or the system will assign a lower priority to
        // the app (since 4.4?)
        if (mCurrentFragmentType != NavigationElement.TYPE_SETTINGS)
        {
            finish();
        }
    }

    @Override
    protected void onDestroy()
    {
        Log.v("Main", "onDestroy");
        super.onDestroy();
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        //Log.d("", "onNewIntent");
        super.onNewIntent(intent);
        setIntent(intent);
        handleIntent();
    }

    @Override
    public void setTitle(CharSequence title)
    {
        super.setTitle(title);
        mTitle = title;
        getSupportActionBar().setTitle(title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.global, menu);
        return true;
    }

    public void setActionBarTitle(int resId)
    {
        mActionBar.setTitle(resId);
    }

    /**
     * @return True if the service is allowed to start
     */
    private boolean showDialogs()
    {
        boolean deny = false;

        // Recovery code
        mSequencer.addDialog(Dialogs.getRecoveryCodeDialog(this));

        // Empty password
        deny = Dialogs.addEmptyPasswordDialog(this, mSequencer);

        mSequencer.start();
        return !deny;
    }

    private void showLockerIfNotUnlocked(boolean relock)
    {
        boolean unlocked = getIntent().getBooleanExtra(EXTRA_UNLOCKED, false);
        if (new PrefUtils(this).isCurrentPasswordEmpty())
        {
            unlocked = true;
        }
        if (!unlocked)
        {
            LockService.showCompare(this, getPackageName());
        }
        getIntent().putExtra(EXTRA_UNLOCKED, !relock);
    }

    private void updateLayout()
    {
        //Log.d("Main",
//                "UPDATE LAYOUT Setting service state: "
//                        + AppLockService.isRunning(this));
        mNavFragment.getAdapter().setServiceState(
                AppLockService.isRunning(this));
    }

    /**
     * Handle this Intent for searching...
     */
    private void handleIntent()
    {
        if (getIntent() != null && getIntent().getAction() != null)
        {
            if (getIntent().getAction().equals(Intent.ACTION_SEARCH))
            {
                //Log.d("MainActivity", "Action search!");
                if (mCurrentFragmentType == NavigationElement.TYPE_APPS)
                {
                    final String query = getIntent().getStringExtra(
                            SearchManager.QUERY);
                    if (query != null)
                    {
                        ((AppsFragment) mCurrentFragment).onSearch(query);
                    }
                }
            }
        }
    }

    public boolean onNavigationElementSelected(int type)
    {
        if (type == NavigationElement.TYPE_TEST)
        {
            // Test something here
            return false;
        } else if (type == NavigationElement.TYPE_STATUS)
        {
            toggleService();
            return false;
        }
        else if(type == NavigationElement.TYPE_LOG_LOC)
        {
            changeTypesToLog(getString(R.string.nav_location));
            return false;
        }
        else if(type == NavigationElement.TYPE_LOG_ACT)
        {
            changeTypesToLog(getString(R.string.nav_action));
            return false;
        }
        else if(type == NavigationElement.TYPE_LOG_BROWSE)
        {
            changeTypesToLog(getString(R.string.nav_browser_hist));
            return false;
        }
        else if(type == NavigationElement.TYPE_LOG_PROC)
        {
            changeTypesToLog(getString(R.string.nav_processes));
            return false;
        }
        else if(type == NavigationElement.TYPE_LOG_SCREEN)
        {
            changeTypesToLog(getString(R.string.nav_screen));
            return false;
        }
        else if(type == NavigationElement.TYPE_LOG_CALL) {
            changeTypesToLog(getString(R.string.nav_callhistory));
            return false;
        }
        else if(type == NavigationElement.TYPE_LOG_SMS)
        {
            changeTypesToLog(getString(R.string.nav_smshistory));
            return false;
        }

        mNavPending = true;
        mNavPendingType = type;
        return true;
    }

    private void toggleService()
    {
        boolean newState = false;
        if (AppLockService.isRunning(this))
        {
            //Log.d("", "toggleService() Service is running, now stopping");
            AppLockService.stop(this);
        } else if (Dialogs.addEmptyPasswordDialog(this, mSequencer))
        {
            mSequencer.start();
        } else
        {
            newState = AppLockService.toggle(this);
        }
        if (mNavFragment != null)
        {
            mNavFragment.getAdapter().setServiceState(newState);
        }
    }

    private void changeTypesToLog(String key)
    {
        //get the old value
        SharedPreferences prefs = PreferenceManager.
                getDefaultSharedPreferences(getApplicationContext());
        boolean previous = prefs.getBoolean(key, false);


        //store the opposite of the old value
        SharedPreferences.Editor prefEditor = prefs.edit();
        //prefEditor.clear();
        prefEditor.putBoolean(key, !previous);
        prefEditor.commit();

        //Log.d(Util.TAG, "Before " + previous + " and now " + prefs.getBoolean(key, true));

        //reflect change in the gui
        mNavFragment.getAdapter().notifyDataSetChanged();

        //restart logging services
        if(LoggingService.isRunning())
            LoggingService.stopLogging();
        if(RecognitionService.isRunning())
            RecognitionService.stopLogging();
        if(QuestionService.isRunning())
            QuestionService.stop();

        Util.loadServicesInBackground(this);
    }
    @Override
    public void onDrawerOpened(View drawerView)
    {
        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    public void onDrawerClosed(View drawerView)
    {
        getSupportActionBar().setTitle(mTitle);
        if (mNavPending)
        {
            navigateToFragment(mNavPendingType);
            mNavPending = false;
        }
    }

    /**
     * Open a specific Fragment
     *
     * @param type
     */
    public void navigateToFragment(int type)
    {
        if (type == mCurrentFragmentType && type != NavigationElement.TYPE_QUESTIONS)
        {
            // Don't duplicate
            return;
        }
        if (type == NavigationElement.TYPE_CHANGE)
        {
            Dialogs.getChangePasswordDialog(this).show();
            // Don't change current fragment type
            return;
        }

        switch (type)
        {
            case NavigationElement.TYPE_APPS:
                mCurrentFragment = new AppsFragment();
                break;
            case NavigationElement.TYPE_SETTINGS:
                mCurrentFragment = new SettingsFragment();
                break;
            case NavigationElement.TYPE_STATISTICS:
                mCurrentFragment = new StatisticsFragment();
                break;
            case NavigationElement.TYPE_POSE_Q:
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                Intent intent = LockService.getLockIntent(this, PACKAGE_NAME);
                intent.setAction(LockService.ACTION_COMPARE);
                intent.putExtra(LockService.EXTRA_PACKAGENAME, PACKAGE_NAME);
                intent.putExtra(LockService.EXTRA_QUESTIONS_TO_ASK, prefs.getInt(PACKAGE_NAME, 8));
                startService(intent);
                //Log.d(Util.TAG, "Button was pressed for sure");
                mCurrentFragment = new AppsFragment();
                type = NavigationElement.TYPE_APPS;
                break;
            case NavigationElement.TYPE_D_TO_S:
                EncryptedPost ep = new EncryptedPost(this);
                ep.execute();
                mCurrentFragment = new AppsFragment();
                type = NavigationElement.TYPE_APPS;
                break;
//            case NavigationElement.TYPE_LOG_CALL:
                // TODO Replace this test function
                // Code from previous implementation, add all call log into database
//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Uri allCalls = Uri.parse("content://call_log/calls");
//                        Cursor cursor = managedQuery(allCalls, null, null, null, null);
//                        mDatabaseHandler.overwriteTable("callHistory");
//                        while (cursor.moveToNext()) {
////                    int id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(CallLog.Calls._ID)));
//                            String num = cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER));
//                            String name = cursor.getString(cursor.getColumnIndex(CallLog.Calls.CACHED_NAME));
//                            String duration = cursor.getString(cursor.getColumnIndex(CallLog.Calls.DURATION));
//                            String date = cursor.getString(cursor.getColumnIndex(CallLog.Calls.DATE));
//                            int callType = Integer.parseInt(cursor.getString(cursor.getColumnIndex(CallLog.Calls.TYPE)));
//                            CallHistory callHistory = new CallHistory(name, num, duration, date, callType);
//                            mDatabaseHandler.addCallHistory(callHistory);
//                            //Log.d(Util.TAG, "Insert call history: " + num + ", " + date);
//                        }
//                        // Get call history from database and generate questions
//                        ArrayList<CallHistory> callHistoryArrayList;
//                        callHistoryArrayList = mDatabaseHandler.getCallHistory(5);
////                        mDatabaseHandler.close();
//                    }
//                }).start();
//
//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Cursor mCur = managedQuery(Browser.BOOKMARKS_URI,
//                                Browser.HISTORY_PROJECTION, null, null, null);
//
//                        mCur.moveToFirst();
//                        if (mCur.moveToFirst() && mCur.getCount() > 0) {
//                            while (!mCur.isAfterLast()) {
//                                //Log.d(Util.TAG, "Browser history: " + mCur.getString(Browser.HISTORY_PROJECTION_TITLE_INDEX));
//                                mCur.moveToNext();
//                            }
//                        }
//                    }
//                }).start();
//
//                break;

        }
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.container, mCurrentFragment)
                .commit();
        mCurrentFragmentType = type;
    }

    @Override
    public void onShareButton()
    {
        // Don't add never button, the user wanted to share
//        Dialogs.getShareEditDialog(this, false).show();
    }

    @Override
    public void onRateButton()
    {
        toGooglePlay();
    }

    private void toGooglePlay()
    {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("market://details?id=" + getPackageName()));
        if (getPackageManager().queryIntentActivities(intent,
                PackageManager.MATCH_DEFAULT_ONLY).size() >= 1)
        {
            startActivity(intent);
        }
    }

    /**
     * overwrites old database tables
     * fills the database with the contents of the csv files
     * the filling is done in transactions of 1000 as long
     * as the database isnt locked upon request
     */
    public void fillDatabase()
    {
        mDatabaseHandler = DatabaseHandler.getInstance(this);
        try
        {
            //activities
//            readActionDatabase();

            //locations
//            readLocationDatabase();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void fillDatabaseAddresses()
    {
        mDatabaseHandler = DatabaseHandler.getInstance(this);
        mDatabaseHandler.replaceAddresses();
    }

    private void readActionDatabase() throws FileNotFoundException
    {
        mDatabaseHandler.overwriteTable(mDatabaseHandler.TABLE_ACTION);
        //Log.d(Util.TAG, "Dumped Action Table");

        File exportDir = Environment.getExternalStoragePublicDirectory("MyCVS");
        Scanner in = new Scanner(new FileReader(exportDir.getAbsolutePath() + "/activity.csv"));
        int c = 0;
        while (in.hasNextLine())
        {
            String s = in.nextLine();
            String[] a = s.split(",");
            Action activity =
                    new Action(Integer.parseInt(a[0]),
                            Integer.parseInt(a[1]), Long.parseLong(a[2]));
            if (in.hasNextLine())
            {
                mDatabaseHandler.addActionFast(activity, false);
                c++;
            } else
            {
                try
                {
                    mDatabaseHandler.addActionFast(activity, true);
                    c = 0;
                }
                catch (SQLiteDatabaseLockedException e)
                {
                    mDatabaseHandler.addActionFast(activity, false);
                }
            }
        }
        in.close();

        //Log.d(Util.TAG, "Added All Activities");
    }

    private void readLocationDatabase() throws FileNotFoundException
    {
        mDatabaseHandler.overwriteTable(mDatabaseHandler.TABLE_LOCATION);
        //Log.d(Util.TAG, "Dumped Location Table");
        File exportDir = Environment.getExternalStoragePublicDirectory("MyCVS");
        Scanner in = new Scanner(new FileReader(exportDir.getAbsolutePath() + "/location.csv"));
        int c = 0;
        long lastLog = Long.MAX_VALUE;
        while (in.hasNextLine())
        {
            String s = in.nextLine();
            String[] a = s.split(",");
            uiuc.research.appguardian.database.Location location =
                    new uiuc.research.appguardian.database.Location(Double.parseDouble(a[0]),
                            Double.parseDouble(a[1]), a[2], Long.parseLong(a[3]));



            if (lastLog + 15000 > location.getTimestamp() || location.getTimestamp() < System.currentTimeMillis() - 7 * Util.DAY_IN_MILLIS)
            {
                //not adding too many locations that were logged very close together
                continue;
            }

            lastLog = location.getTimestamp();

            if (in.hasNextLine() && c < 1000)
            {
                mDatabaseHandler.addLocationFast(location, false);
                c++;
            } else
            {
                try
                {
                    mDatabaseHandler.addLocationFast(location, true);
                    c = 0;
                }
                catch (SQLiteDatabaseLockedException e)
                {
                    mDatabaseHandler.addLocationFast(location, false);
                }
            }
        }
        in.close();
        //Log.d(Util.TAG, "Added All Locations");
    }

    private class ServiceStateReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            //Log.d("MainActivity",
//                    "Received broadcast (action=" + intent.getAction());
            updateLayout();
        }
    }
}
