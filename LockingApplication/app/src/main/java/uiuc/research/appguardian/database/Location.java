package uiuc.research.appguardian.database;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import uiuc.research.appguardian.questions.locationquestions.LocationDatabase;
import uiuc.research.appguardian.util.Util;

/**
 * Created by Qiuhua on 10/22/14.
 */
public class Location implements Loggable
{
    public static long donttryagain = 0;
    int id;
    String locationString;
    String address;
    long timestamp;
    double latitude;
    double longitude;
    //String date;
    double accuracy = 3; //number of decimal places of accuracy


    public Location()
    {
    }

    public Location(double lat, double lng, String address, long timestamp)
    {
        latitude = lat;
        longitude = lng;
        this.timestamp = timestamp;
        //this.date = LocationDatabase.convertStamp(timestamp);
        StringBuffer sb = new StringBuffer();
        sb.append(latitude);
        sb.append(",");
        sb.append(longitude);
        sb.append(",");
        if (address == null || address.length() < 5)
        { //null
            if (LocationDatabase.getAddresses() != null && LocationDatabase.getAddresses().containsKey(this))
            {
                this.address = LocationDatabase.getAddresses().get(this);
            } else
            {
                if (donttryagain > System.currentTimeMillis())
                {
                    this.address = "null";
                    return;
                }
                this.address = getRoadName(lat + "", longitude + "");
                if (address == null || address.equals("null"))
                {
                    donttryagain = System.currentTimeMillis();
                    address = "null";
                }
            }
        } else
        {
            this.address = address;
        }
        sb.append(address);
        sb.append(",");
        sb.append(timestamp);
        locationString = sb.toString();
    }

    public Location(String locationString)
    {
        this.locationString = locationString;
    }

    public static String getRoadName(String latitude, String longitude)
    {

        String roadName = "";
        String url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=__LAT__,__LNG__&sensor=false";

        url = url.replaceAll("__LAT__", latitude.replaceAll(" ", ""));
        url = url.replaceAll("__LNG__", longitude.replaceAll(" ", ""));

        DefaultHttpClient httpclient = new DefaultHttpClient(new BasicHttpParams());
        HttpGet httpget = new HttpGet(url);

        InputStream inputStream = null;
        String result = null;
        try
        {
            HttpResponse response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            inputStream = entity.getContent();
            // json is UTF-8 by default
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            result = sb.toString();
        }
        catch (Exception e)
        {
            // Oops
        }
        finally
        {
            try
            {
                if (inputStream != null)
                {
                    inputStream.close();
                }
            }
            catch (Exception squish)
            {
            }
        }

        try
        {
            if (result == null)
            {
                //Log.d(Util.TAG, "Result is null when requesting road names " + latitude + ", " + longitude);
                return null;
            }
            if (result.equals("{\n   \"error_message\" : \"You have exceeded your daily request quota for this API.\",\n   \"results\" : [],\n   \"status\" : \"OVER_QUERY_LIMIT\"\n}\n"))
            {
                //Log.d(Util.TAG, "Exceeded daily request quota");
                return null;
            }

            JSONObject jObject = new JSONObject(result);
            JSONArray jArray = jObject.getJSONArray("results");

            if (jArray == null && jArray.length() <= 0)
            {
                //Log.d(Util.TAG, "This road name result page does not have a jArray results");
                return null;
            }

            for (int i = 0; i < jArray.length(); i++)
            {
                try
                {
                    JSONObject oneObject = jArray.getJSONObject(i);
                    // Pulling items from the array
                    roadName = oneObject.getString("formatted_address");
                    roadName = roadName.replaceAll(",", " |");
                    return roadName;
                }
                catch (JSONException e)
                {
                    // Oops
                }
            }
        }
        catch (JSONException e)
        {//this wont get thrown
            e.printStackTrace();
        }

        //Log.d(Util.TAG, "Finished all Road Name getting logic and returning null");
        return null;
    }

    /**
     * compares the Location to another Location by timestamp
     */
    public int compareTime(Location loc)
    {
        if (timestamp < loc.getTimestamp())
        {
            return -1;
        } else if (timestamp > loc.getTimestamp())
        {
            return 1;
        } else
        {
            return 0;
        }
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof Location))
        {
            return false;
        }

        Location loc = (Location) o;

        double dx = Math.abs(Math.abs(this.latitude) - Math.abs(loc.getLatitude())); //wont work for opposite sides of the globe
        double dy = Math.abs(Math.abs(this.longitude) - Math.abs(loc.getLongitude())); //wont work for opposite sides of the globe

        double tolerance = Math.pow(10, -1d * accuracy);

        return dx < tolerance && dy < tolerance;
    }

    @Override
    public int hashCode()
    {
        Double bufferedLat = ((double) ((int) (latitude * Math.pow(10, accuracy)))) / Math.pow(10, accuracy);
        Double bufferedLng = ((double) ((int) (longitude * Math.pow(10, accuracy)))) / Math.pow(10, accuracy);

        return bufferedLat.hashCode() + bufferedLng.hashCode();
    }

    @Override
    public String toString()
    {
        return locationString;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public long getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(long timestamp)
    {
        this.timestamp = timestamp;
    }

//    public String getDate()
//    {
//        return date;
//    }

//    public void setDate(String date)
//    {
//        this.date = date;
//    }

    public String getAddress(boolean lookUp)
    {
        if (!lookUp)
        {
            return getAddress();
        }

        if (address != null && address.length() > 20)
        {
            return address;
        } else
        {
            String address = getRoadName(latitude + "", longitude + "");
            if (address == null)
            {
                return "null";
            }
            return address;
        }
    }

    public String getAddress()
    {
        if (address != null)
        {
            return address;
        } else
        {
            return latitude + "," + longitude;
        }
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getLocationString()
    {
        return locationString;
    }

    public void setLocationString(String locationString)
    {
        this.locationString = locationString;
    }

    public double getLongitude()
    {
        return longitude;
    }

    public void setLongitude(double longitude)
    {
        this.longitude = longitude;
    }

    public double getLatitude()
    {
        return latitude;
    }

    public void setLatitude(double lat)
    {
        latitude = lat;
    }

    public String logLine()
    {
        return timestamp + ", " + latitude + ", " + longitude +
                ", " + address + "\n";
    }

}
