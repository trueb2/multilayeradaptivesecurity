package uiuc.research.appguardian.questions.screenquestions;

import uiuc.research.appguardian.database.Loggable;

/**
 * Created by Jacob on 2/26/2015.
 */
public class Switch implements Loggable
{
    public static final int ON = 1;
    public static final int OFF = 0;

    private int type;
    private long timestamp;

    public Switch(int type, long timestamp)
    {
        this.type = type;
        this.timestamp = timestamp;
    }

    public int getType()
    {
        return type;
    }

    public long getTimestamp()
    {
        return timestamp;
    }

    public String logLine()
    {
        return type + ", " + timestamp;
    }
}
