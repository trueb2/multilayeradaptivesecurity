package uiuc.research.appguardian.questions.processesquestions;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Formatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeMap;
import java.util.TreeSet;

import uiuc.research.appguardian.database.Processes;
import uiuc.research.appguardian.questions.Question;
import uiuc.research.appguardian.services.QuestionService;
import uiuc.research.appguardian.util.Util;

/**
 * Created by jwtrueb on 4/29/15.
 */
public class ProcAnalyze {
    private static ProcDatabase pd = new ProcDatabase();

    /**
     * Determines which apps spent the most time in the foreground
     * for one period of time
     */
    public static Question longestUsed()
    {
        if(pd.isEmpty())
            return new Question("Database not loaded",
                    null, null, QuestionService.QT_LONGEST_USED_APP);

        //get all of the processes that were in the foreground
        TreeSet<Processes> foreground = pd.getForeground();
        //get timestamps of each time the processes were logged
        ArrayList<Long> logFreq = pd.getLogFrequency();
        if(logFreq == null)
            return new Question("Database not loaded",
                    null, null, QuestionService.QT_LONGEST_USED_APP);
        //Log.d(Util.TAG, "There were " + logFreq.size() + " mass logs of running processes");

        //create a hashmap that pairs a processes with its longest duration in the foreground
        HashMap<String, Long> constantDurations = new HashMap<String, Long>();
        HashMap<String, Long> durationPairs = new HashMap<String, Long>();

        /*
         * For Each Process in foreground HashSet
         * Check to see if it is the first instance of the process
         *      a. If it is the first instance, store the first time that the process
         *          entered the foreground
         *      b. If it is the second instance, see if the process left the foreground by
         *          finding a timestamp in logFreq between the value already stored and
         *          the process's timestamp. If this is the case then store the difference
         *          between the start and the inbetween timestamp as the duration of the process
         *  If multiple durations are found store the largest duartion in constantDurations
         */
        for(Processes p : foreground)
        {
            Long val = durationPairs.get(p.getProcessName());
            if(val == null)
                durationPairs.put(p.getProcessName(), p.getCreatedAt());
            else {
                long end = findInBetween(logFreq, val, p.getCreatedAt());
                if(end != p.getCreatedAt())
                {
                    //process left foreground; record time spent in foreground if longest so far
                    Long previousDuration = constantDurations.get(p.getProcessName());
                    if(previousDuration == null || previousDuration < (end - p.getCreatedAt()))
                        constantDurations.put(p.getProcessName(), end - p.getCreatedAt());

                    //remove previous pairing
                    durationPairs.remove(p.getProcessName());
                }
            }
        }

        /*
         * Store Durations in TreeMap to order
         * and retrive highest and lowest Durations
         */
        TreeMap<Long, String> processesByDuration = new TreeMap<>();
        for(String name : constantDurations.keySet())
        {
            if(name.equals(getAppName(name))  ||
                    getAppName(name).equals("Android System") ||
                    getAppName(name).equals("System UI") ||
                    nameInMap(getAppName(name), processesByDuration))
                continue;
            processesByDuration.put(constantDurations.get(name), name);
//            //Log.d(Util.TAG, name + " " + getAppName(name) + " " + constantDurations.get(name));
        }

        if(processesByDuration.size() < 4)
            return new Question(null, null, null, QuestionService.QT_LONGEST_USED_APP);


        //Log.d(Util.TAG, processesByDuration.size() + " is the size of the options for longest used");
        String greatest = getAppName(processesByDuration.get(processesByDuration.lastKey())); //largest key's value
        String worst = getAppName(processesByDuration.get(processesByDuration.firstKey())); //smallest key's value
        processesByDuration.remove(processesByDuration.firstKey());
        String secondWorst = getAppName(processesByDuration.get(processesByDuration.firstKey()));//new smallest key's value
        processesByDuration.remove(processesByDuration.firstKey());
        String thirdWorst  = getAppName(processesByDuration.get(processesByDuration.firstKey()));//even newer smallest key's value

        //set up questions
        String query = "Which app did you use for the longest time in the past day?";
        String answer = greatest;
        ArrayList<String> options = new ArrayList<>();

        options.add(greatest);
        options.add(worst);
        options.add(secondWorst);
        options.add(thirdWorst);

//        Collections.shuffle(options);

        return new Question(query, answer, options, QuestionService.QT_LONGEST_USED_APP);
    }

    /**
     * Determines which apps spent the most time in the foreground over
     * a given period of time which may or may not be the same as longestused
     */
    public static Question mostOftenUsed()
    {
        if(pd.isEmpty())
            return new Question("Database not loaded",
                    null, null, QuestionService.QT_MOST_OFTEN_USED_APP);

        //get all of the processes that were in the foreground
        TreeSet<Processes> foreground = pd.getForeground();
        //get timestamps of each time the processes were logged
        ArrayList<Long> logFreq = pd.getLogFrequency();
        if(logFreq == null)
            return new Question("Database not loaded",
                    null, null, QuestionService.QT_LONGEST_USED_APP);
        //Log.d(Util.TAG, "There were " + logFreq.size() + " mass logs of running processes");
        //create a hashmap that pairs a processes with its longest duration in the foreground
        HashMap<String, Long> totalDuration = new HashMap<String, Long>();
        HashMap<String, Long> durationPairs = new HashMap<String, Long>();

        /*
         * Check to see if the process is entering the foreground for the first time
         * If so, then store the timestamp in the durationPairs
         * Else
         * Sum the difference of durationPair's starting timestamp and <Either> with
         *      the duration in totalDuration
         * Set <Either> as the starting timestamp for durationPair
         *
         *      Either:
         *          the new timestamp
         *          or
         *          the first timestamp that is inbetween the old and the new timestamp
         *
         */
        for(Processes p : foreground)
        {
            Long start = durationPairs.get(p.getProcessName());
            if(start == null)
            {
                durationPairs.put(p.getProcessName(), p.getCreatedAt());
            }
            else
            {
                Long end = findInBetween(logFreq, start, p.getCreatedAt());
                Long diff = end - start;
                Long duration = totalDuration.get(p.getProcessName());

                if(duration == null)
                    totalDuration.put(p.getProcessName(), diff);
                else
                    totalDuration.put(p.getProcessName(), diff + duration);

                durationPairs.put(p.getProcessName(), p.getCreatedAt());
            }
        }

        /*
         * Store durations in TreeMap to order and determine greatest and
         * smallest durations
         */
        TreeMap<Long, String> processesByDuration = new TreeMap<>();
        for(String name : totalDuration.keySet())
        {
            if(name.equals(getAppName(name)) ||
                    getAppName(name).equals("Android System") ||
                    getAppName(name).equals("System UI") ||
                    nameInMap(getAppName(name), processesByDuration))
                continue;
            processesByDuration.put(totalDuration.get(name), name);
//            //Log.d(Util.TAG, name + " " + getAppName(name) + " " + totalDuration.get(name));
        }


        if(processesByDuration.size() < 4)
            return new Question(null, null, null, QuestionService.QT_MOST_OFTEN_USED_APP);

        //Log.d(Util.TAG, processesByDuration.size() + " is the size of the options for most often used");
        String greatest = getAppName(processesByDuration.get(processesByDuration.lastKey())); //largest key's value
        String worst = getAppName(processesByDuration.get(processesByDuration.firstKey())); //smallest key's value
        processesByDuration.remove(processesByDuration.firstKey());
        String secondWorst = getAppName(processesByDuration.get(processesByDuration.firstKey()));//new smallest key's value
        processesByDuration.remove(processesByDuration.firstKey());
        String thirdWorst  = getAppName(processesByDuration.get(processesByDuration.firstKey()));//even newer smallest key's value

        //set up questions
        String query = "Which app did you use the most over the course of the day?";
        String answer = greatest;
        ArrayList<String> options = new ArrayList<>();

        options.add(greatest);
        options.add(worst);
        options.add(secondWorst);
        options.add(thirdWorst);

        Collections.shuffle(options);

        return new Question(query, answer, options, QuestionService.QT_MOST_OFTEN_USED_APP);
    }


    public static long findInBetween(ArrayList<Long> logFreq, long start, long stop)
    {
        long end = stop;
        boolean foundStart = false;
        for(Long l : logFreq)
        {
            //find the l that is the start timestamp
            if(!foundStart && Math.abs(l - start) < 1000)
                foundStart = true;

            if(foundStart)
            {
                /*
                 * if there is a long from here until l == stop that is less than stop
                 * then the process left the foreground
                 */
                if(l + 1000 < stop)
                {
                    end = l;
                    break;
                }
                else if(stop + 1000 < l)
                {
                    //no timestamp inbetween
                    break;
                }
            }
        }

        return end;
    }

    /**
     * Uses the package name passed and PackageManger to try to
     * return the name of the application that the user would be
     * able to recognize
     * Returns the ApplicationLabel if it is found and the packageName
     * if it was not found
     */
    private static String getAppName(String packageName)
    {
        PackageManager pm = QuestionService.getInstance()
                                    .getApplicationContext().getPackageManager();
        ApplicationInfo ai = null;
        try
        {
            ai = pm.getApplicationInfo(packageName, 0);
        } catch(PackageManager.NameNotFoundException e)
        {
            ai = null;
        }
        String applicationName = (String) (ai == null ? packageName :
                                                        pm.getApplicationLabel(ai));

//        //Log.d(Util.TAG, packageName + " " + applicationName);
        return applicationName;
    }

    private static boolean nameInMap(String name, TreeMap<Long, String> map)
    {
        for(Long l : map.keySet())
        {
            if(map.get(l).equals(name))
                return true;
        }
        return false;
    }
    /**
     * Passes the processes on to the processes database where they can usefully be sorted
     */
    public static void fillDatabase(ArrayList<Processes> procs)
    {
        pd.fillDatabase(procs);
    }
}
