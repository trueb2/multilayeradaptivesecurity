package uiuc.research.appguardian.questions.callquestions;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.TreeMap;

import uiuc.research.appguardian.questions.Question;
import uiuc.research.appguardian.util.Util;

/**
 * CallHistoryAnalyze retrieves data from CallHistoryDataSet
 * and generate related call history questions
 */
public class CallHistoryAnalyze {

    private static CallHistoryDataSet callHistoryDataSet;
    private static ArrayList<String> contactList;


    /**
     *
     * @param daysBack up to how many days back
     * @return Question
     */
    public static Question whoIsMostOccurringContact(int daysBack, int qType) {
        Log.d(Util.TAG, "Attempting to make a most occuring contact question");
        if (callHistoryDataSet.getMostOccurringContactTreeMap(daysBack).lastEntry() != null) {
            TreeMap<Integer, String> mostOccurringContactTreeMap = callHistoryDataSet.getMostOccurringContactTreeMap(daysBack);

            String query = "Who did you contact most in the past " + Integer.toString(daysBack) +
                    " days?";
            String answer = mostOccurringContactTreeMap.lastEntry().getValue();
            ArrayList<String> choices = new ArrayList<>();

            choices.add(answer);
            int count = 0;
            while (choices.size() < 4 && count++ < 10000) {
                String randomContact = pickRandomContact(contactList);
                if (!choices.contains(randomContact)) {
                    choices.add(randomContact);
                }
            }

            if(count == 10000)
            {
                Question question = new Question(null, null, null, qType);
                Log.d(Util.TAG, "Failed in making a most occuring contact question");
                return question;
            }

            Collections.shuffle(choices);
            Question question = new Question(query, answer, choices, qType);
            Log.d(Util.TAG, "Finished making a most occuring contact question");
            return question;
        }
        else {
            Question question = new Question(null, null, null, qType);
            Log.d(Util.TAG, "Failed in making a most occuring contact question");
            return question;
        }
    }

    public static Question whoIsTheLastPersonYouContact(int qType) {
        Log.d(Util.TAG, "Trying to make a last person contact question");
        if (callHistoryDataSet.getLastContact() == null) {
            Log.d(Util.TAG, "Failed in making a last person contact question");
            return new Question(null, null, null, qType);
        }
        else {
            String query = "Who is the last person you contact 1 hour ago?";
            String answer = callHistoryDataSet.getLastContact();
            ArrayList<String> choices = new ArrayList<>();

            choices.add(answer);
            int count = 0;
            while (choices.size() < 4 && count++ < 10000 ) {
                String randomContact = pickRandomContact(contactList);
                if(randomContact == null)
                {
                    Log.d(Util.TAG, "Failed in making a last person contact question");
                    return new Question(query, null, null, qType);
                }
                if (!choices.contains(randomContact)) {
                    choices.add(randomContact);
                }
            }

            if(count == 10000)
            {
                Question question = new Question(null, null, null, qType);
                Log.d(Util.TAG, "Failed in making a last person contact question");
                return question;
            }

            Collections.shuffle(choices);

            Log.d(Util.TAG, "Made a last person contact question");
            return new Question(query, answer, choices, qType);
        }
    }


    /**
     * load data from DataSet
     * @param inputCallHistoryDataSet
     */
    public static void prepareDataSet(CallHistoryDataSet inputCallHistoryDataSet) {
        callHistoryDataSet = inputCallHistoryDataSet;
        contactList = callHistoryDataSet.getContactList();
    }

    /**
     *
     * @param contactList
     * @return
     */
    private static String pickRandomContact(ArrayList<String> contactList) {
        if(contactList == null || contactList.size() == 0)
            return null;
        Random random = new Random();
        return contactList.get(random.nextInt(contactList.size()));
    }
}
