package uiuc.research.appguardian.questions.locationquestions;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeMap;

import uiuc.research.appguardian.database.Location;
import uiuc.research.appguardian.util.Util;

/**
 * Created by jwtrueb on 10/22/14.
 * This will be the medium between the data stored in CVS file
 * on the phone and the algorithms that need access to the information
 * to develop useful questions
 * <p/>
 * Functionality:
 * 1. Read all Locations from file
 * 2. Has a string representation of the database
 */

public class LocationDatabase
{

    private static ArrayList<Location> places; //all logged locations
    private static ArrayList<Location> time; //sorted by visits
    private static TreeMap<Integer, Location> visits; //sorted by visits
    private static HashMap<Location, String> addresses;
    private static boolean ready = false;

    /**
     * sorts places by everything
     *
     * @throws java.io.FileNotFoundException
     */
    public void sortAll() throws FileNotFoundException
    {
        if (places == null)
        {
            throw new FileNotFoundException("places is null pull from SQL first");
        }
        sortByTime(places);
        sortByVisits(places);
    }

    /**
     * sorts all of the places visited by the number of times that
     * the location was visited
     * <p/>
     * returns a TreeMap ordered by the number of visits to each location
     *
     * @return
     */
    public TreeMap<Integer, Location> sortByVisits(ArrayList<Location> locs)
    {
        TreeMap<Integer, Location> sorted = new TreeMap<Integer, Location>();
        HashMap<Location, Integer> temp = new HashMap<Location, Integer>();

        //put all locations in HashMap to easily count the number of visits
        for (Location loc : locs)
        {
            Integer integer = temp.get(loc);

            if (integer == null)
            {
                integer = 1;
            }

            temp.put(loc, integer + 1);
        }

        //move all locations from HashMap into TreeMap where they will be ordered
        for (Location loc : temp.keySet())
        {
            sorted.put(temp.get(loc), loc);
        }

        visits = sorted;
        return sorted;
    }

    /**
     * sorts all of the places visited by the time that each location
     * was visited
     * <p/>
     * returns an ArrayList in the order that the locations were visited
     */
    public ArrayList<Location> sortByTime(ArrayList<Location> locs)
    {
        if(locs == null)
            return null;

        @SuppressWarnings("unchecked") //no reason to check this
                ArrayList<Location> sorted = (ArrayList<Location>) locs.clone();
        Sort.mergesort(sorted); //sorts sorted using mergesort
        time = sorted;
        return sorted;
    }

    /**
     * returns several different ways to look at the dataset in a string format
     */
    public String toString()
    {
        if (places == null)
        {
            try
            {
                throw new Exception("places is null where toString is called");
            }
            catch (Exception e)
            {
                e.printStackTrace();
            } //make sure that everything has been initialized properly
        }

        StringBuffer sb = new StringBuffer();

        //sorted by number of visits and time
        sb.append("\nSorted by timestamp\n");
        sb.append(toStringTime(places));
        sb.append("Sorted by number of visits\n");
        sb.append(toStringVisits(places));

        return sb.toString();
    }

    /**
     * returns serveral different ways to look at the dataset in a string format
     * without looking at the entire dataset
     */
    public String toString(ArrayList<Location> locs)
    {
        if (locs == null)
        {
            return null;
        }

        StringBuffer sb = new StringBuffer();

        //sorted by number of visits and time
        sb.append("Sorted by number of visits\n");
        sb.append(toStringVisits(locs));
        sb.append("\nSorted by timestamp\n");
        sb.append(toStringTime(locs));

        return sb.toString();
    }

    /**
     * Returns a string representation of the locations sorted by time
     * if the locations have not already been sorted then they will be
     * sorted
     */
    public String toStringTime(ArrayList<Location> locs)
    {
        StringBuffer sb = new StringBuffer();

        if (locs == null)
        {
            return "There were no places in the list";
        }

        //if not already sorted, sort; otherwise use what has already been sorted
        if (time == null || locs != places)
        {
            time = sortByTime(locs);
        }

        for (Location loc : time)
        {
            String stime = Util.convertStamp(loc.getTimestamp());
            sb.append("Location " + loc + " was visited at " + stime + "\n");
        }

        sb.append("Total Locations Visited : " + time.size());

        return sb.toString();
    }

    /**
     * Returns a string representation of the locations sorted by
     * the number of visits to each locations
     * if the locations have not already been sorted by such criteria
     * then they will be sorted
     */
    public String toStringVisits(ArrayList<Location> locs)
    {
        StringBuffer sb = new StringBuffer();

        if (locs == null)
        {
            return "There were no places in the list";
        }

        //if the locations have not been sorted then sort them
        if (visits == null || locs != places)
        {
            visits = sortByVisits(locs);
        }

        Iterator<Integer> itr = visits.keySet().iterator();
        int sum = 0;
        for (Integer i : visits.keySet())
        {
            Location loc = visits.get(i);
            sum += i;
            sb.append("Location " + loc + " was visited " + i + " time(s).   \t" + loc.getAddress() + "\n");
        }

        sb.append("That is " + sum + " visits.\n");

        return sb.toString();
    }

    /**
     * returns the day of the week given a timestamp
     *
     * @param timestamp
     * @return hour
     */
    protected static int dayOfWeekByStamp(long timestamp)
    {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date(timestamp));
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        return dayOfWeek;
    }

    /**
     * returns the hour of the day given a timestamp
     *
     * @param timestamp
     * @return hour
     */
    protected static int hourOfDayByStamp(long timestamp)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(timestamp));
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        return hour;
    }

    /**
     * returns places
     */
    public ArrayList<Location> getPlaces()
    {
        return places;
    }

    /**
     * replaces null addresses in csv file with an SQL safe version
     * of the actual address
     */
    protected boolean assignAddresses()
    {
        //TODO use REPLACE to chnage the SQL table and not have to do this often
        PrintWriter pw = null;
        boolean nonull = true;
        try
        {
            pw = new PrintWriter(new FileWriter("location.csv"));
        }
        catch (Exception e)
        {
            return false;
        }

        for (Location loc : places)
        {
            String address = loc.getAddress();
            if (address == null)
            {
                address = Location.getRoadName(loc.getLatitude() + "", loc.getLatitude() + "");
            }

            if (address == null)
            {
                nonull = false;
                continue;
            }

            address = address.replaceAll(",", " |");
            String s = loc.getLatitude() + "," + loc.getLongitude() +  "," + address + "," + loc.getTimestamp();
            pw.println(s);
        }

        pw.close();
        return nonull;
    }

    public static boolean isReady(){
        return ready;
    }

    public static HashMap<Location, String> getAddresses(){
        return addresses;
    }

    public void setLocations(ArrayList<Location> locs){
        places = locs;
        ready = (places == null) ? false : true ;
    }
}