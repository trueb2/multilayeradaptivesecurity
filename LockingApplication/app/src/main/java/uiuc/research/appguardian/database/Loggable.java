package uiuc.research.appguardian.database;

/**
 * Created by jwtrueb on 5/21/15.
 */
public interface Loggable {

    /*
     * Returns the string that would
     * represent the instance if the String
     * were represented in a textual log
     */
    public String logLine();
}
