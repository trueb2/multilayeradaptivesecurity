package uiuc.research.appguardian.questions.browserquestions;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

import uiuc.research.appguardian.database.DatabaseHandler;
import uiuc.research.appguardian.database.HistoryAndBookmarks;
import uiuc.research.appguardian.database.Location;
import uiuc.research.appguardian.services.QuestionService;
import uiuc.research.appguardian.util.Util;

/**
 * Created by qiuding on 4/29/2015.
 */
public class BrowserHistoryDataSet {

    private ArrayList<HistoryAndBookmarks> habs;
    private HashMap<String, Integer> visitsPerSite;
    private TreeMap<Integer, String> sitesByVisits;

    public BrowserHistoryDataSet()
    {
        habs = new ArrayList<>();
        visitsPerSite = new HashMap<>();
        sitesByVisits = new TreeMap<>();
    }

    public void setHaBs(ArrayList<HistoryAndBookmarks> habs)
    {
        this.habs = habs;

        //sort them now
        //Log.d(Util.TAG, "There are " + habs.size() + " history items ");
        if(habs == null || habs.size() == 0) {
            return;
        }

        if(habs == null)
        {
            //Log.d(Util.TAG, "habs is null");
        }

        for(HistoryAndBookmarks h : habs) {
            if(h == null)
            {
                //Log.d(Util.TAG, "h is null");
                continue;
            }
            //Log.d(Util.TAG, h.getTitle() + " " + h.getUrl());
            if(h.getTitle() == null || h.getTitle().length() < 3)
                continue;
            Integer i = visitsPerSite.get(h.getTitle());
            if(i == null)
                i = 0;
            visitsPerSite.put(h.getTitle(), i + 1);
        }

        for(String title : visitsPerSite.keySet()) {
            Integer newKey = visitsPerSite.get(title);
            String oldTitle = sitesByVisits.put(newKey, title);
            while(oldTitle != null)
                oldTitle = sitesByVisits.put(--newKey, oldTitle);
        }
    }

    public ArrayList<HistoryAndBookmarks> getHaBs()
    {
        return habs;
    }

    public TreeMap<Integer, String> getSitesByVisits()
    {
        return sitesByVisits;
    }
}
