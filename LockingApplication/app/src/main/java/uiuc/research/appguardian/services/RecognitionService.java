package uiuc.research.appguardian.services;

import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.ActivityRecognitionClient;

import uiuc.research.appguardian.R;
import uiuc.research.appguardian.ui.MainActivity;
import uiuc.research.appguardian.util.ActivityUtils;
import uiuc.research.appguardian.util.Util;

/**
 * Created by jwtrueb on 11/8/14.
 */
public class RecognitionService extends Service
{

    private class RecognitionConnectionHandler implements GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener
    {

        protected PendingIntent mPendingIntent;

        public RecognitionConnectionHandler()
        {

            Intent intent = new Intent(mContext, ActivityRecognitionIntentService.class);

            mPendingIntent = PendingIntent.getService(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        }

        /**
         * Verify that Google Play services is available before making a request.
         *
         * @return true if Google Play services is available, otherwise false
         */
        private boolean servicesConnected()
        {

            // Check that Google Play services is available
            int resultCode =
                    GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());

            // If Google Play services is available
            if (ConnectionResult.SUCCESS == resultCode)
            {

                // In debug mode, log the status
                //Log.d(Util.TAG, getString(R.string.play_services_available));

                // Continue
                return true;

                // Google Play services was not available for some reason
            } else
            {

                // Display an error dialog
                return false;
            }
        }

        @Override
        public void onDisconnected()
        {
            //Log.d(Util.TAG, "Disconnected");
        }

        @Override
        public void onConnected(Bundle b)
        {
            //Log.d(Util.TAG, "Connected Activity Recognition");

            //WTF this does not work
            if (servicesConnected())
            {
                //Log.d(Util.TAG, "Activity Recognition Updates Requested");
                mActivityRecognitionClient.requestActivityUpdates(10000, mPendingIntent); //10 seconds
            }
        }

        /*
          * Implementation of OnConnectionFailedListener.onConnectionFailed
          * If a connection or disconnection request fails, report the error
          * connectionResult is passed in from Location Services
          */
        @Override
        public void onConnectionFailed(ConnectionResult connectionResult)
        {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
            if (connectionResult.hasResolution())
            {

                try
                {
                    connectionResult.startResolutionForResult((Activity) mContext,
                            ActivityUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);

            /*
             * Thrown if Google Play services canceled the original
             * PendingIntent
             */
                }
                catch (IntentSender.SendIntentException e)
                {
                    // display an error or log it here.
                }

        /*
         * If no resolution is available, display Google
         * Play service error dialog. This may direct the
         * user to Google Play Store if Google Play services
         * is out of date.
         */
            } else
            {
                Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
                        connectionResult.getErrorCode(),
                        (Activity) mContext,
                        ActivityUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);
                if (dialog != null)
                {
                    dialog.show();
                }
            }
        }

    }

    private NotificationManager mNM;
    private RecognitionConnectionHandler mRecConHandler;
    private Context mContext;
    private Thread mThread;
    private ActivityRecognitionClient mActivityRecognitionClient;
    private static boolean running = false;
    private static boolean logActions = true; // user decides to log actions
    private static RecognitionService mInstance = null;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        /*
        if intent is null then the service crashed and the system tried to restart it
        because it is sticky
        this needs to be done right because aparently the service only works
        correctly right now whenever the service is started by the mainactivity

        I dont know why this way does not work
         */
        //Log.d(Util.TAG, "Received start id " + startId + ": " + intent);

        mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();

        if (intent != null)
        {
            showNotification(getText(R.string.activity_recognition_service_label).toString());
        } else
        {
            showNotification("Location Service Crashed and Restarted");
        }

        mContext = this;
        mRecConHandler = new RecognitionConnectionHandler();
        mActivityRecognitionClient = new ActivityRecognitionClient(this, mRecConHandler, mRecConHandler);
        mInstance = this;

        mThread = new Thread()
        {
            @Override
            public void run()
            {
                //Log.d(Util.TAG, "Running Recognition Thread");
                mActivityRecognitionClient.connect();
            }
        };


        mThread.start();

        running = true;
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }

    @Override
    public void onDestroy()
    {
        // Cancel the persistent notification.
        stopForeground(true);

        mActivityRecognitionClient.removeActivityUpdates(mRecConHandler.mPendingIntent);

        // Tell the user we stopped.
        Toast.makeText(this, R.string.activity_recognition_destroy, Toast.LENGTH_SHORT).show();
        mActivityRecognitionClient.disconnect();
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    /**
     * Start a Service in the foreground and remove the notification
     */
    private void showNotification(String s)
    {
        // In this sample, we'll use the same text for the ticker and the expanded notification
        CharSequence text = getText(R.string.activity_recognition_service_label);

        // Set the icon, scrolling text and timestamp
        Notification notification = new Notification(R.drawable.walking, text,
                System.currentTimeMillis());

        // The PendingIntent to launch our activity if the user selects this notification
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);

        // Set the info for the views that show in the notification panel.
        notification.setLatestEventInfo(this, s,
                text, contentIntent);

        //start foreground and remove notification
        startForeground(AppLockService.NOTIFICATION_ID, notification);
        HelperService.removeNotification(this);
    }

    /**
     * This provides a way of checking if the service is currently running
     * or not
     * @return
     */
    public static boolean isRunning()
    {
        return running;
    }

    /**
     * This provides a way of finding out if the user has set the
     * application to log actions or not
     * @return
     */
    public static boolean getLogActions(){
        return logActions;
    }

    /**
     * This is the method that will be used when the user sets
     * the application to log/not log something
     * @param log
     */
    public static void setLogActions(boolean log){
        logActions = log;
    }

    public static void stopLogging()
    {
        if(!running)
            return;
        mInstance.stopService(new Intent(mInstance, RecognitionService.class));
        running = false;
    }
}
