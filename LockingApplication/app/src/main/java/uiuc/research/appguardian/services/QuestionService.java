package uiuc.research.appguardian.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

import uiuc.research.appguardian.R;
import uiuc.research.appguardian.questions.Question;
import uiuc.research.appguardian.questions.QuestionHandler;
import uiuc.research.appguardian.questions.activityquestions.ActivityAnalyze;
import uiuc.research.appguardian.questions.activityquestions.ActivityDatabase;
import uiuc.research.appguardian.questions.browserquestions.BrowserHistoryAnalyze;
import uiuc.research.appguardian.questions.callquestions.CallHistoryAnalyze;
import uiuc.research.appguardian.questions.locationquestions.LocationAnalyze;
import uiuc.research.appguardian.questions.locationquestions.LocationDatabase;
import uiuc.research.appguardian.questions.processesquestions.ProcAnalyze;
import uiuc.research.appguardian.questions.screenquestions.ScreenAnalyze;
import uiuc.research.appguardian.questions.smsquestions.SMSAnalyze;
import uiuc.research.appguardian.ui.MainActivity;
import uiuc.research.appguardian.ui.views.QuestionView;
import uiuc.research.appguardian.servercommunications.ServerPostRequest;
import uiuc.research.appguardian.util.Util;

/**
 * Created by Jacob on 1/14/2015.
 */
public class QuestionService extends Service {
    public static final int MAX_NUM_QS = 5;
    public static final int TOTAL_QUESTION_TYPES = 24;
    public static final int QT_WALK_MOST_5_DAYS = 0;
    public static final int QT_WALK_MOST_YESTERDAY = 1;
    public static final int QT_DRIVE_MOST_5_DAYS = 2;
    public static final int QT_DRIVE_MOST_YESTERDAY = 3;
    public static final int QT_TIME_AT_YESTERDAY = 4;
    public static final int QT_TIME_AT_2_DAYS = 5;
    public static final int QT_WHERE_WERE_YOU_5_DAYS = 6;
    public static final int QT_MOST_COMMON_DEST_5_DAYS = 7;
    public static final int QT_MOST_COMMON_SOURCE_5_DAYS = 8;
    public static final int QT_SCREEN_ON = 9;
    public static final int QT_WHERE_WERE_YOU_2_DAYS = 10;
    public static final int QT_WHERE_WERE_YOU_YESTERDAY = 11;
    public static final int QT_MOST_COMMON_DEST_2_DAYS = 12;
    public static final int QT_MOST_COMMON_DEST_YESTERDAY = 13;
    public static final int QT_MOST_COMMON_SOURCE_2_DAYS = 14;
    public static final int QT_MOST_COMMON_SOURCE_YESTERDAY = 15;
    public static final int QT_MOST_CALL_CONTACT_5_DAYS = 16;
    public static final int QT_MOST_CALL_CONTACT_YESTERDAY = 17;
    public static final int QT_LAST_CALL_CONTACT = 18;
    public static final int QT_LONGEST_USED_APP = 19;
    public static final int QT_MOST_OFTEN_USED_APP = 20;
    public static final int QT_MOST_SMS_CONTACT_5_DAYS = 21;
    public static final int QT_MOST_SMS_CONTACT_YESTERDAY = 22;
    public static final int QT_MOST_VISITED_WEBSITE = 23;
    public static final String FIRST_QUESTION_MADE = "first_question_made";
    public static boolean ready;
    private static QuestionService instance = null;
    private static boolean running = false;
    private static ServerPostRequest mServerPostRequest = null;
    private final IBinder mBinder = new LocalBinder();
    private QuestionHandler mQuestionHandler;
    private NotificationManager mNM;
    private QuestionView qView;
    private Timer mTimer;
    private Thread mThread;
    private int questionsToGenerate;
    private int nextType;
    private LinkedList<Question> mQuestions;
    private long donttryagain = 0;
    private int incompleteCount = 0;


    //ETHAN add your Question Constants Here then go to
    //line ~130 and add your case to the switch

    public static boolean isRunning() {
        return running;
    }

//    public static void generateLocationQuestions()
//    {
//        if (!ready)
//        {
//            return;
//        }
//
//        //Log.d(Util.TAG, "Generating Location Questions");
//        LocationAnalyze.generateAllQuestions(10, 0, Calendar.WEDNESDAY, Calendar.TUESDAY);
//
//    }
//
//    public static void generateActivityQuestions()
//    {
//        if (ready)
//        {
//            //Log.d(Util.TAG, "Generating Location Questions");
//
//            ActivityAnalyze.generateAllQuestions(10, 0);
//        }
//    }

    public static QuestionService getInstance() {
        return instance;
    }

    /**
     * Returns the ServerPostRequest instance if the QuestionService is running and
     * there is an instance, otherwise it will create a new instance
     * @return
     */
    public static ServerPostRequest getServerPostRequest()
    {
        if(!running)
            return null;

        if(mServerPostRequest == null || mServerPostRequest.isSending()) {
            mServerPostRequest = new ServerPostRequest(instance.getApplicationContext());
            return mServerPostRequest;
        }
        else
            return mServerPostRequest;
    }

    /**
     * Resets the ServerPostRequest instace to null so that questions' data is
     * not blended together
     * Should be called upon submitIfReady actullay submitting to the server
     * which is after the final element of ServerPostRequest is set
     */
    public static void resetServerPostRequest()
    {
        if(!running)
            return;
        else
            mServerPostRequest = null;
    }

    public static void stop()
    {
        if(!running)
            return;

        instance.stopService(new Intent(instance, QuestionService.class));
        running = false;
    }

    @Override
    public void onCreate() {
        mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // Display a notification then remove it
        showNotification();

        instance = this;
        mQuestionHandler = new QuestionHandler(this);
        mQuestionHandler.loadDatabase(this);
        final Context CONTEXT = instance;
        final QuestionHandler mQH = mQuestionHandler;
        mTimer = new Timer();
        TimerTask timerTask = new TimerTask() {
            public void run() {
                mQH.loadDatabase(CONTEXT);
            }
        };
        mTimer.schedule(timerTask, 0, 6 * 60 * 60 * 1000); // six hours
        mQuestions = new LinkedList<Question>();
        boolean startThread = defineThread();
        questionsToGenerate = 8;
        if(startThread)
            mThread.start();

        running = true;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Log.d(Util.TAG, "Received start id " + startId + ":" + intent);
        ready = false;
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        // Cancel the persistent notification.
        mNM.cancel(AppLockService.NOTIFICATION_ID);

        // Tell the user we stopped.
        Toast.makeText(this, R.string.question_service_destroy, Toast.LENGTH_SHORT).show();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /**
     * Start the Service in the foreground and remove the notification
     */
    private void showNotification() {
        CharSequence text = getText(R.string.question_service_label);
        // Set the icon, scrolling text and timestamp
        Notification notification = new Notification(R.drawable.pattern_circle_green, text,
                System.currentTimeMillis());

        // The PendingIntent to launch our activity if the user selects this notification
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);

        // Set the info for the views that show in the notification panel.
        notification.setLatestEventInfo(this, getText(R.string.question_service_content).toString(),
                text, contentIntent);

        //start foreground and remove notification
        startForeground(AppLockService.NOTIFICATION_ID, notification);
        HelperService.removeNotification(this);
    }

    public void postQuestion(int numQs, LockService ls) {
        if (!LocationDatabase.isReady() || !ActivityDatabase.isReady()) {
            return;
        }

        if (qView != null && (qView.isShown() || qView.isViewShown())) {
            hideView();
        }

        //Log.d(Util.TAG, "Trying to inflate questions");
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        LayoutInflater li = (LayoutInflater) this.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        WindowManager.LayoutParams mLayoutParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM
                        | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                        | WindowManager.LayoutParams.FLAG_FULLSCREEN
                        | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                PixelFormat.TRANSLUCENT);
        qView = (QuestionView) li.inflate(R.layout.view_lock_question, null);
        qView.setVisibility(View.VISIBLE);
        qView.setNumQs(numQs, ls);
        wm.addView(qView, mLayoutParams);
        qView.showQuestions();
    }

    public void hideView() {
        if (qView == null || qView.getVisibility() == View.INVISIBLE || !qView.isShown()) {
            return;
        }

        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        wm.removeView(qView);
        qView = null;
    }

    /**
     * Returns the number of questions specified
     * Creates more questions on a seperate thread
     */
    public LinkedList<Question> getQuestions(int numQs) {
        questionsToGenerate += numQs;
        mThread.run();

        //Log.d(Util.TAG, numQs + " !!!!!!!!!!!!!!!!!!!!!!!!");
        LinkedList<Question> list = new LinkedList<Question>();
        while(list.size() < numQs && mQuestions.size() > 0)
        {
            Question q = mQuestions.remove();
            if(q.COMPLETE) {
                list.add(q);
                //Log.d(Util.TAG, "||| " + q.getQuestionType() + " |||");
            }
        }

        return list;
    }

    private boolean defineThread() {
        nextType = QT_LONGEST_USED_APP;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final boolean action = prefs.getBoolean(getString(R.string.nav_action), true);
        final boolean loc = prefs.getBoolean(getString(R.string.nav_location), true);
        final boolean browse = prefs.getBoolean(getString(R.string.nav_browser_hist), true);
        final boolean call = prefs.getBoolean(getString(R.string.nav_callhistory), true);
        final boolean proc = prefs.getBoolean(getString(R.string.nav_processes), true);
        final boolean screen = prefs.getBoolean(getString(R.string.nav_screen), true);
        final boolean sms = Build.VERSION.SDK_INT < 19 ? false : prefs.getBoolean(getString(R.string.nav_smshistory), true);


        mThread = new Thread(new Runnable() {
            public void run() {
//                while (questionsToGenerate > 0 && donttryagain + 1000 * 60 * 60 < System.currentTimeMillis()) {
                int count = 0;
                while(0 < questionsToGenerate && donttryagain < System.currentTimeMillis() - 1000*120){
                    switch (nextType) {
                        case QT_WALK_MOST_5_DAYS:
                            if(action)
                                mQuestions.add(ActivityAnalyze.whendYouWalkMost(5, 0, QT_WALK_MOST_5_DAYS));
                            else
                                mQuestions.add(new Question(null, null, null,-1));
                            break;
                        case QT_WALK_MOST_YESTERDAY:
                            if(action)
                                mQuestions.add(ActivityAnalyze.whendYouWalkMost(1, 0, QT_WALK_MOST_YESTERDAY));
                            else
                                mQuestions.add(new Question(null, null, null,-1));
                            break;
                        case QT_DRIVE_MOST_5_DAYS:
                            if(action)
                                mQuestions.add(ActivityAnalyze.whendYouDriveMost(5, 0, QT_DRIVE_MOST_5_DAYS));
                            else
                                mQuestions.add(new Question(null, null, null,-1));
                            break;
                        case QT_DRIVE_MOST_YESTERDAY:
                            if(action)
                                mQuestions.add(ActivityAnalyze.whendYouDriveMost(1, 0, QT_DRIVE_MOST_YESTERDAY));
                            else
                                mQuestions.add(new Question(null, null, null,-1));
                            break;
                        case QT_TIME_AT_YESTERDAY:
                            if(loc)
                                mQuestions.add(LocationAnalyze.timeAt(Util.yesterday(), QT_TIME_AT_YESTERDAY));
                            else
                                mQuestions.add(new Question(null, null, null,-1));
                            break;
                        case QT_TIME_AT_2_DAYS:
                            if(loc)
                                mQuestions.add(LocationAnalyze.timeAt(Util.dayBeforeYesterday(), QT_TIME_AT_2_DAYS));
                            else
                                mQuestions.add(new Question(null, null, null,-1));
                            break;
                        case QT_WHERE_WERE_YOU_5_DAYS:
                            if(loc)
                                mQuestions.add(LocationAnalyze.whereWereYou(5, QT_WHERE_WERE_YOU_5_DAYS));
                            else
                                mQuestions.add(new Question(null, null, null,-1));
                            break;
                        case QT_WHERE_WERE_YOU_2_DAYS:
                            if(loc)
                                mQuestions.add(LocationAnalyze.whereWereYou(2, QT_WHERE_WERE_YOU_2_DAYS));
                            else
                                mQuestions.add(new Question(null, null, null,-1));
                            break;
                        case QT_WHERE_WERE_YOU_YESTERDAY:
                            if(loc)
                                mQuestions.add(LocationAnalyze.whereWereYou(1, QT_WHERE_WERE_YOU_YESTERDAY));
                            else
                                mQuestions.add(new Question(null, null, null,-1));
                            break;
                        case QT_MOST_COMMON_DEST_5_DAYS:
                            if(loc)
                                mQuestions.add(LocationAnalyze.mostCommon(false, 5, 0, QT_MOST_COMMON_DEST_5_DAYS));
                            else
                                mQuestions.add(new Question(null, null, null,-1));
                            break;
                        case QT_MOST_COMMON_DEST_2_DAYS:
                            if(loc)
                                mQuestions.add(LocationAnalyze.mostCommon(false, 2, 0, QT_MOST_COMMON_DEST_2_DAYS));
                            else
                                mQuestions.add(new Question(null, null, null,-1));
                            break;
                        case QT_MOST_COMMON_DEST_YESTERDAY:
                            if(loc)
                                mQuestions.add(LocationAnalyze.mostCommon(false, 1, 0, QT_MOST_COMMON_DEST_YESTERDAY));
                            else
                                mQuestions.add(new Question(null, null, null,-1));
                            break;
                        case QT_MOST_COMMON_SOURCE_5_DAYS:
                            if(loc)
                                mQuestions.add(LocationAnalyze.mostCommon(true, 5, 0, QT_MOST_COMMON_SOURCE_5_DAYS));
                            else
                                mQuestions.add(new Question(null, null, null,-1));
                            break;
                        case QT_MOST_COMMON_SOURCE_2_DAYS:
                            if(loc)
                                mQuestions.add(LocationAnalyze.mostCommon(true, 2, 0, QT_MOST_COMMON_SOURCE_2_DAYS));
                            else
                                mQuestions.add(new Question(null, null, null,-1));
                            break;
                        case QT_MOST_COMMON_SOURCE_YESTERDAY:
                            if(loc)
                                mQuestions.add(LocationAnalyze.mostCommon(true, 1, 0, QT_MOST_COMMON_SOURCE_YESTERDAY));
                            else
                                mQuestions.add(new Question(null, null, null,-1));
                            break;
                        case QT_SCREEN_ON:
                            if(screen)
                                mQuestions.add(ScreenAnalyze.longOn(1, QT_SCREEN_ON));
                            else
                                mQuestions.add(new Question(null, null, null,-1));
                            break;
                        case QT_MOST_CALL_CONTACT_5_DAYS:
                            if(call)
                                mQuestions.add(CallHistoryAnalyze.whoIsMostOccurringContact(5, QT_MOST_CALL_CONTACT_5_DAYS));
                            else
                                mQuestions.add(new Question(null, null, null,-1));
                            break;
                        case QT_MOST_CALL_CONTACT_YESTERDAY:
                            if(call)
                                mQuestions.add(CallHistoryAnalyze.whoIsMostOccurringContact(1, QT_MOST_CALL_CONTACT_YESTERDAY));
                            else
                                mQuestions.add(new Question(null, null, null,-1));
                            break;
                        case QT_LAST_CALL_CONTACT:
                            if(call)
                                mQuestions.add(CallHistoryAnalyze.whoIsTheLastPersonYouContact(QT_LAST_CALL_CONTACT));
                            else
                                mQuestions.add(new Question(null, null, null,-1));
                            break;
                        case QT_LONGEST_USED_APP:
                            if(proc)
                                mQuestions.add(ProcAnalyze.longestUsed());
                            else
                                mQuestions.add(new Question(null, null, null,-1));
                            break;
                        case QT_MOST_OFTEN_USED_APP:
                            if(proc)
                                mQuestions.add(ProcAnalyze.mostOftenUsed());
                            else
                                mQuestions.add(new Question(null, null, null,-1));
                            break;
                        case QT_MOST_SMS_CONTACT_5_DAYS:
                            if(sms)
                                mQuestions.add(SMSAnalyze.whoIsMostOccurringSMSContact(5, QT_MOST_SMS_CONTACT_5_DAYS));
                            else
                                mQuestions.add(new Question(null, null, null,-1));
                            break;
                        case QT_MOST_SMS_CONTACT_YESTERDAY:
                            if(sms)
                                mQuestions.add(SMSAnalyze.whoIsMostOccurringSMSContact(1, QT_MOST_SMS_CONTACT_YESTERDAY));
                            else
                                mQuestions.add(new Question(null, null, null,-1));
                            break;
                        case QT_MOST_VISITED_WEBSITE:
                            if(browse)
                                mQuestions.add(BrowserHistoryAnalyze.mostVisitedWebsite(1));
                            else
                                mQuestions.add(new Question(null, null, null,-1));
                            break;
                        default:
                            if(screen)
                                mQuestions.add(ScreenAnalyze.longOn(1, QT_SCREEN_ON));
                            else
                                mQuestions.add(new Question(null, null, null,-1));
                    }

                    if (mQuestions.getLast().COMPLETE) {
                        incompleteCount = 0;
                        questionsToGenerate--;
                        count++;
                        if(count % 100 == 0)
                            mQuestionHandler.loadDatabase(instance);

                        Log.d(Util.TAG, "Created Question of type : " + nextType + " : Still to generate " + questionsToGenerate + " : of Size " + mQuestions.size());
                    } else {
                        mQuestions.removeLast();
                        if (incompleteCount++ > TOTAL_QUESTION_TYPES*4) {
                            donttryagain = System.currentTimeMillis();
                            incompleteCount = 0;
                            count = 0;
                            mQuestionHandler.loadDatabase(instance);
                            Log.d(Util.TAG, "Not going to attempt to create questions for 2 minutes");
                        }
                        else
                        {
                            count++;
                            if(count % 100 == 0)
                                mQuestionHandler.loadDatabase(instance);
                        }
                        Log.d(Util.TAG, "Incomplete question of type : " + nextType);
                    }
                    nextType = ++nextType % TOTAL_QUESTION_TYPES;
                }
            }
        }

        );
        return action || loc || browse || call || sms || proc || screen;

    }

    /**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC.
     */
    public class LocalBinder extends Binder {
        QuestionService getService() {
            return QuestionService.this;
        }
    }
}
