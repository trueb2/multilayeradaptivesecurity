package uiuc.research.appguardian.services;

import android.app.ActivityManager;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.Browser;
import android.provider.Telephony;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.ActivityRecognitionClient;
import com.google.android.gms.location.LocationClient;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import uiuc.research.appguardian.R;
import uiuc.research.appguardian.database.CallHistory;
import uiuc.research.appguardian.database.DatabaseHandler;
import uiuc.research.appguardian.database.HistoryAndBookmarks;
import uiuc.research.appguardian.database.ListApps;
import uiuc.research.appguardian.database.Location;
import uiuc.research.appguardian.database.Processes;
import uiuc.research.appguardian.database.SMS;
import uiuc.research.appguardian.questions.screenquestions.Off;
import uiuc.research.appguardian.questions.screenquestions.On;
import uiuc.research.appguardian.ui.MainActivity;
import uiuc.research.appguardian.util.ActivityUtils;
import uiuc.research.appguardian.util.Util;

/**
 * Created by jwtrueb on 10/12/14.
 */
public class LoggingService extends Service
{

    private class LocationConnectionHandler implements GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener
    {

        private int type;
        private static final int TYPE_ACTIVITY_RECOGNITION = 1;
        private static final int TYPE_LOCATION = 2;

        public LocationConnectionHandler()
        {
            type = 0;
        }

        public LocationConnectionHandler(int type)
        {
            this.type = type;
        }

        @Override
        public void onConnected(Bundle b)
        {
            //Log.d(Util.TAG, "Connected Location");
            locationConnectedCallback();
            //Log.d(Util.TAG, "Location updates should start popping up shortly");
        }

        @Override
        public void onDisconnected()
        {
            //Log.d(Util.TAG, "Disconnected");
        }

        /*
          * Implementation of OnConnectionFailedListener.onConnectionFailed
          * If a connection or disconnection request fails, report the error
          * connectionResult is passed in from Location Services
          */
        @Override
        public void onConnectionFailed(ConnectionResult connectionResult)
        {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
            if (connectionResult.hasResolution())
            {

                try
                {
                    connectionResult.startResolutionForResult((android.app.Activity) mContext,
                            ActivityUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);

            /*
             * Thrown if Google Play services canceled the original
             * PendingIntent
             */
                }
                catch (IntentSender.SendIntentException e)
                {
                    // display an error or log it here.
                }

        /*
         * If no resolution is available, display Google
         * Play service error dialog. This may direct the
         * user to Google Play Store if Google Play services
         * is out of date.
         */
            } else
            {
                Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
                        connectionResult.getErrorCode(),
                        (android.app.Activity) mContext,
                        ActivityUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);
                if (dialog != null)
                {
                    dialog.show();
                }
            }
        }
    }

    private class RecognitionConnectionHandler implements GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener
    {

        private PendingIntent mPendingIntent;

        public RecognitionConnectionHandler()
        {
                    /*
         * Create the PendingIntent that Location Services uses
         * to send activity recognition updates back to this app.
         */
            Intent intent = new Intent(
                    mContext, ActivityRecognitionIntentService.class);
        /*
         * Return a PendingIntent that starts the IntentService.
         */
            mPendingIntent = PendingIntent.getService(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        }

        /*
         * Handle results returned to this Activity by other Activities started with
         * startActivityForResult(). In particular, the method onConnectionFailed() in
         * DetectionRemover and DetectionRequester may call startResolutionForResult() to
         * start an Activity that handles Google Play services problems. The result of this
         * call returns here, to onActivityResult.
         */
        protected void onActivityResult(int requestCode, int resultCode, Intent intent)
        {

            // Choose what to do based on the request code
            switch (requestCode)
            {

                // If the request code matches the code sent in onConnectionFailed
                case ActivityUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST:

                    switch (resultCode)
                    {
                        // If Google Play services resolved the problem
                        case android.app.Activity.RESULT_OK:

                            // If the request was to start activity recognition updates
                            if (ActivityUtils.REQUEST_TYPE.ADD == mRequestType)
                            {

                                // Restart the process of requesting activity recognition updates
                                mActivityRecognitionClient.connect();
                                // If the request was to remove activity recognition updates
                            } else if (ActivityUtils.REQUEST_TYPE.REMOVE == mRequestType)
                            {

                                /*
                                 * Restart the removal of all activity recognition updates for the
                                 * PendingIntent.
                                 */
                                mActivityRecognitionClient.connect();

                            }
                            break;

                        // If any other result was returned by Google Play services
                        default:

                            // Report that Google Play services was unable to resolve the problem.
                            //Log.d(Util.TAG, getString(R.string.no_resolution));
                    }

                    // If any other request code was received
                default:
                    // Report that this Activity received an unknown requestCode
                    //Log.d(Util.TAG,
//                            getString(R.string.unknown_activity_request_code, requestCode));

                    break;
            }
        }

        /**
         * Verify that Google Play services is available before making a request.
         *
         * @return true if Google Play services is available, otherwise false
         */
        private boolean servicesConnected()
        {

            // Check that Google Play services is available
            int resultCode =
                    GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());

            // If Google Play services is available
            if (ConnectionResult.SUCCESS == resultCode)
            {

                // In debug mode, log the status
                //Log.d(Util.TAG, getString(R.string.play_services_available));

                // Continue
                return true;

                // Google Play services was not available for some reason
            } else
            {

                // Display an error dialog
//                GooglePlayServicesUtil.getErrorDialog(resultCode, this, 0).show();
                return false;
            }
        }

        @Override
        public void onDisconnected()
        {
            //Log.d(Util.TAG, "Disconnected");
        }

        @Override
        public void onConnected(Bundle b)
        {
            //Log.d(Util.TAG, "Connected Activity Recognition");

            if (servicesConnected())
            {
                //Log.d(Util.TAG, "Activity Recognition Updates Requested");
                mActivityRecognitionClient.requestActivityUpdates(3000, mPendingIntent);
            }
        }

        /*
          * Implementation of OnConnectionFailedListener.onConnectionFailed
          * If a connection or disconnection request fails, report the error
          * connectionResult is passed in from Location Services
          */
        @Override
        public void onConnectionFailed(ConnectionResult connectionResult)
        {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
            if (connectionResult.hasResolution())
            {

                try
                {
                    connectionResult.startResolutionForResult((android.app.Activity) mContext,
                            ActivityUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);

            /*
             * Thrown if Google Play services canceled the original
             * PendingIntent
             */
                }
                catch (IntentSender.SendIntentException e)
                {
                    // display an error or log it here.
                }

        /*
         * If no resolution is available, display Google
         * Play service error dialog. This may direct the
         * user to Google Play Store if Google Play services
         * is out of date.
         */
            } else
            {
                Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
                        connectionResult.getErrorCode(),
                        (android.app.Activity) mContext,
                        ActivityUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);
                if (dialog != null)
                {
                    dialog.show();
                }
            }
        }
    }

    private NotificationManager mNM;
    private int NOTIFICATION = R.string.location_service_label;
    private ActivityUtils.REQUEST_TYPE mRequestType;
    private IntentFilter mBroadcastFilter;
    private LocalBroadcastManager mBroadcastManager;
    private PendingIntent mActivityRecognitionPendingIntent;
    private ActivityRecognitionClient mActivityRecognitionClient;
    private LocationClient mLocationClient;
    private Context mContext;
    private Timer timer;
    private Thread mThread;
    private static DatabaseHandler mDatabaseHandler;
    private LocationConnectionHandler mLocConHandler;
    private RecognitionConnectionHandler mRecConHandler;
    private final int ACTIVITY_RECOGNITION_UPDATE_INTERVAL = 1000; //in ms
    private static boolean running = false;
    private long lastLoggedLocation = System.currentTimeMillis();
    private static boolean logLocation = true; //user decides to log location
    private static LoggingService mInstance;

    /**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC.
     */
    public class LocalBinder extends Binder
    {
        LoggingService getService()
        {
            return LoggingService.this;
        }
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        Toast.makeText(this, "Service Created", Toast.LENGTH_LONG).show();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        super.onStartCommand(intent, flags, startId);
        //Log.d(Util.TAG, "Received start id " + startId + ": " + intent);
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.

        mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();

        if (intent != null)
        {
            showNotification(getText(R.string.location_service_label).toString());
        } else
        {
            showNotificationCrash(getText(R.string.location_service_restart).toString());
        }

        mContext = this;
        mDatabaseHandler = DatabaseHandler.getInstance(this);
        mLocConHandler = new LocationConnectionHandler();
        mInstance = this;

        mThread = new Thread()
        {
            @Override
            public void run()
            {
                //Log.d(Util.TAG, "Running Thread");
                automateLogs();
            }
        };

        mThread.start();
        running = true;
        return START_STICKY;
    }

    @Override
    public void onDestroy()
    {
        // Cancel the persistent notification.
        stopForeground(true);

        //stop logging
        timer.cancel();

        // Tell the user we stopped.
        Toast.makeText(this, R.string.location_service_destroy, Toast.LENGTH_SHORT).show();
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    /**
     * Start the Service in the foreground and remove the
     * notification
     */
    private void showNotification(String s)
    {
        CharSequence text = getText(R.string.location_service_label);

        // Set the icon, scrolling text and timestamp
        Notification notification = new Notification(R.drawable.graph, text,
                System.currentTimeMillis());

        // The PendingIntent to launch our activity if the user selects this notification
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);

        // Set the info for the views that show in the notification panel.
        notification.setLatestEventInfo(this, s,
                text, contentIntent);

        //start foreground and remove notification
        startForeground(AppLockService.NOTIFICATION_ID, notification);
        HelperService.removeNotification(this);
    }

    /**
     * Show a notification while the service is running
     * that shows that the service has crashed
     */
    private void showNotificationCrash(String s)
    {
        CharSequence text = "Service Restarted";
        Notification notification = new Notification(R.drawable.graph, text, System.currentTimeMillis());
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);
        notification.setLatestEventInfo(this, text, s, contentIntent);
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(37331, notification);
    }


    /**
     * Verify that Google Play services is available before making a request.
     *
     * @return true if Google Play services is available, otherwise false
     */
    private boolean servicesConnected()
    {

        // Check that Google Play services is available
        int resultCode =
                GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode)
        {

            // In debug mode, log the status
            //Log.d(Util.TAG, getString(R.string.play_services_available));

            // Continue
            return true;

            // Google Play services was not available for some reason
        } else
        {

            // Display an error dialog
            //Log.d(Util.TAG, "Usually would fire off an error dialog here");
            return false;
        }
    }

    /**
     * Starts log updates on the following
     * Activity Recognition
     * Location
     * Call History
     * App History
     */
    public void automateLogs()
    {
        timer = new Timer();
//        monitorCallHistory(); //there is a broadcast receiver for all phone calls and sms
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        try {
            if (prefs.getBoolean(getString(R.string.nav_location), true))
                logLocation(); //starts a TimerTask and updates location every now and then
        }catch(ClassCastException e)
        {
            SharedPreferences.Editor ed = prefs.edit();
            ed.putBoolean(getString(R.string.nav_location), true);
            ed.commit();
        }
//        monitorAppInstallations(); //starts a TimerTask and checks for changes every 6 hours
        try {
            if (prefs.getBoolean(getString(R.string.nav_processes), true))
                monitorActiveProcesses();
        }catch(ClassCastException e)
        {
            SharedPreferences.Editor ed = prefs.edit();
            ed.putBoolean(getString(R.string.nav_processes), true);
            ed.commit();
        }

        // TODO: should not run every time service restart, and we're getting way too many data
        try {
            if (prefs.getBoolean(getString(R.string.nav_browser_hist), true))
                monitorBrowser();
        }catch(ClassCastException e)
        {
            SharedPreferences.Editor ed = prefs.edit();
            ed.putBoolean(getString(R.string.nav_browser_hist), true);
            ed.commit();
        }
        try {
            if (Build.VERSION.SDK_INT >= 19 && prefs.getBoolean(getString(R.string.nav_smshistory), true))
                monitorSms();
        } catch (ClassCastException e) {
            SharedPreferences.Editor ed = prefs.edit();
            ed.putBoolean(getString(R.string.nav_smshistory), true);
            ed.commit();
        }
    }

    /**
     * connects to Google Play Services and begins Activity Recognition
     * requests and sets up intent service and pending intent to receive callbacks
     * <p/>
     * the logging itself is handled in the onHandleIntent method of the
     * ActivityRecognitionIntentService class
     */
    public void startRecognitionUpdates()
    {
//        mRecognitionConnectionsHandler = new ConnectionsHandler(ConnectionsHandler.TYPE_ACTIVITY_RECOGNITION);
        mActivityRecognitionClient =
                new ActivityRecognitionClient(this, mRecConHandler, mRecConHandler);
        /*
         * Create the PendingIntent that Location Services uses
         * to send activity recognition updates back to this app.
         */
        Intent intent = new Intent(
                mContext, ActivityRecognitionIntentService.class);
        /*
         * Return a PendingIntent that starts the IntentService.
         */
        mActivityRecognitionPendingIntent = PendingIntent.getService(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (!servicesConnected())
        {
            return; //not connected to Google Play Services
        }

        mRequestType = ActivityUtils.REQUEST_TYPE.ADD;
        mActivityRecognitionClient.connect();
    }

    /**
     * Starts a service to monitor changes in phone history
     */
    public void monitorCallHistory()
    {
        //permissions
        //RECEIVE_SMS
        //RECEIVE_MMS
//        //Log.d(Util.TAG, "Call History Checking does not do anything right now on this thread");

    }

    /**
     * Starts monitoring and logging Locations as provided by Google Play Services
     */
    public void logLocation()
    {
//        mLocationConnectionsHandler = new ConnectionsHandler(ConnectionsHandler.TYPE_LOCATION);
        mLocationClient = new LocationClient(this, mLocConHandler, mLocConHandler);
        mLocationClient.connect();
        //need to wait for the onConnected() to be called
    }

    /**
     * Starts to monitor app history ie
     * Version Code and Number and Newest Application
     * This uses a Timer Task that checks up on the app history every
     * 6 hours
     */
    public void monitorAppInstallations()
    {
        //timer is already instantiated in logLocation()
        TimerTask monitorInstallations = new TimerTask()
        {
            @Override
            public void run()
            {
                //TODO confer with database and identify changes
                ListApps la = new ListApps();
                //Log.d(Util.TAG, "Checking Application Installations does nothing right now");
            }
        };

        timer.schedule(monitorInstallations, 0l, 1000 * 60 * 60 * 6); //6 hour intervals
    }

    /**
     * Starts to monitor active processes
     * Uses a TimerTask that runs every 5 minutes
     */
    public void monitorActiveProcesses()
    {
        final Context mContext = this;
        //mDatabaseHandler.overwriteTable(mDatabaseHandler.TABLE_PROCESSES);
        TimerTask checkProcesses = new TimerTask()
        {
            @Override
            public void run()
            {
                //Log.d(Util.TAG, "Trying to check processes");
                ActivityManager mActivityManager = (ActivityManager) mContext.getSystemService(ACTIVITY_SERVICE);
                List<ActivityManager.RunningAppProcessInfo> processInfos = mActivityManager.getRunningAppProcesses();
                //could get list of services and tasks here too but I dont think that will be helpful

//                //append to log
//                File exportDir = Environment.getExternalStoragePublicDirectory("MyCVS");
//                if (!exportDir.exists())
//                {
//                    exportDir.mkdirs();
//                }
//
//                File locationFile = new File(exportDir, "processes.csv");
//                try
//                {
//                    PrintWriter pw = new PrintWriter(new FileWriter(locationFile, true));
//
//                    pw.println("ID,PROCNAME,IMPORTANCE,TIMESTAMP");

                    for (ActivityManager.RunningAppProcessInfo p : processInfos)
                    {
                        int importance = p.importance;

                        Processes processes = new Processes(p.processName, importance);
                        mDatabaseHandler.addProcesses(processes);
                        String record = processes.getProcessName() + "," + processes.getImportance() + "," + processes.getCreatedAt();


//                        pw.println(record);
                        //Log.d(Util.TAG, "Process: " + p.processName + " " + importance);
                    }

//                    pw.close();
//                }
//                catch (Exception e)
//                {
//                    e.printStackTrace();
//                }
            }
        };

        timer.schedule(checkProcesses, 0l, 1000 * 60 * 15); //15 minute intervals
    }

    /**
     * Starts to monitor the browser history
     * Uses a TimerTask that runs every 24 hours
     */
    public void monitorBrowser() {
        final long back = System.currentTimeMillis() - Util.DAY_IN_MILLIS * 5;
        //Log.d(Util.TAG, "logging browser history back to timestamp: " + back);

        // TODO: checkHistory is not getting correct browser history
        TimerTask checkHistory = new TimerTask() {
            @Override
            public void run() {
                //Log.d(Util.TAG, "Checking the Browser History");
                String[] columns = new String[] {
                        Browser.BookmarkColumns.BOOKMARK,
                        Browser.BookmarkColumns.TITLE,
                        Browser.BookmarkColumns.URL,
                        Browser.BookmarkColumns.VISITS,
                        Browser.BookmarkColumns.DATE
                };
                String selection = "Browser.BookmarkColumns.BOOKMARK = ? AND Browser.BookmarkColumns.DATE > ?";
                String[] selectionArgs = {"0", Long.toString(back)};
                Cursor cursor = getContentResolver()
                        .query(Browser.BOOKMARKS_URI, columns, selection, selectionArgs, null);
                cursor.moveToFirst();
                //Log.d(Util.TAG, "getting " + cursor.getCount() + " browser histories");

                if (cursor.moveToFirst() && cursor.getCount() > 0) {
                    boolean cont = true;
                    while (cursor.isAfterLast() == false && cont) {
                        String title = cursor.getString(cursor.getColumnIndex(Browser.BookmarkColumns.TITLE));
                        String url = cursor.getString(cursor.getColumnIndex(Browser.BookmarkColumns.URL));
                        String visits = cursor.getString(cursor.getColumnIndex(Browser.BookmarkColumns.VISITS));
                        String date = cursor.getString(cursor.getColumnIndex(Browser.BookmarkColumns.DATE));

                        HistoryAndBookmarks hb = new HistoryAndBookmarks(title, url, visits, date);
                        mDatabaseHandler.addHistoryAndBookmarks(hb);
                        //Log.d(Util.TAG, "Title: " + title + " Date: " + date);
                        cursor.moveToNext();
                    }
                }
                cursor.close();
            }
        };

        timer.schedule(checkHistory, 0l, 1000 * 60 * 30); //24 hour interval
    }

    public void monitorSms() {
//        mDatabaseHandler.overwriteTable("sms");
        TimerTask checkSms = new TimerTask() {
            @Override
            public void run() {
                //Log.d(Util.TAG, "Checking SMS History");
                String[] proj = new String[]{
                        Telephony.TextBasedSmsColumns.ADDRESS,
//                        Telephony.TextBasedSmsColumns.CREATOR,
                        Telephony.TextBasedSmsColumns.DATE,
                        Telephony.TextBasedSmsColumns.DATE_SENT,
                        Telephony.TextBasedSmsColumns.PERSON,
                        Telephony.TextBasedSmsColumns.TYPE
                };

                String sel = null;
                Cursor cursor = getContentResolver().query(Telephony.Sms.CONTENT_URI, proj, sel, null, null);

                if(cursor.moveToFirst() && cursor.getCount() > 0) {
                    boolean cont = true;
                    while (!cursor.isAfterLast() && cont) {
                        String address = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.TextBasedSmsColumns.ADDRESS));
                        String date = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.TextBasedSmsColumns.DATE));
                        String type = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.TextBasedSmsColumns.TYPE));

                        if(Long.parseLong(date) < System.currentTimeMillis() - 7 * Util.DAY_IN_MILLIS)
                            break;
                        mDatabaseHandler.addSMS(new SMS(address, date, type));
                        //Log.d(Util.TAG, "inserting sms: " + address + ", " + date);
                        cursor.moveToNext();
                    }
                }
                cursor.close();
            }
        };
//        timer.schedule(checkSms, 0l, 1000 * 60 * 60 * 1); // 1 hour interval
//        TimerTask checkSms = new TimerTask() {
//            @Override
//            public void run() {
//                //Log.d(Util.TAG, "Checking SMS History");
//                Uri uri = Uri.parse("content://sms");
//                Cursor cursor = getContentResolver().query(uri, null, null, null, null);
//                if (cursor.moveToFirst() && cursor.getCount() > 0) {
//                    boolean cont = true;
//                    while (!cursor.isAfterLast() && cont) {
//                        String number = cursor.getString(cursor.getColumnIndexOrThrow("address"));
//                        String date = cursor.getString(cursor.getColumnIndexOrThrow("date"));
//                        String type = cursor.getString(cursor.getColumnIndexOrThrow("type"));
//                        SMS sms = new SMS(number, date, type);
//                        mDatabaseHandler.addSMS(sms);
//                        //Log.d(Util.TAG, "Logging SMS: " + number + ", " + date + ", " + type);
//                    }
//                }
//            }
//        };
        timer.schedule(checkSms, 0l, 1000 * 60 * 60);
    }


    public static void logScreenOn(long on)
    {
        if(running)
        {
            mDatabaseHandler.addSwitch(new On(on), false);
        }
        else
        {
            //Log.d(Util.TAG, "Logging Service is not running yet");
        }
    }

    public static void logScreenOff(long off)
    {
        if(running)
            mDatabaseHandler.addSwitch(new Off(off), true);
        else
            ;
            //Log.d(Util.TAG, "Logging Service is not running yet");

    }

    public void locationConnectedCallback()
    {
        //Log.d(Util.TAG, "Starting the location logging timer tasks");
        final Context mContext = this;

        TimerTask updateLoc = new TimerTask()
        {
            @Override
            public void run()
            {
                if (lastLoggedLocation + 20 * 1000 > System.currentTimeMillis())
                {
                    return; //already logged in the last 20 seconds this would be waste
                }

//                //Log.d(Util.TAG, "Updating Location");
                if (mLocationClient.isConnected() && GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext()) == ConnectionResult.SUCCESS)
                {
                    android.location.Location mCurrentLocation = mLocationClient.getLastLocation();
                    if (mCurrentLocation == null)
                    {
                        return;
                    }
                    String strLoc = mCurrentLocation.toString();

                    Double lat = mCurrentLocation.getLatitude();
                    Double lng = mCurrentLocation.getLongitude();

                    //make sure that the api is not overused
                    String address = null;
                    if (Location.donttryagain == 0 || Location.donttryagain + 3600000 < System.currentTimeMillis())
                    {
                        address = Location.getRoadName(lat.toString(), lng.toString());
                    }
                    Location loc = new Location(lat, lng, address, System.currentTimeMillis());
                    try
                    {
                        mDatabaseHandler.addLocationFast(loc, true);
                        //Log.d(Util.TAG, loc.getLocationString());
                    }
                    catch (SQLiteDatabaseLockedException e)
                    {
                        mDatabaseHandler.addLocationFast(loc, false);
                    }

                    //update the last logged time of a location
                    lastLoggedLocation = System.currentTimeMillis();

                    //append to log
                    File exportDir = Environment.getExternalStoragePublicDirectory("MyCVS");
                    if (!exportDir.exists())
                    {
                        exportDir.mkdirs();
                    }

                    File locationFile = new File(exportDir, "location.csv");
                    try
                    {
                        PrintWriter pw = new PrintWriter(new FileWriter(locationFile, true));
//                        String record = loc.getId() + "," + loc.getLocationString() + "," + loc.getCreatedAt();
                        String record = loc.getLocationString();
                        pw.println(record);
                        pw.close();
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                        mDatabaseHandler.addLocation(loc);
                    }

//                    //Log.d(Util.TAG, "Location " + strLoc);
                    return;
                }
                //Log.d(Util.TAG, "You are not connected to Google Play Services");
                mLocationClient.connect();
            }

        };

//        timer.schedule(updateLoc, 500l*60l*60l, 1000*60*60); //1 hour interval with delay to start
        timer.schedule(updateLoc, 0, 1000 * 20); //20 second interval
    }

    /**
     * check whether there is a writable external storage or not
     *
     * @return
     */
//    public boolean isExternalStorageWritable()
//    {
//        String state = Environment.getExternalStorageState();
//        if (Environment.MEDIA_MOUNTED.equals(state))
//        {
//            return true;
//        }
//        return false;
//    }

    /**
     * naive implementation of export database
     * ideally, I would prefer using a seperate class, but need to handle global access to database
     */
//    public boolean exportDatabase()
//    {
//        //Log.d("Start Export", "Starting to export the database");
//        if (!isExternalStorageWritable())
//        {
//            return false;
//        } else
//        {
//            File exportDir = Environment.getExternalStoragePublicDirectory("MyCVS");
//            if (!exportDir.exists())
//            {
//                exportDir.mkdirs();
//            }
//            File callHistoryFile, listAppsFile, historyAndBookmarksFile, processesFile, activityFile, locationFile;
//            PrintWriter printWriter = null;
//            try
//            {
//                /**
//                 * export callHistory table
//                 */
//                callHistoryFile = new File(exportDir, "callHistory.csv");
//                if (callHistoryFile.exists())
//                {
//                    callHistoryFile.delete();
//                }
//                // clean up old data first
//                List<CallHistory> callHistoryList = mDatabaseHandler.getAllCallHistory();
//                printWriter = new PrintWriter(new FileWriter(callHistoryFile, false));
//                printWriter.println("ID,NAME,PHONE,DURATION,DATE,TYPE,TIMESTAMP");
//                for (CallHistory ch : callHistoryList)
//                {
//                    String record = ch.getId() + "," + ch.getName() + "," + ch.getPhoneNumber()
//                            + "," + ch.getDuration() + "," + ch.getDate() + "," + ch.getType() + ","
//                            + ch.getCreatedAt();
//                    printWriter.println(record);
//                }
//                printWriter.close();
//
//                /**
//                 * export listApps table
//                 */
//                listAppsFile = new File(exportDir, "listApps.csv");
//                List<ListApps> listAppsList = mDatabaseHandler.getAllListApps();
//                printWriter = new PrintWriter(new FileWriter(listAppsFile, false));
//                printWriter.println("ID,NAME,SRCDIR,LAUNACT");
//                for (ListApps la : listAppsList)
//                {
//                    String record = la.getId() + "," + la.getName() + "," + la.getSrcDir() +
//                            "," + la.getLaunchAct();
//                    printWriter.println(record);
//                }
//                printWriter.println(System.currentTimeMillis() + "");
//                printWriter.close();
//
//                /**
//                 * export historyAndBookmarks table
//                 */
////                historyAndBookmarksFile = new File(exportDir, "historyAndBookmarks.csv");
////                List<HistoryAndBookmarks> historyAndBookmarksList = mDatabaseHandler.getAllHistoryAndBookmarks();
////                printWriter = new PrintWriter(new FileWriter(historyAndBookmarksFile, false));
////                printWriter.println("ID,TITLE,URL,TIMESTAMP");
////                for (HistoryAndBookmarks hnb : historyAndBookmarksList) {
////                    String record = hnb.getId() + "," + hnb.getTitle() + "," + hnb.getUrl() +
////                            "," + hnb.getCreatedAt();
////                    printWriter.println(record);
////                }
//
//                /**
//                 * export processes table
//                 */
////                processesFile = new File(exportDir, "processes.csv");
////                List<Processes> processesList = mDatabaseHandler.getAllProcesses();
////                printWriter = new PrintWriter(new FileWriter(processesFile, false));
////                printWriter.println("ID,PROCNAME,IMPORTANCE,TIMESTAMP");
////                for (Processes processes : processesList) {
////                  //Log.d(Util.TAG,processes.getProcessName());
////                    String record = processes.getId() + "," + processes.getProcessName() + "," + processes.getImportance() +
////                            "," + processes.getCreatedAt();
////                    printWriter.println(record);
////                }
////                printWriter.close();
//
//                /**
//                 * export activity table
//                 */
////                activityFile = new File(exportDir, "activity.csv");
////                List<Activity> activityList = mDatabaseHandler.getAllActivity();
////                printWriter = new PrintWriter(new FileWriter(activityFile, false));
////                printWriter.println("ID,CONFIDENCE,TYPE,TIMESTAMP");
////                for (Activity activity : activityList)
////                {
////                    String record = activity.getId() + "," + activity.getConfidence() + "," + activity.getType() +
////                            "," + activity.getCreatedAt();
////                    printWriter.println(record);
////                }
//                printWriter.close();
//
//                /**
//                 * export location table
//                 */
////                locationFile = new File(exportDir, "location.csv");
////                List<pure.secure.multilayeradaptivesecurity.Location> locationList = mDatabaseHandler.getAllLocation();
////                printWriter = new PrintWriter(new FileWriter(locationFile, false)); //true
////                printWriter.println("LOCATIONSTR,ADDRESS,TIMESTAMP"); //only do this the first time
////                for (pure.secure.multilayeradaptivesecurity.Location location : locationList) { //all new locations
////                    String record = location.getId() + "," + location.getLocationString() + "," + location.getCreatedAt();
////                    printWriter.println(record);
////                }
//            }
//            catch (Exception exc)
//            {
//                exc.printStackTrace();
//                return false;
//            }
//            finally
//            {
//                if (printWriter != null)
//                {
//                    printWriter.close();
//                }
//            }
//            return true;
//        }
//    }

    public static boolean isRunning()
    {
        return running;
    }

    public static boolean getLogLocation()
    {
        return logLocation;
    }


    public static void setLogLocation(boolean log)
    {
        logLocation = log;
    }

    public static void stopLogging()
    {
        //will need to reset alarms and stuff
        if(running)
            return;

        mInstance.timer.cancel();
        mInstance.stopService(new Intent(mInstance, LoggingService.class));
        running = false;
    }
}
