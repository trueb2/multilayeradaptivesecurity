package uiuc.research.appguardian.questions.screenquestions;

/**
 * Created by Jacob on 2/26/2015.
 */
public class On extends Switch
{
    public On(long timestamp)
    {
        super(ON, timestamp);
    }
}
