package uiuc.research.appguardian.questions.browserquestions;

import android.util.Log;

import com.google.android.gms.games.quest.Quest;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;
import java.util.TreeMap;

import uiuc.research.appguardian.database.HistoryAndBookmarks;
import uiuc.research.appguardian.questions.Question;
import uiuc.research.appguardian.services.QuestionService;
import uiuc.research.appguardian.util.Util;

/**
 * Created by qiuding on 4/29/2015.
 */
public class BrowserHistoryAnalyze {
    private static BrowserHistoryDataSet browserHistoryDataSet = new BrowserHistoryDataSet();

    /*
     * Recieve data from QuestionHandler
     */
    public static void fillDatabase(ArrayList<HistoryAndBookmarks> habs)
    {
        browserHistoryDataSet.setHaBs(habs);
    }

    /**
     * Makes  a question about the most visited site in a period of time
     */
    public static Question mostVisitedWebsite(int daysBack)
    {
        TreeMap<Integer, String> sbv = browserHistoryDataSet.getSitesByVisits();

        if(sbv.size() < 4)
        {
            //Log.d(Util.TAG, "Less than four entries in sitesByVisits");
            return new Question(null, null, null, QuestionService.QT_MOST_VISITED_WEBSITE);
        }

        String query = "Which website did you visit the most yesterday?";
        ArrayList<String> options = new ArrayList<>(4);

        String answer = sbv.get(sbv.lastKey());
        options.add(answer);
        options.add(sbv.get(sbv.firstKey()));
        sbv.remove(sbv.firstKey());
        sbv.remove(sbv.lastKey());
        options.add(sbv.get(sbv.firstKey()));
        sbv.remove(sbv.firstKey());
        options.add(sbv.get(sbv.firstKey()));

        return new Question(query, answer, options, QuestionService.QT_MOST_VISITED_WEBSITE);
    }


}
