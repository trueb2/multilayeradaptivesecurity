package uiuc.research.appguardian.questions.screenquestions;

/**
 * Created by Jacob on 2/26/2015.
 */
public class Off extends Switch
{
    public Off(long timestamp)
    {
        super(OFF, timestamp);
    }
}
