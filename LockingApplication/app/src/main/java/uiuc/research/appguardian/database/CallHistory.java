package uiuc.research.appguardian.database;

/**
 * Created by qiuding on 9/30/2014.
 * <p/>
 * This class construct the call history object
 * <p/>
 * Took http://www.androidhive.info/2011/11/android-sqlite-database-tutorial/ as a reference
 * both for this class along with the database class
 */
public class CallHistory implements Loggable
{

    // private variables
    int id;
    String name;
    String phoneNumber;
    String duration;
    String date;
    int type;
    String createdAt;

    // empty constructor
    public CallHistory()
    {
    }

    // Test purpose constructor
    public CallHistory(String date) {
        this.date = date;
    }

    // constructor
    public CallHistory(String name, String phoneNumber, String duration, String date, int type)
    {
        this.id = id;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.duration = duration;
        this.date = date;
        this.type = type;
        this.createdAt = createdAt;
    }

    // getter and setter
    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getDuration()
    {
        return duration;
    }

    public void setDuration(String duration)
    {
        this.duration = duration;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public int getType()
    {
        return type;
    }

    public void setType(int type)
    {
        this.type = type;
    }

    public String getCreatedAt()
    {
        return createdAt;
    }

    public void setCreatedAt(String createdAt)
    {
        this.createdAt = createdAt;
    }

    public String logLine()
    {
        return name + ", " + phoneNumber + ", " + duration + ", " +
                date + ", " + type + ", " + createdAt + "\n";
    }
}
