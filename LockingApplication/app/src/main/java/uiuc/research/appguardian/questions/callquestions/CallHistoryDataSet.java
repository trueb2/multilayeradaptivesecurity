package uiuc.research.appguardian.questions.callquestions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

import uiuc.research.appguardian.database.CallHistory;
import uiuc.research.appguardian.util.Util;

/**
 * CallHistoryDataSet gets an ArrayList of CallHistory,
 * process the data in that ArrayList, and output the
 * processed data to CallHistoryAnalyze
 */
public class CallHistoryDataSet {
    // all CallHistory
    private ArrayList<CallHistory> callHistories;
    private TreeMap<Integer, String> mostOccurringContactTreeMap = new TreeMap<>();
    private ArrayList<String> contactList = new ArrayList<>();
    private String lastContact;

    /**
     * instead of making a bunch of static methods, I'm trying to make
     * this class more like a normal oop class
     * @param callHistories data from database
     */
    public CallHistoryDataSet(ArrayList<CallHistory> callHistories) {
        this.callHistories = callHistories;
    }

    /**
     *
     * @return TreeMap containing (Number of calls, Name)
     */
    public TreeMap<Integer, String> getMostOccurringContactTreeMap(int daysBack) {
        calculateMostOccurringContact(daysBack);
        return mostOccurringContactTreeMap;
    }

    /**
     *
     * @return a recent contact list
     */
    public ArrayList<String> getContactList() {
        return contactList;
    }

    /**
     * calculate most occurring contact based on callHistories
     */
    private void calculateMostOccurringContact(int daysBack) {
        HashMap<String, Integer> callerFrequencyHashMap = new HashMap<>();
        ArrayList<CallHistory> localCallHistories = new ArrayList<>();
        long backTimeLimit = System.currentTimeMillis() - Util.DAY_IN_MILLIS * daysBack;

        // get a subset of callhistories for question generation
        for (CallHistory callHistory : callHistories) {
            if (Long.parseLong(callHistory.getDate()) > backTimeLimit) {
                localCallHistories.add(callHistory);
            }
        }

        // count frequency
        for (CallHistory callHistory : localCallHistories) {
            String name = callHistory.getName();
            // if name is not in the HashMap
            if (callerFrequencyHashMap.get(name) == null) {
                callerFrequencyHashMap.put(name, 1);
                this.contactList.add(name);
            }
            // it's already in the HashMap
            else {
                int currentCount = callerFrequencyHashMap.get(name);
                currentCount += 1;
                callerFrequencyHashMap.put(name, currentCount);
            }
        }
        // sort using a TreeMap
        for (String name : callerFrequencyHashMap.keySet()) {
            this.mostOccurringContactTreeMap.put(callerFrequencyHashMap.get(name), name);
        }
    }

    public String getLastContact() {
        return lastContact;
    }

    public void setLastContact(String lastContact) {
        this.lastContact = lastContact;
    }
}
