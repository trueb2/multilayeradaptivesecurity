package uiuc.research.appguardian.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

import uiuc.research.appguardian.database.DatabaseHandler;
import uiuc.research.appguardian.database.SMS;
import uiuc.research.appguardian.util.Util;

public class IncomingSmsReceiver extends BroadcastReceiver {
    final SmsManager smsManager = SmsManager.getDefault();

    public IncomingSmsReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final Bundle bundle = intent.getExtras();
        DatabaseHandler databaseHandler = DatabaseHandler.getInstance(context);

        try {
            if (bundle != null) {
                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String num = currentMessage.getDisplayOriginatingAddress();
                    String date = Long.toString(currentMessage.getTimestampMillis());
                    int type = 1; // incoming message

                    // TODO switch logging sms from TimerTask in logging service to this broadcast receiver
//                    Log.i(Util.TAG, "incoming sms: " + num + ", " + date);
//
//                    SMS sms = new SMS(num, date, type);
//                    databaseHandler.addSMS(sms);
                }
            }
        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" + e);
        }
    }
}
