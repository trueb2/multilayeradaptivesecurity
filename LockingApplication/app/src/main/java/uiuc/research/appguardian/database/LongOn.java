package uiuc.research.appguardian.database;

import uiuc.research.appguardian.questions.screenquestions.Off;
import uiuc.research.appguardian.questions.screenquestions.On;
import uiuc.research.appguardian.util.Util;

/**
 * Created by Jacob on 3/1/2015.
 */
public class LongOn implements Loggable
{
    private On on;
    private Off off;

    public LongOn()
    {
        on = null;
        off = null;
    }

    public LongOn(On on, Off off)
    {
        this.on = on;
        this.off = off;
    }

    /**
     * This should never be negative but sometimes it is so idk how to have more reliable
     * logging of this or how that could be fixed.
     * @return
     */
    public long getDuration()
    {
        if(on == null)
            return 0;
        return Math.abs(off.getTimestamp() - on.getTimestamp());
    }

    public String toString() {if(on == null)
        return "default LongOn";
        return Util.convertStamp(on.getTimestamp()) + " for " + Util.convertMillis(getDuration()); }

    public long getStart()
    {
        if(on == null)
            return 0;
        return on.getTimestamp();
    }

    public boolean hasOn()
    {
        return on != null;
    }
    public boolean hasOff()
    {
        return off != null;
    }

    public void setOn(On on)
    {
        this.on = on;
    }

    public void setOff(Off off)
    {
        this.off = off;
    }

    public long getOnTimestamp()
    {
        return on.getTimestamp();
    }

    public long getOffTimestamp()
    {
        return off.getTimestamp();
    }

    public String logLine()
    {
        return on.logLine() + ", " + off.logLine() + "\n";
    }

}

