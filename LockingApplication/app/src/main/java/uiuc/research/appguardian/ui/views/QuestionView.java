package uiuc.research.appguardian.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedList;

import uiuc.research.appguardian.questions.Question;
import uiuc.research.appguardian.services.LockService;
import uiuc.research.appguardian.services.QuestionService;

/**
 * Created by Jacob on 1/14/2015.
 */
public class QuestionView extends LinearLayout
{
    public static final int ONE_QUESTION = 1;
    public static final int TWO_QUESTIONS = 2;
    public static final int THREE_QUESTIONS = 3;
    public static final int FOUR_QUESTIONS = 4;
    public static final int FIVE_QUESTIONS = 5;
    private int numQs; //the remaining number of questions to ask before granting access
    private Question mQuestion;
    private LinkedList<Question> mQuestions;
    private ArrayList<TextView> textViews;
    private boolean answered = true;
    private boolean viewShown = false;
    private long questionVisible, questionAnswered;
    private boolean allQuestionsAnsweredCorrectly;
    private int correct = 0;
    private boolean c;
    private LockService mLockService;


    public QuestionView(Context c)
    {
        super(c);
    }

    public QuestionView(Context context, AttributeSet attributeSet)
    {
        super(context, attributeSet);
        numQs = 4;
//
//        int one = attributeSet.getAttributeResourceValue(ONE_QUESTION, -1);
//        int two = attributeSet.getAttributeResourceValue(TWO_QUESTIONS, -1);
//        int three = attributeSet.getAttributeResourceValue(THREE_QUESTIONS, -1);
//        int four = attributeSet.getAttributeResourceValue(FOUR_QUESTIONS, -1);
//        int five = attributeSet.getAttributeResourceValue(FIVE_QUESTIONS, -1);
//
//        if (one != -1)
//        {
//            numQs = 1;
//        } else if (two != -1)
//        {
//            numQs = 2;
//        } else if (three != -1)
//        {
//            numQs = 3;
//        } else if (four != -1)
//        {
//            numQs = 4;
//        } else if (five != -1)
//        {
//            numQs = 5;
//        } else
//        {
//            numQs = 2;
//            //Log.d(Util.TAG, "dont know how many questions to put up");
//        }

        getQuestions();
    }

    @Override
    /**
     * get instances of the textviews
     */
    protected void onFinishInflate()
    {
        super.onFinishInflate();
        textViews = new ArrayList<TextView>();
        TextView tv1 = (TextView) this.getChildAt(0);
        TextView tv2 = (TextView) this.getChildAt(1);
        TextView tv3 = (TextView) this.getChildAt(2);
        TextView tv4 = (TextView) this.getChildAt(3);
        TextView tv5 = (TextView) this.getChildAt(4);

        textViews.add(tv1);
        textViews.add(tv2);
        textViews.add(tv3);
        textViews.add(tv4);
        textViews.add(tv5);

        tv2.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                opt1(v);
            }
        });
        tv3.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                opt2(v);
            }
        });
        tv4.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                opt3(v);
            }
        });
        tv5.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                opt4(v);
            }
        });
    }

    /**
     * makes questions as long as there are questions
     * that are supposed to be asked
     * which means either there are no more questions to be
     * generated or all of the questions that could be generated
     * have already been generated
     */
    public void showQuestions()
    {
        //make sure that there are more questions to ask
        if(mQuestions.size() == 0)
            getQuestions();

        if(mQuestions.size() == 0)
        {
            hide();
            //Log.d(Util.TAG, "There were no questions");
            return;
        }

        //post another question if there are still questions requested
        if (numQs-- > 0)
        {
            //Log.d(Util.TAG, "Another iteration of showQuestions with current value " + numQs + " num questions " + mQuestions.size());
            mQuestion = mQuestions.remove();

            //find the next complete question in the questions queued
            int count = 1;
            while (!mQuestion.COMPLETE && count++ < 20 * numQs)
            {
                //Log.d(Util.TAG, "Bad Question: " + mQuestion);
                if(mQuestions.size() != 0)
                    mQuestion = mQuestions.remove();
            }

            //there were no questions in the queue that could be asked
            if (!mQuestion.COMPLETE)
                hide();
            //there is a valid quesiton to ask
            else
                postQuestion();
        } else
        {
            //showQuestions was called despite numQs being < 1
            hide();
        }
    }

    /**
     * Assigns the TextViews in the Question Lock
     * Screen their String values
     */
    public void postQuestion()
    {
        if (!mQuestion.COMPLETE)
        {
            //should never be here ever
            mQuestion = null;
            return;
        }
        answered = false;
        textViews.get(0).setText(mQuestion.getQuery());
        //Log.d(Util.TAG, mQuestions.size() + " ||| " + mQuestion.getQuestionType() + " ||| " + mQuestion.getQuery());
        ArrayList<String> mOptions = mQuestion.getOptions();
        for (int i = 0; i < 4; i++)
        {
            //Log.d(Util.TAG, mOptions.get(i));
            textViews.get(i + 1).setText(mOptions.get(i));
        }
        for (int i = 0; i < 4; i++)
        {
            if (mQuestion.isCorrect(i + 1))
            {
                //Log.d(Util.TAG, mQuestion.getOptions().get(i) + " is correct");
            }
        }
        viewShown = true;
        questionVisible = System.currentTimeMillis();
        QuestionService.getServerPostRequest().setQuestionType(mQuestion.getQuestionType());
        QuestionService.getServerPostRequest().setWhenQuestionAsked(questionVisible); //last part of question
    }

    /**
     * sets the number of questions to show
     * and generates the questions
     *
     * @param qs
     */
    public void setNumQs(int qs, LockService ls)
    {
        numQs = qs;
        mLockService = ls;
        //showQuestions();
    }

    /**
     * Gets Questions that have already been generated from
     * the Question Service
     */
    private void getQuestions()
    {
        QuestionService qs = QuestionService.getInstance();
        mQuestions = (qs == null) ? null : qs.getQuestions(numQs);
    }

    /**
     * option 1 chosen
     *
     * @param v
     */
    public void opt1(View v)
    {
        answered = answered();
        c = mQuestion.isCorrect(1);
        finishAndLog(c);
        //Log.d(Util.TAG, c + "");
        Toast.makeText(getContext(), c + "", Toast.LENGTH_SHORT).show();
        showQuestions(); //if there are more questions to be asked they will be asked
    }

    /**
     * option 2 chosen
     *
     * @param v
     */
    public void opt2(View v)
    {
        answered = answered();
        c = mQuestion.isCorrect(2);
        finishAndLog(c);
        //Log.d(Util.TAG, c + "");
        Toast.makeText(getContext(), c + "", Toast.LENGTH_SHORT).show();
        showQuestions(); //if there are more questions to be asked they will be asked
    }

    /**
     * option 3 chosen
     *
     * @param v
     */
    public void opt3(View v)
    {
        answered = answered();
        c = mQuestion.isCorrect(3);
        finishAndLog(c);
        //Log.d(Util.TAG, c + "");
        Toast.makeText(getContext(), c + "", Toast.LENGTH_SHORT).show();
        showQuestions(); //if there are more questions to be asked they will be asked
    }

    /**
     * option 4 chosen
     *
     * @param v
     */
    public void opt4(View v)
    {
        answered();
        c = mQuestion.isCorrect(4);
        finishAndLog(c);
        //Log.d(Util.TAG, c + "");
        Toast.makeText(getContext(), c + "", Toast.LENGTH_SHORT).show();
        showQuestions(); //if there are more questions to be asked they will be asked
    }

    public void hide()
    {
        if (getVisibility() == VISIBLE || viewShown)
        {
            ((QuestionService) getContext()).hideView();
        }
        viewShown = false;
    }

    //if a view is still up for some reason ??? get rid of it
    private boolean answered()
    {
        if(answered)
            hide();

        return true;
    }

    /**
     * completes the necessary fields to be send the qeustion results to the server
     */
    private void finishAndLog(boolean correct)
    {
        allQuestionsAnsweredCorrectly = allQuestionsAnsweredCorrectly && correct;
        QuestionService.getServerPostRequest().setAnsweredCorrectly(correct);
        questionAnswered = System.currentTimeMillis();
        QuestionService.getServerPostRequest().setTimeInScreen(questionAnswered - questionVisible);
        if(correct)
            this.correct += 1;

        if(numQs == 0) {
            //Log.d(Util.TAG, "Sending " + this.correct + " answers answered correctly");
            mLockService.checkDone(this.correct);
            this.correct = 0;
        }

    }


    public boolean isViewShown()
    {
        return viewShown;
    }

}
