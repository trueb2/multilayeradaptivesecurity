package uiuc.research.appguardian.servercommunications;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.UUID;

import uiuc.research.appguardian.services.QuestionService;

/**
 * Created by jwtrueb on 4/20/15.
 */
public class ServerPostRequest {

    //keys that have to be associated with their values in order to be sent to the server
    public static final String KEY_QUESTION_TYPE = "q";
    public static final String KEY_ANSWERED_CORRECTLY = "c";
    public static final String KEY_TIME_IN_SCREEN = "t";
    public static final String KEY_WHEN_ASKED = "w";
    public static final String KEY_ANDROID_VERSION = "v";
    public static final String KEY_DEVICE_HASH = "h";

    private int questionType;
    private boolean answeredCorrectly;
    private long timeInScreen;
    private long whenQuestionAsked;
    private String androidVersion;
    private String id;
    private int hashedID;

    private boolean ready, qtSet, acSet, tisSet, wqaSet;
    private boolean sending, sent;

    //should not be used
    public ServerPostRequest(Context context)
    {
        androidVersion = Build.VERSION.RELEASE;
        TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        id = tm.getDeviceId();
        hashedID = id.hashCode();
        sending = sent = false;
    }

    //should be the only constructor used
    public ServerPostRequest(int questionType,
                             boolean answeredCorrectly,
                             long timeInScreen,
                             long whenQuestionAsked,
                             Context context)
    {
        this.questionType = questionType;
        this.answeredCorrectly = answeredCorrectly;
        this.timeInScreen = timeInScreen;
        this.whenQuestionAsked = whenQuestionAsked;
        androidVersion = Build.VERSION.RELEASE;
        TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        id = tm.getDeviceId();
        hashedID = id.hashCode();

        qtSet = true;
        acSet = true;
        tisSet = true;
        wqaSet = true;
        ready = true;
        submitIfReady();
    }

    public void setQuestionType(int questionType)
    {
        qtSet = true;
        checkReady();
        this.questionType = questionType;
        submitIfReady();
    }

    public void setAnsweredCorrectly(boolean answeredCorrectly)
    {
        acSet = true;
        checkReady();
        this.answeredCorrectly = answeredCorrectly;
        submitIfReady();
    }

    public void setTimeInScreen(long timeInScreen)
    {
        tisSet = true;
        checkReady();
        this.timeInScreen = timeInScreen;
        submitIfReady();
    }

    public void setWhenQuestionAsked(long whenQuestionAsked)
    {
        wqaSet = true;
        checkReady();
        this.whenQuestionAsked = whenQuestionAsked;
        submitIfReady();
    }

    public void checkReady()
    {
        if(qtSet == acSet && acSet == tisSet && tisSet == wqaSet && wqaSet == true)
            ready = true;
        //Log.d(Util.TAG, ready + " " +  qtSet + " "  +  acSet + " " +  tisSet + " " +  wqaSet);
    }

    public boolean isSending()
    {
        return sending;
    }

    public boolean isSent()
    {
        return sent;
    }

    public void submitIfReady()
    {
        if(!ready)
            return;

        //Log.d(Util.TAG, "Going to submit to server");
        sending = true;

        AsyncTask post = new AsyncTask<Object, Void, Void>(){
            private String string;

            protected Void doInBackground(Object ... empty)
            {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost("http://ttat-control.iti.illinois.edu:5900/webfiles/log.php");

                try
                {
                    ArrayList<NameValuePair> nameValuePairs = new ArrayList<>();
                    nameValuePairs.add(new BasicNameValuePair(KEY_QUESTION_TYPE, questionType+""));
                    nameValuePairs.add(new BasicNameValuePair(KEY_ANSWERED_CORRECTLY, answeredCorrectly+""));
                    nameValuePairs.add(new BasicNameValuePair(KEY_TIME_IN_SCREEN, timeInScreen+""));
                    nameValuePairs.add(new BasicNameValuePair(KEY_WHEN_ASKED, whenQuestionAsked+""));
                    nameValuePairs.add(new BasicNameValuePair(KEY_ANDROID_VERSION, androidVersion));
                    nameValuePairs.add(new BasicNameValuePair(KEY_DEVICE_HASH, hashedID + ""));

                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    HttpResponse httpResponse = httpClient.execute(httpPost);
                    //Log.d(Util.TAG, "Response: " + httpResponse.getStatusLine().toString());

                }
                catch(ClientProtocolException e)
                {
                    e.printStackTrace();
                }
                catch(UnsupportedEncodingException e)
                {
                    e.printStackTrace();
                }
                catch(IOException e)
                {
                    e.printStackTrace();
                }

                return null;
            }

            protected void onPostExecute(Void v)
            {
                //sent something to server
                sent = true;
                //Log.d(Util.TAG, "Sent to the Server");
                if(QuestionService.getServerPostRequest().isSent())
                    QuestionService.resetServerPostRequest();
            }

        };

        post.execute();

    }
}
