package uiuc.research.appguardian.database;

/**
 * Created by qiuding on 10/16/2014.
 */
public class HistoryAndBookmarks implements Loggable
{

    int id;
    String title;
    String url;
    String date;
    String visits;
    String createdAt;

    public HistoryAndBookmarks()
    {
    }

    public HistoryAndBookmarks(String title, String url)
    {
        this.title = title;
        this.url = url;
    }

    public HistoryAndBookmarks(String title, String url, String visits, String date)
    {
        this.title = title;
        this.url = url;
        this.visits = visits;
        this.date = date;
    }

    public String getVisits() {
        return visits;
    }

    public void setVisits(String visits) {
        this.visits = visits;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getCreatedAt()
    {
        return createdAt;
    }

    public void setCreatedAt(String createdAt)
    {
        this.createdAt = createdAt;
    }

    public String getDate()
    {
        return date;
    }

    public String logLine()
    {
        return title + ", " + url + ", " + visits + ", " + createdAt + "\n";
    }
}
