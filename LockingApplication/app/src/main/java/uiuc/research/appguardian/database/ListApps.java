package uiuc.research.appguardian.database;

/**
 * Created by qiuding on 10/9/2014.
 */
public class ListApps implements Loggable
{

    int id;
    String name;
    String srcDir;
    String launchAct;

    public ListApps()
    {
    }

    public ListApps(String name, String srcDir, String launchAct)
    {
        this.id = id;
        this.name = name;
        this.srcDir = srcDir;
        this.launchAct = launchAct;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getSrcDir()
    {
        return srcDir;
    }

    public void setSrcDir(String srcDir)
    {
        this.srcDir = srcDir;
    }

    public String getLaunchAct()
    {
        return launchAct;
    }

    public void setLaunchAct(String launchAct)
    {
        this.launchAct = launchAct;
    }

    public String logLine()
    {
        return name + ", " + srcDir + ", " + launchAct + "\n";
    }
}
