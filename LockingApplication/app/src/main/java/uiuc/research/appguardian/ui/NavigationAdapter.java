package uiuc.research.appguardian.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import uiuc.research.appguardian.R;
import uiuc.research.appguardian.services.AppLockService;

public class NavigationAdapter extends BaseAdapter
{

    private Context mContext;
    private LayoutInflater mInflater;
    private List<NavigationElement> mItems;

    private boolean mServiceRunning = false;

    public NavigationAdapter(Context context)
    {
        mContext = context;
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mServiceRunning = AppLockService.isRunning(context);
        mItems = new ArrayList<NavigationElement>();
        setupElements();
    }

    public NavigationElement getItemFor(int type)
    {
        return mItems.get(getPositionFor(type));
    }

    public int getPositionFor(int type)
    {
        for (int i = 0; i < mItems.size(); i++)
        {
            if (mItems.get(i).type == type)
            {
                return i;
            }
        }
        return -1;
    }

    public int getTypeOf(int position)
    {
        return mItems.get(position).type;
    }

    public void setServiceState(boolean newState)
    {
        if (mServiceRunning != newState)
        {
            mServiceRunning = newState;
            notifyDataSetChanged();
        }
    }

    private void addElement(int title, int type)
    {
        final NavigationElement el = new NavigationElement();
        el.title = title;
        el.type = type;
        mItems.add(el);
    }

    private void setupElements()
    {
        addElement(R.string.nav_status, NavigationElement.TYPE_STATUS);
        addElement(R.string.nav_apps, NavigationElement.TYPE_APPS);
        addElement(R.string.nav_change, NavigationElement.TYPE_CHANGE);
        addElement(R.string.nav_settings, NavigationElement.TYPE_SETTINGS);
        //addElement(R.string.nav_questions, NavigationElement.TYPE_QUESTIONS); //add Generate Questions
        //addElement(R.string.nav_database, NavigationElement.TYPE_DATABASE);
        addElement(R.string.nav_location, NavigationElement.TYPE_LOG_LOC);
        addElement(R.string.nav_action, NavigationElement.TYPE_LOG_ACT);
        addElement(R.string.nav_browser_hist, NavigationElement.TYPE_LOG_BROWSE);
        addElement(R.string.nav_screen, NavigationElement.TYPE_LOG_SCREEN);
        addElement(R.string.nav_processes, NavigationElement.TYPE_LOG_PROC);
        addElement(R.string.nav_callhistory, NavigationElement.TYPE_LOG_CALL);
        addElement(R.string.nav_smshistory, NavigationElement.TYPE_LOG_SMS);
        addElement(R.string.nav_pose, NavigationElement.TYPE_POSE_Q);
        addElement(R.string.nav_database_to_server, NavigationElement.TYPE_D_TO_S);
    }

    @Override
    public int getCount()
    {
        return mItems.size();
    }

    @Override
    public Object getItem(int position)
    {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewGroup root = (ViewGroup) mInflater.inflate(
                R.layout.navigation_drawer_list_item, null);

        if (mItems.get(position).type == NavigationElement.TYPE_STATUS)
        {
            final CheckBox cb = (CheckBox) root
                    .findViewById(R.id.navFlag);
            cb.setChecked(mServiceRunning);
            cb.setVisibility(View.VISIBLE);
        }

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext.getApplicationContext());

        //assign switch values according to shared prefs
        switch(mItems.get(position).type)
        {
            case NavigationElement.TYPE_LOG_LOC:
                setButton(R.string.nav_location, root, prefs);
                break;
            case NavigationElement.TYPE_LOG_ACT:
                setButton(R.string.nav_action, root, prefs);
                break;
            case NavigationElement.TYPE_LOG_BROWSE:
                setButton(R.string.nav_browser_hist, root, prefs);
                break;
            case NavigationElement.TYPE_LOG_SCREEN:
                setButton(R.string.nav_screen, root, prefs);
                break;
            case NavigationElement.TYPE_LOG_PROC:
                setButton(R.string.nav_processes, root, prefs);
                break;
            case NavigationElement.TYPE_LOG_CALL:
                setButton(R.string.nav_callhistory, root, prefs);
                break;
            case NavigationElement.TYPE_LOG_SMS:
                setButton(R.string.nav_smshistory, root, prefs);
                break;
            default:
                break;
        }



        TextView navTitle = (TextView) root.findViewById(R.id.navTitle);
        navTitle.setText(mItems.get(position).title);
        return root;
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    private static CheckBox getSwitchCompat(Context c)
    {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH)
        {
            return new CheckBox(c);
        } else
        {
//            return new Switch(c);
            return new CheckBox(c);
        }
    }

    private void setButton(int stringNum, ViewGroup root, SharedPreferences prefs)
    {
        final CheckBox cb = (CheckBox) root.
                findViewById(R.id.navFlag);

        boolean setFlag = prefs.getBoolean(mContext.getString(stringNum), true);
        cb.setChecked(setFlag);
        cb.setVisibility(View.VISIBLE);
    }
}
