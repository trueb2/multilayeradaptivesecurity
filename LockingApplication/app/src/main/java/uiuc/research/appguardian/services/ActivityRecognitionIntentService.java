package uiuc.research.appguardian.services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.os.Environment;
import android.util.Log;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import uiuc.research.appguardian.R;
import uiuc.research.appguardian.database.Action;
import uiuc.research.appguardian.database.DatabaseHandler;
import uiuc.research.appguardian.util.Util;

/**
 * Created by jwtrueb on 10/7/14.
 */
public class ActivityRecognitionIntentService extends IntentService
{

    //dummy constructor
    public ActivityRecognitionIntentService()
    {
        super("ActivityRecognitionIntentService");
    }

    private DatabaseHandler mDatabaseHandler = DatabaseHandler.getInstance(this);

    /**
     * Called when a new activity detection update is available
     */
    @Override
    protected void onHandleIntent(Intent intent)
    {
        //If the intent contains an update
        if (ActivityRecognitionResult.hasResult(intent))
        {
            //Get the update
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);

            DetectedActivity mostProbableActivity = result.getMostProbableActivity();

            //Get the confidence % (probability)
            int confidence = mostProbableActivity.getConfidence();

            //Get the type
            int activityType = mostProbableActivity.getType();

            Action activity = new Action(confidence, activityType, System.currentTimeMillis());

            try{
                mDatabaseHandler.addActionFast(activity, true);
                //Log.d(Util.TAG, activity.toString());
            } catch(SQLiteDatabaseLockedException e){
                mDatabaseHandler.addActionFast(activity, false);
            }

//            mDatabaseHandler.addActivity(activity);

            //append to log
            File exportDir = Environment.getExternalStoragePublicDirectory("MyCVS");
            if (!exportDir.exists())
            {
                exportDir.mkdirs();
            }

            File activityFile = new File(exportDir, "activity.csv");
            try
            {
                PrintWriter pw = new PrintWriter(new FileWriter(activityFile, true));
                String record = confidence + "," + activityType + "," + System.currentTimeMillis();
                pw.println(record);
                pw.close();

//                String ns = Context.NOTIFICATION_SERVICE;
//                NotificationManager mNotificationManager = (NotificationManager) getApplicationContext().getSystemService(ns);
//                long when = System.currentTimeMillis();
//                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
//                String date = sdf.format(new Date(when));
//
//                Notification notification = new Notification(R.drawable.walking, "Logged at : " + date, when);
//                notification.setLatestEventInfo(getApplicationContext(), "Recognition Service", "T: " + activityType + " C: " + confidence + " Logged: " + date, null);
//                mNotificationManager.notify(113373, notification);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                mDatabaseHandler.addAction(activity);
            }
//            //Log.d(Util.TAG, "Confidence: " + confidence + " Type: " + activityType);
        }
    }
}
