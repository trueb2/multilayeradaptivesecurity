package uiuc.research.appguardian.questions;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;

import uiuc.research.appguardian.util.Util;


public class Question
{

    private String query;
    private String answer;
    private ArrayList<String> options;
    private int questionType;
    public final boolean COMPLETE;

    public Question(String query, String answer, ArrayList<String> options, int questionType)
    {
        this.query = query;
        this.answer = answer;
        this.options = options;
        this.questionType = questionType;

        if (query != null && answer != null && options != null && options.size() == 4 && options.get(0) != null &&
                options.get(1) != null && options.get(2) != null && options.get(3) != null)
        {
            COMPLETE = true;
        } else
        {
            COMPLETE = false;
            //Log.d(Util.TAG, "Created an incomplete question");
        }
    }

//    public boolean checkAnswer(String response)
//    {
//        return response.equals(answer);
//    }

    @Override
    public String toString()
    {
        if (query == null || answer == null || options == null)
        {
            return "Null Question";
        }
        StringBuffer sb = new StringBuffer();
        sb.append("\n" + query.toString());

        //randomize the order of questions
        if (options == null)
        {
            return "This question does not have enough options to be generated. NULL options";
        }
        Collections.shuffle(options);

        if (options.size() < 4)
        {
            return "This question does not have enough options to be generated. < 4 options";
        }
        String a = options.get(0);
        String b = options.get(1);
        String c = options.get(2);
        String d = options.get(3);

        if (a == null || b == null || c == null || d == null)
        {
            return "This question does not have enough options to be generated. NULL opt.";
        }

        //Show as Choices
//		sb.append("\nA) " + options.get(0));
//		sb.append("\nB) " + options.get(1));
//		sb.append("\nC) " + options.get(2));
//		sb.append("\nD) " + options.get(3));
        sb.append("\n" + options.get(0));
        sb.append("\n" + options.get(1));
        sb.append("\n" + options.get(2));
        sb.append("\n" + options.get(3));

        return sb.toString();
    }

    public boolean answerIsNull()
    {
        return answer == null;
    }

    /**
     * returns whether the option number is the correct answer
     * index is 1-based
     * @param opt
     * @return
     */
    public boolean isCorrect(int opt){
        return options.get(opt-1).equals(answer);
    }
    public String getQuery()
    {
        return query;
    }
    public ArrayList<String> getOptions() {
        return options;
    }
    public int getQuestionType()
    {
        return questionType;
    }
}
