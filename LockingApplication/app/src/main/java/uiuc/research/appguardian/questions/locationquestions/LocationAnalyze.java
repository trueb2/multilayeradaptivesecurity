package uiuc.research.appguardian.questions.locationquestions;

import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.TreeMap;

import uiuc.research.appguardian.database.Location;
import uiuc.research.appguardian.questions.Question;
import uiuc.research.appguardian.util.Util;

/**
 * Created by jwtrueb on 10/23/14.
 */
public class LocationAnalyze
{
//TODO make sure that question options match the form of the answer when passing them as arguments to the constructor

    public static final int WEEK = Calendar.MONDAY + Calendar.TUESDAY + Calendar.WEDNESDAY +
            Calendar.THURSDAY + Calendar.FRIDAY + Calendar.SATURDAY + Calendar.SUNDAY;

    private static LocationDatabase ld = new LocationDatabase();
    private static Calendar cal = Calendar.getInstance();

    /**
     * Generates every location based question given the number of days back to consider
     * the number of days back to stop considering and the day of the week
     * to consider time expenditure
     *
     * @param daysBack to include in database
     * @param upto     to include in database, normally 0 as in today
     * @param dayWeek  day of week
     * @return
     * @throws java.io.FileNotFoundException
     */
//    public static ArrayList<Question> generateAllQuestions(int daysBack, int upto, int dayWeek, int altDayWeek)
//    {
//        long start = System.currentTimeMillis();
////		ld.readAll(); //read the locations from the database
//        if (!ld.isReady())
//        {
////            ld.readAllAfter(daysBack);
//            //Log.d(Util.TAG, "Location Database is not ready");
//        }
//        long stop = System.currentTimeMillis();
//        //Log.d("Timer", "Read in Locations took: " + (stop - start));
//
////		Question mostVisits = mostVisited(daysBack);
//        start = System.currentTimeMillis();
//        Question spentTime = timeAt(dayWeek);
//        stop = System.currentTimeMillis();
//        //Log.d("Timer", "Time Spent At: " + (stop - start) + " ms");
//
//        start = System.currentTimeMillis();
//        Question spentTime1 = timeAt(altDayWeek);
//        stop = System.currentTimeMillis();
//        //Log.d("Timer", "Time Spent At (alternative): " + (stop - start) + " ms");
//
//        start = System.currentTimeMillis();
//        Question wherewereyou = whereWereYou(daysBack);
//        stop = System.currentTimeMillis();
//        //Log.d("Timer", "Where were you: " + (stop - start) + " ms");
//
//        start = System.currentTimeMillis();
//        Question commonDest = mostCommon(false, daysBack, upto);
//        stop = System.currentTimeMillis();
//        //Log.d("Timer", "Most Common Destination: " + (stop - start) + " ms");
//
//        start = System.currentTimeMillis();
//        Question commonSource = mostCommon(true, daysBack, upto);
//        stop = System.currentTimeMillis();
//        //Log.d("Timer", "Most Common Source: " + (stop - start) + " ms");
//
//        ArrayList<Question> questions = new ArrayList<Question>();
////        questions.add(mostVisits);
//        questions.add(spentTime);
//        questions.add(spentTime1);
//        questions.add(wherewereyou);
//        questions.add(commonDest);
//        questions.add(commonSource);
//
//        for (int i = 0; i < questions.size(); i++)
//        {
//            Question question = questions.get(i);
//            if (question.COMPLETE == false)
//            {
//                questions.remove(i);
//                //Log.d(Util.TAG, "Question is not complete : " + question.getQuery());
//            }
//        }
//
//        return questions;
//    }

    /**
     * generates a how long did you spend at ... Question
     * 1. Don't want this to be the most visited location preferably
     * 2. Don't want this to be at a location that might have been for
     * a short period of time
     * 3. 20 minutes to 1 and 20 minutes probably
     * <p/>
     * This will probably be unique with most days, once we have the
     * ability to find trends so that something like work or home does not
     * show up in this question
     * param Integer day of the week to check Calendar Constant
     *
     * @return Question
     */
    public static Question timeAt(int dayofweek, int questionType)
    {
        if (ld.getPlaces() == null)
        {
            return new Question("How much time did you spend at ...", null, null, questionType);
        }

        String day = dayOfWeek(dayofweek);

        String answer = "eh";
        String query = "How much time did you spend at ... ";

        ArrayList<Trip> staycations = staysByWeekDay(dayofweek);

        //so now we have all of the trips compare the durations
        ArrayList<Trip> goodOpts = new ArrayList<Trip>();
        for (Trip stay : staycations)
        {   // 1 hour 15 min < duration < 5 hr
            if (stay.duration > 75 * 60 * 1000 && stay.duration < 5 * 60 * 60 * 1000 && stay.source.getAddress(true) != null && !stay.source.getAddress(false).contains(","))
            {    //good choice for asking a question about
                goodOpts.add(stay);
            }
        }

        if (goodOpts.size() == 0)
        {
            //Log.d(Util.TAG, "Not a single trip greater than 75 minutes and less than 5 hours on " + day);
            return new Question(query, null, null, questionType); //not enough good options
        }

        //with the good choices available randomly choose 4 and assign the Question params
        Collections.shuffle(goodOpts);
        Trip ans = goodOpts.get(0);
        answer = "Starting at " + Util.convertStamp(ans.source.getTimestamp()) + " at " + ans.getSource().getAddress(true);
        ArrayList<String> options = new ArrayList<String>();
        options.add(answer);

        fillTimeAtOptions(options, staycations);

        query = "Where did you spend " + Util.convertMillis(ans.duration) + " on " + day + "?";
        Question question = new Question(query, answer, options, questionType);
        return question;
    }

    /**
     * Generates a question as follows:
     * Where were you at ... on ...?
     * this location can be pulled from any stay within the past few days
     * that is longer than about an hour
     *
     * @return Question
     */
    public static Question whereWereYou(int daysBack, int questionType)
    {
        long start = System.currentTimeMillis() - daysBack * Util.DAY_IN_MILLIS;
        ArrayList<Trip> trips = findStays(ld.getPlaces());
        if(trips == null || trips.size() == 0)
            return new Question(null, null, null, questionType);
        //Log.d(Util.TAG, trips.get(trips.size()-1).getDestination().getTimestamp()+"");
        ArrayList<Trip> options = new ArrayList<Trip>();

        //look for trips that are over an hour long and occurred in the last 3 days
        for (int i = 0; i < trips.size(); i++)
        {
            Trip t = trips.get(i);
            if (t.duration > 60 * 60 * 1000 && t.source.getTimestamp() > start)
            {
                ////Log.d("Trip Options", t.toString());
                options.add(t);
            }
        }

        //not enough options to generate a question
        if (options.size() < 4)
        {
            //Log.d(Util.TAG, "Fewer than 4 options where were you");
            return new Question("Where were you at ... on ...?", null, null, questionType);
        }

        Collections.shuffle(options);
        Collections.shuffle(options);


        Trip l = null;
        double a = options.size();
        double b = 0;
        for (int i = 0; i < options.size(); i++)
        {
            String s = options.get(i).source.getAddress(true);

            if (s != null && !s.contains(",") && !s.equals("null"))
            {
                l = options.get(i);
            } else
            {
                ////Log.d(Util.TAG, s);
                b++;
            }
        }

        //Log.d(Util.TAG, b + "/" + a + "=" +(b/a));

        if (l == null)
        {
            return new Question(null, null, null, questionType);
        }

//        long time = l.source.getTimestamp() + 30 * 60 * 60 * 1000;
        long time = l.source.getTimestamp();

        String query = "Where were you at on " + LocationAnalyze.dayOfWeek(time) + ", " + Util.convertStamp(time) + "?";
        String answer = l.source.getAddress(true);
        ArrayList<String> multichoice = new ArrayList<String>();
        multichoice.add(answer);
        for (Trip z : options)
        {
            if (z == l)
            {
                continue;
            }
            if (multichoice.size() == 4)
            {
                break;
            }

            String s = z.source.getAddress(true);
            if (s != null && !s.equals("null") && !s.equals(answer) && !multichoice.contains(s) && !s.contains(","))
            {
                multichoice.add(s);
            }
        }

        if (multichoice.size() < 4)
        {
            return new Question(query, answer, null, questionType);
        }

        Question question = new Question(query, answer, multichoice, questionType);
        return question;
    }

    /**
     * Generates a Question that asks what the most common destination or source is
     * the boolean parameter determines whether the question is about the most common source
     * if set to false then the question will be about the most common destination
     * time0 is the number of days back to consider in the question
     * and time1 is the number of days back from the current day to stop considering
     * time1 = 0 is current
     *
     * @throws java.io.FileNotFoundException
     */
    public static Question mostCommon(boolean source, int time0, int time1, int questionType)
    {
        ArrayList<Location> common;
        String query = "What is your most common ";
        if (!source)
        {
            common = commonDestination(time0, time1);
            query += "destination ";
        } else
        {
            common = commonSource(time0, time1);
            query += "source of travel ";
        }

        if (time1 != 0)
        {
            query += "between " + time0 + " and " + time1 + " days ago?";
        } else
        {
            query += "over the past " + time0 + " days?";
        }

        if(common == null || common.size() < 4)
        {
            //Log.d(Util.TAG, "Not enough options for common ____ questions");
            return new Question(null, null, null, questionType);
        }

        Location answerLoc = common.get(0);

        String answer = answerLoc.getAddress(true);
        ArrayList<String> options = new ArrayList<String>();

        for (Location loc : common)
        {
            String opt = loc.getAddress(false);
//            if(opt.contains(","))
//                continue;
            options.add(opt);
        }

        Collections.shuffle(options);

        if (options.size() < 4)
        {
            return new Question(query, answer, null, questionType);
        }

        Question question = new Question(query, answer, options, questionType);
        return question;
    }

    /**
     * Generates a question that asks how long a user spent
     * at a location.
     * Things to Consider when choosing the location:
     * 1. Must be a place that is visited fairly often
     * 2. must be a place that was visited for an unusual amount of time
     * ie 3 hours instead of the usual one or two
     * <p/>
     * this one will have to reference previous trips possibly weeks back in order
     * to determine the trend
     *
     * @param trips
     * @return Question
     */
    public static Question howLongWereYouAt(ArrayList<Trip> trips)
    {
        //Communicate with tripdatabase

        //loop through common locations

        //compare usual durations with durations in the past week

        //identify an unusual duration

        //identify 3 more trips with usual durations

        //create Question
        return null;
    }

    public static void printDurations(ArrayList<Trip> trips)
    {
        System.out.println("\nPrinting Durations");
        for (Trip t : trips)
        {
            if (t.duration > 30 * 60 * 1000)
            {
                System.out.println(t.toString() + " " + Util.convertMillis(t.duration));
            }
        }
    }

    /**
     * Going off of: 1. Several Consecutive Logs of the same location 2. A few
     * logs of different location 3. Several Consecutive Logs of the same
     * location
     * forms a Trip
     * <p/>
     * It is likely that the user was traveling
     * <p/>
     * TODO Compare this to the Activity Recognition (Walk, Bike, Run)
     *
     * @param time0 Days back that you are willing to start looking
     * @param time1 Days back that you want to stop looking 0 will be defined as
     *              current
     * @return ArrayList<Location> containing the starting location, the
     * recording traveling locations, and the final location
     * @throws java.io.FileNotFoundException
     */
    public static ArrayList<Trip> findTravel(int time0, int time1)
    {
        ArrayList<Trip> trips = new ArrayList<Trip>();
        ArrayList<Location> time = ld.sortByTime(ld.getPlaces());
        if(time == null)
            return null;

        // Start considering trends at time0
        // Note getTimestamp() is Epoch (ms) getTimestamp() 1 day = 86400000 ms
        long now = System.currentTimeMillis();
        long start = now - Util.DAY_IN_MILLIS * time0;
        long stop = now - Util.DAY_IN_MILLIS * time1;

        Trip first = new Trip();
        trips.add(first);

        //iterate through every location to compare if the location has changed
        Location previous = null, current = null;
        for (int i = 0; i < time.size(); i++)
        {
            int t = trips.size();
            Trip trip = trips.get(t - 1);
            Location loc = time.get(i);

            previous = current;
            current = loc;

            if (loc.getTimestamp() < start) // cant be farther back than the start
            {
                continue;
            }
            if (loc.getTimestamp() > stop) // nothing past here is worth looking at
            {
                break;
            }

            if (previous == null)
            {
                continue;
            }

            if (!previous.equals(current) && !trip.isStarted())
            { //just started to change location
                trip.setSource(previous);
                trip.addToPath(current);
                trip.setStarted(true);
            } else if (!previous.equals(current) && trip.isStarted())
            {   //location is still changing
                trip.addToPath(current);
            } else if (previous.equals(current) && trip.isStarted())
            {   //location has stopped changing
                trip.setDestination(previous);

                //set up for next Trip
                trips.add(new Trip());
            }
        }

        // the last trip may not have ended so remove it from the list
        if (trips.get(trips.size() - 1).getDestination() == null)
        {
            trips.remove(trips.size() - 1);
        }

        return trips;
    }

    /**
     * returns a list of Trips such that the source of the Trip is
     * the same as the destination of the trip and no locations in between the
     * source and the destination are different from the source.
     * this is referred to as a staycation in the code and in essence represents
     * the lack of movement
     *
     * @return ArrayList<Trip> staycations
     */
    public static ArrayList<Trip> findStays(ArrayList<Location> dayLocs)
    {
        if(dayLocs == null)
            return null;
        ArrayList<Trip> staycations = new ArrayList<Trip>();
        Trip first = new Trip();
        staycations.add(first);

        //make sure that the days are order correctly
        dayLocs = ld.sortByTime(dayLocs);


        //find 4 acceptable staycations
        //randomly pick one
        //set up a Question and return it

        long t0, t1;
        Location previous = null, current = null;
        for (Location l : dayLocs)
        {
            int i = staycations.size() - 1;
            Trip staycation = staycations.get(i);

            previous = current;
            current = l;

            if (previous == null || current == null)
            {
                continue;
            }

            if (previous.equals(current) && !staycation.isStarted())
            {   //no change in location
                t0 = previous.getTimestamp();
                //start staycation
                staycation.setSource(previous);
                staycation.setStarted(true);
                staycation.setStaycation();
            } else if (!previous.equals(current) && !staycation.isStarted())
            {   //still moving so dont worry about it
//				System.out.println(previous.toString() + " != " + current.toString());

            } else if (previous.equals(current) && staycation.isStarted())
            {   //on a staycation so dont worry about it
                staycation.addToPath(previous);
//				System.out.println(previous.toString() + " = " + current.toString());
            } else if (!previous.equals(current) && staycation.isStarted())
            {   //started moving again and was stationary before
                staycation.setDestination(previous);
                //set up for the next staycation
                staycations.add(new Trip());
            }
        }


        // remove the final staycation if it did not reach its destination
        if (staycations.get(staycations.size() - 1).getDestination() == null)
        {
            staycations.remove(staycations.size() - 1);
        }
        return staycations;
    }

    /**
     * Looks for the most common source of Trips
     *
     * @param time0
     * @param time1
     * @return the most common source of a Trip
     * @throws java.io.FileNotFoundException
     */
    public static ArrayList<Location> commonSource(int time0, int time1)
    {
        //Log.d(Util.TAG, "Entering commonSource");

        ArrayList<Trip> trips = findTravel(time0, time1);
        if(trips == null)
            return null;

        HashMap<Location, Integer> locVisits = new HashMap<>();
        for(Trip t: trips)
        {
            Integer i = locVisits.get(t.source);
            if(i == null)
                locVisits.put(t.source, 1);
            else
                locVisits.put(t.source, i+1);
        }

        TreeMap<Integer, Location> counts = new TreeMap<Integer, Location>();
        for(Location loc : locVisits.keySet())
        {
            if(loc.getAddress() == null || loc.getAddress() == "null" || loc.getAddress().contains(","))
                continue;

            int newKey = locVisits.get(loc);
            Location old = counts.put(newKey, loc);
            while(old != null) {
                old = counts.put(--newKey, old);
            }
        }

        if(counts.size() < 4)
        {
            return null;
        }

        ArrayList<Location> options = new ArrayList<>(4);
        options.add(counts.get(counts.lastKey()));
        options.add(counts.get(counts.firstKey()));
        counts.remove(counts.firstKey());
        options.add(counts.get(counts.firstKey()));
        counts.remove(counts.firstKey());
        options.add(counts.get(counts.firstKey()));

        //Log.d(Util.TAG, "Exiting commonSource");

        return options;
//        HashMap<Location, Integer> counts = new HashMap<Location, Integer>();
//
//        //put locations in a hashmap to count the number of occurences for each
//        for (Trip t : trips)
//        {
//            Integer i = counts.get(t.source);
//
//            //increment the number of times that the source Location has been a source
//            if (i == null)
//            {
//                i = 1;
//            } else
//            {
//                i++;
//            }
//
//            //update Map
//            counts.put(t.source, i);
//        }
//
//        TreeMap<Integer, Location> ordered = new TreeMap<Integer, Location>();
//        //iterate through the keyset and extract the most common source
//        int t = 0;
//        for (Location l : counts.keySet())
//        {
//            int temp = counts.get(l);
//            if (temp > t)
//            {
//                t = temp;
//            }
//            ordered.put(temp, l);
//        }
//
//        ArrayList<Location> options = new ArrayList<Location>(4);
//        Location largest = ordered.get(t);
//        options.add(largest);
//
//        //pull some of the lowest locations from the ordered treemap
//        int total = 1;
//        t = 1; //scratch
//        while (total < 4)
//        {
//            Location notveryvisited = ordered.get(t++); //get a location with t visits
//            if (notveryvisited == null)
//            {
//                continue;
//            }
//            options.add(notveryvisited);
//            total++;
//        }
//
//        //Log.d(Util.TAG, "Exiting commonSource");
//
//        return options;
    }

    /**
     * Looks for the most common place for a Trip to end
     * the first element in the arraylist that is returned is the most
     * common destination and the final three are locations that are very
     * infrequently visited locations
     *
     * @param time0 days back to search
     * @param time1 days back to stop searching
     * @return list containing the most common destinations in order
     * @throws java.io.FileNotFoundException
     */
    public static ArrayList<Location> commonDestination(int time0, int time1)
    {
        //Log.d(Util.TAG, "Entering commonDestination");
        ArrayList<Trip> trips = findTravel(time0, time1);
        if(trips == null)
            return null;

        HashMap<Location, Integer> locVisits = new HashMap<>();
        for(Trip t: trips)
        {
            Integer i = locVisits.get(t.destination);
            if(i == null)
                locVisits.put(t.destination, 1);
            else
                locVisits.put(t.destination, i+1);
        }

        TreeMap<Integer, Location> counts = new TreeMap<Integer, Location>();
        for(Location loc : locVisits.keySet())
        {
            if(loc.getAddress() == null || loc.getAddress() == "null" || loc.getAddress().contains(","))
                continue;

            int newKey = locVisits.get(loc);
            Location old = counts.put(newKey, loc);
            while(old != null) {
                old = counts.put(--newKey, old);
            }
        }

        if(counts.size() < 4)
        {
            return null;
        }


        ArrayList<Location> options = new ArrayList<>(4);
        options.add(counts.get(counts.lastKey()));
        options.add(counts.get(counts.firstKey()));
        counts.remove(counts.firstKey());
        options.add(counts.get(counts.firstKey()));
        counts.remove(counts.firstKey());
        options.add(counts.get(counts.firstKey()));

        //Log.d(Util.TAG, "Exiting commonDestination");

        return options;

//        HashMap<Location, Integer> counts = new HashMap<Location, Integer>();
//        //put the locations in a hash map to count the number of occurrences for each
//        for (Trip t : trips)
//        {
//            Integer i = counts.get(t.destination);
//
//            //increment the number of times that the source Location has been a source
//            if (i == null)
//            {
//                i = 1;
//            } else
//            {
//                i++;
//            }
//
//            //update Map
//            counts.put(t.destination, i);
//        }
//
//        //Log.d(Util.TAG, "1 commonDestination");
//
//        TreeMap<Integer, Location> ordered = new TreeMap<Integer, Location>();
//        int c = 0;
//        //iterate through the keyset and order by count
//        for (Location loc : counts.keySet())
//        {
//            int temp = counts.get(loc);
//
//            if (temp > c)
//            {
//                c = temp;
//            }
//
//            ordered.put(temp, loc);
//        }
//
//        //Log.d(Util.TAG, "2 commonDestination");
//
//
//        ArrayList<Location> options = new ArrayList<Location>(4);
//        options.add(ordered.get(c)); //largest number of visits
//
//        //pull some of the lowest locations from the ordered treemap
//        int total = 1;
//        int t = 0; //scratch
//        while (total < 4)
//        {
//            Location notveryvisited = ordered.get(t++); //get a location with t visits
//            if (notveryvisited == null)
//            {
//                continue;
//            }
//            options.add(notveryvisited);
//            total++;
//            //Log.d(Util.TAG, total + " ");
//            if (total > ld.getPlaces().size())
//            {
//                System.out.println("Infinite Loop");
//                break;
//            }
//        }
//
//        //Log.d(Util.TAG, "Exiting commonDestination");
//
//        return options;
    }


    /**
     * reads in 14 days worth of locations and assigns the
     * list to the locations arraylist in the LocationDatabase
     */
    public static void prepareDatabase(ArrayList<Location> locs)
    {
        ld.setLocations(locs);
    }

    public static String dayOfWeek(long timestamp)
    {
        cal.clear();
        cal.setTimeInMillis(timestamp);
        cal.setTimeZone(TimeZone.getTimeZone("CMT"));
        int day = cal.get(Calendar.DAY_OF_WEEK);
        return dayOfWeek(day);
    }
    /**
     * returns the day of the week in
     * string form
     */
    public static String dayOfWeek(int dayofweek)
    {
        String day = "";
        switch (dayofweek)
        {
            case Calendar.MONDAY:
                day = "Monday";
                break;
            case Calendar.TUESDAY:
                day = "Tuesday";
                break;
            case Calendar.WEDNESDAY:
                day = "Wednesday";
                break;
            case Calendar.THURSDAY:
                day = "Thursday";
                break;
            case Calendar.FRIDAY:
                day = "Friday";
                break;
            case Calendar.SATURDAY:
                day = "Saturday";
                break;
            case Calendar.SUNDAY:
                day = "Sunday";
                break;
            case WEEK:
                day = "All Week";
                break;
            default:
                System.out.println("That is not a day of the week ??? ");
                break;
        }
        return day;
    }

    /**
     * returns the staycations specific to the day of the
     * week passed to it
     *
     * @param dayofweek
     * @return
     */
    private static ArrayList<Trip> staysByWeekDay(int dayofweek)
    {
        ArrayList<Trip> staycations = findStays(ld.getPlaces());
        ArrayList<Trip> stays = new ArrayList<Trip>();
        for (Trip t : staycations)
        {
            //if started on day of interest then add it to stays
            cal.clear();
            cal.setTimeInMillis(t.source.getTimestamp());
//            cal.setTimeZone(TimeZone.getTimeZone("CMT"));
            int sourceDay = cal.get(Calendar.DAY_OF_WEEK);
            if (sourceDay == dayofweek || WEEK == dayofweek)
            {
                stays.add(t);
            }
        }
        return stays;
    }

    /**
     * fills the list of options to choose from for Time At questions
     */
    private static void fillTimeAtOptions(ArrayList<String> options, ArrayList<Trip> staycations)
    {
        int count = 1; //4 total
        int scratch = 0;
        while (count < 4 && scratch < staycations.size())
        {
            Trip possible = staycations.get(scratch++);
            options.add("Starting at " + Util.convertStamp(possible.source.getTimestamp()) + " at " + possible.getSource().getAddress(true));
            count++;
        }
    }
}
