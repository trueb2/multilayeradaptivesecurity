package uiuc.research.appguardian.database;

/**
 * Created by qiuding on 10/16/2014.
 */
public class Processes implements Loggable
{

    int id;
    String processName;
    int importance;
    long createdAt;

    public Processes()
    {
    }

    public Processes(String processName, int importance)
    {
        this.processName = processName;
        this.importance = importance;
        this.createdAt = System.currentTimeMillis();
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getProcessName()
    {
        return processName;
    }

    public void setProcessName(String processName)
    {
        this.processName = processName;
    }

    public int getImportance()
    {
        return importance;
    }

    public void setImportance(int importance)
    {
        this.importance = importance;
    }

    public long getCreatedAt()
    {
        return createdAt;
    }

    public void setCreatedAt(long createdAt)
    {
        this.createdAt = createdAt;
    }

    public String logLine()
    {
        return processName + ", " + importance + ", " + createdAt + "\n";
    }
}
