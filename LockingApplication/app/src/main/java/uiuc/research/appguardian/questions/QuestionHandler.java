package uiuc.research.appguardian.questions;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import android.util.Log;

import java.util.ArrayList;

import uiuc.research.appguardian.database.Action;
import uiuc.research.appguardian.database.CallHistory;
import uiuc.research.appguardian.database.DatabaseHandler;
import uiuc.research.appguardian.database.HistoryAndBookmarks;
import uiuc.research.appguardian.database.Location;
import uiuc.research.appguardian.database.Processes;
import uiuc.research.appguardian.database.SMS;
import uiuc.research.appguardian.questions.activityquestions.ActivityAnalyze;
import uiuc.research.appguardian.questions.browserquestions.BrowserHistoryAnalyze;
import uiuc.research.appguardian.questions.callquestions.CallHistoryAnalyze;
import uiuc.research.appguardian.questions.callquestions.CallHistoryDataSet;
import uiuc.research.appguardian.questions.locationquestions.LocationAnalyze;
import uiuc.research.appguardian.questions.processesquestions.ProcAnalyze;
import uiuc.research.appguardian.database.LongOn;
import uiuc.research.appguardian.questions.screenquestions.ScreenAnalyze;
import uiuc.research.appguardian.questions.smsquestions.SMSAnalyze;
import uiuc.research.appguardian.questions.smsquestions.SMSDataSet;
import uiuc.research.appguardian.util.Util;

/**
 * Created by jwtrueb on 11/24/14.
 */
public class QuestionHandler
{


    private static boolean loaded = false;
    ArrayList<Question> locationQuestions, callQuestions,
            activityQuestions, smsQuestions, browserQuestions;
    private static Context mContext;

    public QuestionHandler(Context context)
    {
        locationQuestions = new ArrayList<Question>();
        callQuestions = new ArrayList<Question>();
        activityQuestions = new ArrayList<Question>();
        smsQuestions = new ArrayList<Question>();
        browserQuestions = new ArrayList<Question>();
        mContext = context;
    }

    public static void loadDatabase(Context context)
    {
        if (loaded)
        {
            return;
        }
        loadLocations(context);
        loadActions(context);
        loadSwitches(context);
        loadProcesses(context);
        loadCallHistories(context);
        loadBrowserHistories(context);
        loadSmsHistories(context);

        loaded = true;
    }

    private static void loadActions(Context context)
    {
        DatabaseHandler mDatabaseHandler = DatabaseHandler.getInstance(context);
        long start = System.currentTimeMillis();
        ArrayList<Action> actions = mDatabaseHandler.getActions(5);
        ActivityAnalyze.prepareDatabase(actions);
        long stop = System.currentTimeMillis();
        Log.d(Util.TAG, "Got " + actions.size() + " Actions : " + (stop - start) + " ms");
    }

    private static void loadLocations(Context context)
    {
        DatabaseHandler mDatabaseHandler = DatabaseHandler.getInstance(context);
        long start = System.currentTimeMillis();
        ArrayList<Location> locs = mDatabaseHandler.getLocations(5);
        if(locs.size() > 0)

            Log.d(Util.TAG, "Last loc in database " + locs.get(locs.size() - 1).toString());
        else
            Log.d(Util.TAG, "THERE ARE ZERO LOCATIONS IN THE DATABASE");
        LocationAnalyze.prepareDatabase(locs);
        long stop = System.currentTimeMillis();
        Log.d(Util.TAG, "Got " + locs.size() + " Locations : " + (stop - start) + " ms");
    }

    private static void loadSwitches(Context context)
    {
        DatabaseHandler mDatabaseHandler = DatabaseHandler.getInstance(context);
        //mDatabaseHandler.overwriteTable(DatabaseHandler.TABLE_SCREEN);
        long start = System.currentTimeMillis();
        ArrayList<LongOn> swtchs = mDatabaseHandler.getScreenChanges(2);
        ScreenAnalyze.prepareDatabase(swtchs);
        long stop = System.currentTimeMillis();
        Log.d(Util.TAG, "Got " + swtchs.size() + " screen changes : " + (stop - start) + " ms");
    }

    private static void loadCallHistories(Context context) {
        DatabaseHandler mDatabaseHandler = DatabaseHandler.getInstance(context);
        CallHistoryDataSet callHistoryDataSet;

        long start = System.currentTimeMillis();
        ArrayList<CallHistory> callHistories = mDatabaseHandler.getCallHistory(5);
//        CallHistoryAnalyze.prepareDataSet(callHistories);
        callHistoryDataSet = new CallHistoryDataSet(callHistories);
        Uri allCalls = Uri.parse("content://call_log/calls");
        Cursor cursor = context.getContentResolver().query(allCalls, null, null, null, null);
        cursor.moveToLast();
        String lastContact;
        if (cursor.getString(cursor.getColumnIndex(CallLog.Calls.CACHED_NAME)) != null) {
            lastContact = cursor.getString(cursor.getColumnIndex(CallLog.Calls.CACHED_NAME));
        }
        else {
            lastContact = cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER));
        }
        cursor.close();
        callHistoryDataSet.setLastContact(lastContact);

        CallHistoryAnalyze.prepareDataSet(callHistoryDataSet);
        long stop = System.currentTimeMillis();
        Log.d(Util.TAG, "Got " + callHistories.size() + " call histories : " + (stop - start) + " ms");
    }

    private static void loadSmsHistories(Context context) {
        DatabaseHandler mDatabaseHandler = DatabaseHandler.getInstance(context);
        SMSDataSet smsDataSet;

        long start = System.currentTimeMillis();
        ArrayList<SMS> smsHistories = mDatabaseHandler.getSMS(5);
        smsDataSet = new SMSDataSet(smsHistories);

        SMSAnalyze.prepareDataSet(smsDataSet);
        long stop = System.currentTimeMillis();
        Log.d(Util.TAG, "Got " + smsHistories.size() + " sms histories : " + (stop - start) + " ms");
    }

    private static void loadBrowserHistories(Context context) {
        DatabaseHandler mDatabaseHandler = DatabaseHandler.getInstance(context);
//        mDatabaseHandler.overwriteTable(DatabaseHandler.TABLE_HISNBKMK);
        long start = System.currentTimeMillis();
        ArrayList<HistoryAndBookmarks> habs = mDatabaseHandler.getAllHistoryAndBookmarksSince(5);
        long stop = System.currentTimeMillis();
        BrowserHistoryAnalyze.fillDatabase(habs);
        Log.d(Util.TAG, "Got " + habs.size() + " browser histories : " + (stop - start) + " ms");

    }

    private static void loadProcesses(Context context)
    {
        DatabaseHandler mDatabaseHandler = DatabaseHandler.getInstance(context);
//        mDatabaseHandler.overwriteTable(DatabaseHandler.TABLE_PROCESSES);
        long start = System.currentTimeMillis();
        ArrayList<Processes> procs = mDatabaseHandler.getAllProcessesSince(1);
        long stop = System.currentTimeMillis();
        Log.d(Util.TAG, "Got " + procs.size() + " processes histories : " + (stop - start) + " ms");
        ProcAnalyze.fillDatabase(procs);

    }
//    public boolean generateLocationQuestions()
//    {
//        int day = Util.yesterday();
//        int day1 = Util.dayBeforeYesterday();
//
//        long start = System.currentTimeMillis();
//        locationQuestions = LocationAnalyze.generateAllQuestions(5, 0, day, day1);
//        long stop = System.currentTimeMillis();
//        //Log.d("Timer", "Location Questions took: " + (stop - start));
//        return (locationQuestions != null) ? true : false;
//    }
//
//    public boolean generateActivityQuestions()
//    {
//        long start = System.currentTimeMillis();
//        activityQuestions = ActivityAnalyze.generateAllQuestions(10, 0);
//        long stop = System.currentTimeMillis();
//        //Log.d("Timer", "Activity Questions took: " + (stop - start));
//        return activityQuestions != null;
//    }

    //TODO Make it so that call questions compile
    public boolean generateCallQuestion()
    {
//        callQuestions = analyzeCallHistory.generateCallQuestion();
//        return callQuestions != null;
        return false;
    }

    public ArrayList<Question> getLocationQuestions()
    {
        return locationQuestions;
    }

    public ArrayList<Question> getCallQuestions()
    {
        return callQuestions;
    }

    public ArrayList<Question> getActivityQuestions()
    {
        return activityQuestions;
    }

    public ArrayList<Question> getSmsQuestions()
    {
        return smsQuestions;
    }

    public ArrayList<Question> getBrowserQuestions()
    {
        return browserQuestions;
    }
}
