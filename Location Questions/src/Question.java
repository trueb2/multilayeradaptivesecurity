import java.util.ArrayList;
import java.util.Collections;


public class Question {

	private String query;
	private String answer;
	private ArrayList<String> options;
	
	public Question(String query, String answer, ArrayList<String> options)
	{
		this.query = query;
		this.answer = answer;
		this.options = options;
	}
	
	public boolean checkAnswer(String response)
	{
		return response.equals(answer);
	}
	
	@Override
	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append("\n"+query.toString());
		
		//randomize the order of questions
		if(options == null)
			return "This question does not have enough options to be generated. NULL options";
		Collections.shuffle(options);
		
		if(options.size() < 4)
			return "This question does not have enough options to be generated. < 4 options";
		String a = options.get(0);
		String b = options.get(1);
		String c = options.get(2);
		String d = options.get(3);
		
		if(a == null || b == null || c == null || d == null)
			return "This question does not have enough options to be generated. NULL opt.";
		
		//Show as Choices
//		sb.append("\nA) " + options.get(0));
//		sb.append("\nB) " + options.get(1));
//		sb.append("\nC) " + options.get(2));
//		sb.append("\nD) " + options.get(3));
		sb.append("\n" + options.get(0));
		sb.append("\n" + options.get(1));		
		sb.append("\n" + options.get(2));		
		sb.append("\n" + options.get(3));
		
		return sb.toString();
	}
}
