import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.TreeMap;

/**
 * Created by jwtrueb on 10/23/14.
 */
public class Analyze {
//TODO make sure that question options match the form of the answer when passing them as arguments to the constructor
	
	public static final long DAY_IN_MILLIS = 86400000;
	public static final int WEEK = Calendar.MONDAY+Calendar.TUESDAY+Calendar.WEDNESDAY+
			Calendar.THURSDAY+Calendar.FRIDAY+Calendar.SATURDAY+Calendar.SUNDAY;
	
	private static LocationDatabase ld = new LocationDatabase();
	private static LocationDatabase ld1 = new LocationDatabase();

	public static void main(String[] args) throws FileNotFoundException {
		ld.readAll("../../git/multilayeradaptivesecurity/Log Snapshot/11.20.2014/location.csv");
		ld1.readAll("../../git/multilayeradaptivesecurity/Log Snapshot/Qiuhua-112314-112614/location.csv");
//		ld.assignAddresses();
//		System.out.println(ld.toString());
		System.out.println("//////////////////////////////////////////");
		
//		Graphing.scatterLogOccurences(ld.sortByVisits(ld.places));
//		Graphing.scatterLogOccurences(ld1.sortByVisits(ld1.places));
		Graphing.scatterLogOccurencesComparison(ld.sortByVisits(ld.places), ld1.sortByVisits(ld1.places));
//		Graphing.scatterTripSourceWeekTimestamp(findStays(ld.places), "Stays by Day of the Week");
//		Graphing.scatterTripSourceWeekTimestamp(findTravel(21,0,ld), "Travel by Day of the Week");
//		Graphing.scatterTripSourceWeekTimestamp(findTravel(21,0,ld1), "Travel By Day of the Week");
//		Graphing.scatterTripSourceWeekTimestamp(findTravel(21, 0, ld), findTravel(21,0,ld1), "Travel By Day of Week");
		Graphing.barTripWeekComparison(findTravel(21, 0, ld), findTravel(21,0,ld1), "Travel By Day of Week");
//		Graphing.scatterTripSourceDayTimestamp(findStays(ld.places), "Stays by Hour");
		Graphing.barTripDayComparison(findTravel(21,0,ld), findTravel(21,0,ld1), "Travel by Hour");
//		
//		Question mostVisits = mostVisited(10); //days back
//		System.out.println("\nPrinting Most Visited Question" + mostVisits);
//		
//		Question spentTime = timeAt(WEEK);
//		System.out.println("\nPrinting Time Spent At Question" + spentTime);
//		
//		Question wherewereyou = whereWereYou();
//		System.out.println("\nPrinting Where Were You At Question" + wherewereyou);
//		
//
//		Question commonDest = mostCommon(false, 10, 0);
//		System.out.println("\nPrinting Common Destination Question" + commonDest);
//		
//		Question commonSource = mostCommon(true, 10, 0);
//		System.out.println("\nPrinting Common Source Question" + commonSource);		
		
//		Question question = timeAt(Calendar.TUESDAY);
//		System.out.println(question);
	}

	
	
	/**
	 * Generates a Question that asks what the most frequently visited
	 * location was over a certain number of days back
	 * the question will have 4 options in random order
	 * the options will be:
	 * First most visited location
	 * Fifth most visited location
	 * Sixth most visited location
	 * Seventh most visited location
	 * 
	 * @param daysBack
	 * @return Question
	 */
	protected static Question mostVisited(int daysBack)
	{
		String query = "Where did you (or maybe just your phone) visit the most over the past " + daysBack + " days?";
		String answer = "nowhere";
		ArrayList<String> options = new ArrayList<String>();
		long now = System.currentTimeMillis();
		long start = now - DAY_IN_MILLIS * daysBack;
		
		//if the location was visited within time window we will take a look
		ArrayList<LocationData> locs = new ArrayList<LocationData>();
		for(LocationData l : ld.places)
			if(l.timestamp >= start)
				locs.add(l);
		TreeMap<Integer, LocationData> sorted = ld.sortByVisits(locs);
		
		//keys in order by number of visits
		ArrayList<Integer> keys = new ArrayList<Integer>(sorted.keySet());
		int size = keys.size();
		for(int i = size-1; i >= 0; i--)
		{
			if (i == size-1)
			{   //most visited
				String t = sorted.get(keys.get(i)).getAddress();
				answer = t;
				options.add(t);
			}
			else if(i == size-5)
			{   //fifth most visited
				options.add(sorted.get(keys.get(i)).getAddress());
			}
			else if(i == size-6)
			{   //sixth most visited
				options.add(sorted.get(keys.get(i)).getAddress());
			}
			else if(i == size-7)
			{   //seventh most visited
				options.add(sorted.get(keys.get(i)).getAddress());
				break;  //no more options necessary
			}
		}
		
		System.out.println(answer);
		Question question = new Question(query, answer, options);
		return question;
	}
	
	/**
	 * generates a how long did you spend at ... Question
	 * 1. Don't want this to be the most visited location preferably
	 * 2. Don't want this to be at a location that might have been for 
	 * a short period of time
	 * 3. 20 minutes to 1 and 20 minutes probably
	 * 
	 * This will probably be unique with most days, once we have the 
	 * ability to find trends so that something like work or home does not 
	 * show up in this question
	 * @param int day of the week to check Calendar Constant
	 * @return Question
	 */
	protected static Question timeAt(int dayofweek)
	{
		if(ld.places == null)
			return null;
		
		Week w = new Week(ld.places);
		
		String day = null;
		//get just this days locations
		ArrayList<LocationData> dayLocs = null;
		switch(dayofweek){
		case Calendar.MONDAY: dayLocs = w.monday; day = "Monday";
			break;
		case Calendar.TUESDAY: dayLocs = w.tuesday; day = "Tuesday";
			break;
		case Calendar.WEDNESDAY: dayLocs = w.wednesday; day = "Wednesday";
			break;
		case Calendar.THURSDAY: dayLocs = w.thursday; day = "Thursday";
			break;
		case Calendar.FRIDAY: dayLocs = w.friday; day = "Friday";
			break;
		case Calendar.SATURDAY: dayLocs = w.saturday; day = "Saturday";
			break;
		case Calendar.SUNDAY: dayLocs = w.sunday; day = "Sunday";
			break;
		case WEEK: dayLocs = w.allWeek(); day = "All Week";
			break;
		default: System.out.println("That is not a day of the week ??? ");
			break;
		}
		
		String answer = "eh";
		String query = "Not yet";
		ArrayList<String> options = new ArrayList<String>();

		ArrayList<Trip> staycations = findStays(dayLocs);
		//slightlyDifferent(dayLocs);
		
		//print durations
//		printDurations(staycations);
		
		//so now we have all of the trips compare the durations
		ArrayList<Trip> goodOpts = new ArrayList<Trip>();
		for(Trip stay : staycations)
		{   // 1 hour 15 min < duration < 5 hr
			if(stay.duration > 75*60*1000 && stay.duration < 5*60*60*1000 && stay.source.address != null)
			{	//good choice for asking a question about
				goodOpts.add(stay);
			}
		}
		
		//with the good choices available randomly choose 4 and assign the Question params
		Collections.shuffle(goodOpts);
		Trip ans = goodOpts.get(0);
		answer = "Duration " + LocationData.convertMillis(ans.duration) + " " + ans.getSource().getAddress();
		options.add(answer);
		
		int count = 1; //4 total 
		//don't want to put options up with equal time spent at each location
		for(int i = 1; i < goodOpts.size(); i++)
		{ 
			Trip possible = goodOpts.get(i);
			//if the difference in duration is greater than 60 minutes
//			if(Math.abs(ans.duration - possible.duration) > 30*60*1000)
//			{   //this is a good choice duration wise
				options.add("Duration: " + LocationData.convertMillis(possible.duration) + " " +possible.getSource().getAddress());
				count++;
//			}

			if(count == 4)
				break; //4 options generated
		}
		
		query = "Where did you spend " + LocationData.convertMillis(ans.duration) + " on " + day + "?";
		Question question = new Question(query, answer, options);
		return question;
	}
	
	/**
	 * Generates a question as follows:
	 * Where were you at ... on ...?
	 * this location can be pulled from any stay within the past few days
	 * that is longer than about an hour
	 * @return Question
	 */
	protected static Question whereWereYou(){
		int daysBack = 10; //dont want to ask about more than two days back probably
		long start = System.currentTimeMillis() - daysBack * DAY_IN_MILLIS;
		ArrayList<Trip> trips = findStays(ld.places);
		ArrayList<Trip> options = new ArrayList<Trip>();
		
		//look for trips that are over an hour long and occured in the last 3 days
		for(int i = 0; i < trips.size(); i++){
			Trip t = trips.get(i);
			if(t.duration > 60*60*1000 && t.source.timestamp > start){
				options.add(t);
//				if(options.size() == 4)
//					break; //get 4 options	
			}
		}
		
		//not enough options to generate a question
		if(options.size() < 4)
			return new Question("Where were you at ... on ...?", "nowhere apparently", null);
		
		Collections.shuffle(options);
		Collections.shuffle(options);
		
		Trip l = options.get(0);
		long time = (l.source.timestamp + l.destination.timestamp)/2;
		String query = "Where were you at " + LocationDatabase.convertStamp(time) + "?";
		String answer = l.source.address;
		ArrayList<String> multichoice = new ArrayList<String>();
		for(Trip z : options){
//			if(z == l)
//				continue;
			multichoice.add(z.source.address + " \n" +  z.toString());
		}
		
		Question question = new Question(query, answer, multichoice);
		return question;
	}
	
	/**
	 * Generates a question that asks how long a user spent 
	 * at a location.
	 * Things to Consider when choosing the location:
	 * 1. Must be a place that is visited fairly often
	 * 2. must be a place that was visited for an unusual amount of time
	 * 		ie 3 hours instead of the usual one or two
	 * 
	 * this one will have to reference previous trips possibly weeks back in order
	 * to determine the trend
	 * @param trips
	 * @return Question
	 */
	public static Question howLongWereYouAt(ArrayList<Trip> trips){
		//Communicate with tripdatabase
		
		//loop through common locations
		
		//compare usual durations with durations in the past week
		
		//identify an unusual duration
		
		//identify 3 more trips with usual durations
		
		//create Question
		return null;
	}

	public static void printDurations(ArrayList<Trip> trips){
		System.out.println("\nPrinting Durations");
		for(Trip t : trips){
			if(t.duration > 30*60*1000)
				System.out.println(t.toString() + " " + LocationData.convertMillis(t.duration));
		}
	}
	
	/**
	 * Going off of: 1. Several Consecutive Logs of the same location 2. A few
	 * logs of different location 3. Several Consecutive Logs of the same
	 * location
	 * forms a Trip
	 *
	 * It is likely that the user was traveling
	 *
	 * TODO Compare this to the Activity Recognition (Walk, Bike, Run)
	 *
	 * @param time0
	 *            Days back that you are willing to start looking
	 * @param time1
	 *            Days back that you want to stop looking 0 will be defined as
	 *            current
	 *
	 * @return ArrayList<LocationData> containing the starting location, the
	 *         recording traveling locations, and the final location
	 * @throws FileNotFoundException
	 */
	protected static ArrayList<Trip> findTravel(int time0, int time1,LocationDatabase ld) throws FileNotFoundException {
		ArrayList<Trip> trips = new ArrayList<Trip>();
		ArrayList<LocationData> time = ld.sortByTime(ld.places);

		// Start considering trends at time0
		// Note timestamp is Epoch (ms) timestamp 1 day = 86400000 ms
		long now = System.currentTimeMillis();
		long start = now - DAY_IN_MILLIS * time0;
		long stop = now - DAY_IN_MILLIS * time1;

		Trip first = new Trip();
		trips.add(first);
		
		//iterate through every location to compare if the location has changed
		LocationData previous = null, current = null;
		for (int i = 0; i < time.size(); i++)
		{
			int t = trips.size();
			Trip trip = trips.get(t - 1);
			LocationData loc = time.get(i);

			previous = current;
			current = loc;
			
			if (loc.getTimestamp() < start) // cant be farther back than the start
				continue;
			if (loc.getTimestamp() > stop) // nothing past here is worth looking at
				break;
		
			if(previous == null)
				continue;
			
			if(!previous.equals(current) && !trip.isStarted())
			{ //just started to change location
				trip.setSource(previous);
				trip.addToPath(current);
				trip.setStarted(true);
			}
			else if (!previous.equals(current) && trip.isStarted())
			{   //location is still changing
				trip.addToPath(current);
			}
			else if (previous.equals(current) && trip.isStarted())
			{   //location has stopped changing
				trip.setDestination(previous);
				
				//set up for next Trip
				trips.add(new Trip());
			}
		}
		
		// the last trip may not have ended so remove it from the list
		if(trips.get(trips.size() - 1).getDestination() == null)
			trips.remove(trips.size() - 1);
		
		return trips;
	}
	
	/**
	 * returns a list of Trips such that the source of the Trip is
	 * the same as the destination of the trip and no locations in between the 
	 * source and the destination are different from the source.
	 * this is referred to as a staycation in the code and in essence represents
	 * the lack of movement
	 * @return ArrayList<Trip> staycations
	 */
	public static ArrayList<Trip> findStays(ArrayList<LocationData> dayLocs){
		ArrayList<Trip> staycations = new ArrayList<Trip>();
		Trip first = new Trip();
		staycations.add(first); 
		
		//make sure that the days are order correctly
		dayLocs = ld.sortByTime(dayLocs); 
		
		
		//find 4 acceptable staycations
		//randomly pick one
		//set up a Question and return it
		
		long t0, t1;
		LocationData previous = null, current = null;
		for(LocationData l : dayLocs)
		{
			int i = staycations.size() - 1;
			Trip staycation = staycations.get(i);
			
			previous = current;
			current = l;
			
			if(previous == null || current == null)
				continue;
				
			if(previous.equals(current) && !staycation.isStarted())
			{   //no change in location
				t0 = previous.timestamp;
				//start staycation
				staycation.setSource(previous);
				staycation.setStarted(true);
				staycation.setStaycation();
			}
			else if(!previous.equals(current) && !staycation.isStarted())
			{   //still moving so dont worry about it
//				System.out.println(previous.toString() + " != " + current.toString());

			}
			else if(previous.equals(current) && staycation.isStarted())
			{   //on a staycation so dont worry about it
				staycation.addToPath(previous);
//				System.out.println(previous.toString() + " = " + current.toString());
			}
			else if(!previous.equals(current) && staycation.isStarted())
			{   //started moving again and was stationary before
				staycation.setDestination(previous);
				//set up for the next staycation
				staycations.add(new Trip());
			}
		}

		
		// remove the final staycation if it did not reach its destination
		if(staycations.get(staycations.size() - 1).getDestination() == null)
			staycations.remove(staycations.size() - 1);
		return staycations;
	}
	
	public static void slightlyDifferent(ArrayList<LocationData> dayLocs){
		dayLocs = ld.sortByTime(dayLocs);
		ArrayList<ArrayList<LocationData>> sequences = new ArrayList<ArrayList<LocationData>>();
		ArrayList<LocationData> seq = new ArrayList<LocationData>();
		int j;
		for(int i = 1; i < dayLocs.size(); i++){
			j = i - 1;
			LocationData a = dayLocs.get(i);
			LocationData b = dayLocs.get(j);
			if(a.equals(b))
				seq.add(a);
			if(!a.equals(b) && seq.size() == 0)
				continue;
			if(!a.equals(b) && seq.size() != 0){
				ArrayList<LocationData> nextSequence = new ArrayList<LocationData>();
				sequences.add(seq);
				seq  = nextSequence;
			}
		}
		
//		for(ArrayList<LocationData> list : sequences){
//			LocationData c = list.get(0);
//			LocationData d = list.get(list.size()-1);
//			long duration = d.timestamp - c.timestamp;
//			System.out.println(c + " to " + d + " in " + LocationData.convertMillis(duration));
//		}
	}
	
	/**
	 * Looks for the most common source of Trips
	 * @param time0
	 * @param time1
	 * @return the most common source of a Trip
	 * @throws FileNotFoundException
	 */
	protected static ArrayList<LocationData> commonSource(int time0, int time1) throws FileNotFoundException {
		ArrayList<Trip> trips = findTravel(time0, time1, ld);
		
		HashMap<LocationData, Integer> counts = new HashMap<LocationData, Integer>();
		
		//put locations in a hashmap to count the number of occurences for each
		for(Trip t : trips){
			Integer i = counts.get(t.source);
			
			//increment the number of times that the source LocationData has been a source
			if(i == null)
				i = 1;
			else
				i++;
			
			//update Map
			counts.put(t.source, i);
		}
		
		TreeMap<Integer, LocationData> ordered = new TreeMap<Integer, LocationData>();
		//iterate through the keyset and extract the most common source
		int t = 0;
		for(LocationData l : counts.keySet()){
			int temp = counts.get(l);
			if(temp > t)
				t = temp;
			ordered.put(temp, l);
		}
		
		ArrayList<LocationData> options = new ArrayList<LocationData>(4);
		LocationData largest = ordered.get(t);		
		options.add(largest);
		
		//pull some of the lowest locations from the ordered treemap
		int total = 1;
		t = 1; //scratch
		while(total < 4){
			LocationData notveryvisited = ordered.get(t++); //get a location with t visits
			if(notveryvisited == null)
				continue;
			options.add(notveryvisited);
			total++;
		}
		
		return options;
	}
	
	/**
	 * Looks for the most common place for a Trip to end
	 * the first element in the arraylist that is returned is the most 
	 * common destination and the final three are locations that are very
	 * infrequently visited locations
	 * @param time0 days back to search
	 * @param time1 days back to stop searching
	 * @return list containing the most common destinations in order
	 * @throws FileNotFoundException
	 */
	protected static ArrayList<LocationData> commonDestination(int time0, int time1) throws FileNotFoundException {
		ArrayList<Trip> trips = findTravel(time0, time1, ld);
		
		HashMap<LocationData, Integer> counts = new HashMap<LocationData, Integer>();
		//put the locations in a hash map to count the number of occurrences for each
		for(Trip t : trips){
			Integer i = counts.get(t.destination);
			
			//increment the number of times that the source LocationData has been a source
			if(i == null)
				i = 1;
			else
				i++;
			
			//update Map
			counts.put(t.destination, i);
		}
		
		TreeMap<Integer, LocationData> ordered = new TreeMap<Integer, LocationData>();		
		int c = 0;
		//iterate through the keyset and order by count
		for(LocationData loc : counts.keySet()){
			int temp = counts.get(loc);
			
			if(temp > c)
				c = temp;
			
			ordered.put(temp, loc);
		}
		
		ArrayList<LocationData> options = new ArrayList<LocationData>(4);
		options.add(ordered.get(c)); //largest number of visits
		
		//pull some of the lowest locations from the ordered treemap
		int total = 1;
		int t = 1; //scratch
		while(total < 4){
			LocationData notveryvisited = ordered.get(t++); //get a location with t visits
			if(notveryvisited == null)
				continue;
			options.add(notveryvisited);
			total++;
			if(total > ld.places.size())
				System.out.println("Infinite Loop");
		}
		
		return options;
	}
	
	/**
	 * Generates a Question that asks what the most common destination or source is
	 * the boolean parameter determines whether the question is about the most common source
	 * if set to false then the question will be about the most common destination
	 * time0 is the number of days back to consider in the question
	 * and time1 is the number of days back from the current day to stop considering
	 * time1 = 0 is current
	 * @throws FileNotFoundException 
	 */
	public static Question mostCommon(boolean source, int time0, int time1) throws FileNotFoundException{
		ArrayList<LocationData> common;
		String query = "What is your most common ";
		if(!source){
			common = commonDestination(time0, time1);
			query += "destination ";
		}
		else {
			common = commonSource(time0, time1);
			query += "source of travel ";
		}
		
		LocationData answerLoc = common.get(0);
		if(time1 != 0)
			query += "between " + time0 + " and " + time1 + " days ago?";
		else
			query += "over the past " + time0 + " days?";
		
		String answer = answerLoc.toString();
		ArrayList<String> options = new ArrayList<String>();
		
		for(LocationData loc : common){
			String opt = loc.toString();
			options.add(opt);
		}
		
		Collections.shuffle(options);
		Collections.shuffle(options);
		
		Question question = new Question(query, answer, options);
		return question;
	}
	

}
