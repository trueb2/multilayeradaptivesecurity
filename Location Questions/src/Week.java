import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;


public class Week {

	protected ArrayList<LocationData> monday, tuesday, wednesday,
							thursday, friday, saturday, sunday;
	protected Calendar cal;
	
	public Week(ArrayList<LocationData> locs){
		monday = new ArrayList<LocationData>();
		tuesday = new ArrayList<LocationData>();
		wednesday = new ArrayList<LocationData>();
		thursday = new ArrayList<LocationData>();
		friday = new ArrayList<LocationData>();
		saturday = new ArrayList<LocationData>();
		sunday = new ArrayList<LocationData>();
		
		cal = Calendar.getInstance();
		
		for(LocationData loc : locs){
			//find out what day it is in and put it in the associated list
			cal.clear();
			cal.setTimeInMillis(loc.timestamp);
			cal.setTimeZone(TimeZone.getTimeZone("CMT"));
			int day = cal.get(Calendar.DAY_OF_WEEK);
			switch(day){
			case Calendar.MONDAY: monday.add(loc);
				break;
			case Calendar.TUESDAY: tuesday.add(loc);
				break;
			case Calendar.WEDNESDAY: wednesday.add(loc);
				break;
			case Calendar.THURSDAY: thursday.add(loc);
				break;
			case Calendar.FRIDAY: friday.add(loc);
				break;
			case Calendar.SATURDAY: saturday.add(loc);
				break;
			case Calendar.SUNDAY: sunday.add(loc);
				break;
			default: System.out.println("That is not a day of the week ??? ");
				break;
			}
		}
	}

	public ArrayList<LocationData> allWeek() {
		ArrayList<LocationData> week = new ArrayList<LocationData>();
		week.addAll(monday);
		week.addAll(tuesday);
		week.addAll(wednesday);
		week.addAll(thursday);
		week.addAll(friday);
		week.addAll(saturday);
		week.addAll(sunday);
		return week;
	}
	
	
}
