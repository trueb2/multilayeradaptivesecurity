import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;
import java.util.TreeMap;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;


public class Graphing {

	private static final Random R = new Random();

	public static void makeAScatter(){
		JFreeChart chart = ChartFactory.createScatterPlot(
	            "Scatter Plot", // chart title
	            "X", // x axis label
	            "Y", // y axis label
	            createRandomDataset(), // data  ***-----PROBLEM------***
	            PlotOrientation.VERTICAL,
	            true, // include legend
	            true, // tooltips
	            false // urls
	            );

	        // create and display a frame...
	        ChartFrame frame = new ChartFrame("First", chart);
	        frame.pack();
	        frame.setVisible(true);
	}

	private static XYDataset createRandomDataset() {
	    XYSeriesCollection result = new XYSeriesCollection();
	    XYSeries series = new XYSeries("Random");
	    for (int i = 0; i <= 100; i++) {
	        double x = R.nextDouble();
	        double y = R.nextDouble();
	        series.add(x, y);
	    }
	    result.addSeries(series);
	    return result;
	}
	
	public static void scatterLogOccurences(TreeMap<Integer, LocationData> tree){
		XYSeriesCollection result = new XYSeriesCollection();
		XYSeries series = new XYSeries("Location");
		int c = 1;
		for(Integer i : tree.keySet()){
				series.add(c++, i);
		}
		result.addSeries(series);
		
		JFreeChart chart = ChartFactory.createScatterPlot("Locations By Number of Occurences in Log",
				"Unique Locations", "Number of Occurences", result, PlotOrientation.VERTICAL, true,
				true, false);
		
		ChartFrame frame = new ChartFrame("Log Occurences", chart);
		frame.pack();
		frame.setVisible(true);
	}
	
	/**
	 * Creates a scatter plot with the x axis being the day of the week
	 * and the y axis being the number of visits
	 * @param trips
	 * @param title
	 */
	public static void scatterTripSourceWeekTimestamp(ArrayList<Trip> trips, String title){
		XYSeriesCollection result = new XYSeriesCollection();
		XYSeries series = new XYSeries("Sunday is " + Calendar.SUNDAY + " and Saturday is " + Calendar.SATURDAY);
		
		//make an arraylist with 0s 
		ArrayList<Integer> week = new ArrayList<Integer>(7);
		for(int i = 0; i < 7; i++)
			week.add(0);
		
		//increment counts of trips on each day
		for(Trip t : trips){
			int day = LocationDatabase.dayOfWeekByStamp(t.source.timestamp);
			week.set(day-1, week.get(day-1)+1);
		}
		
		for(int i = 0; i < 7; i++)
			series.add(i+1, week.get(i));
		
		result.addSeries(series);
		JFreeChart chart = ChartFactory.createScatterPlot(title, "Day of Week", "Number of Trips",
				result, PlotOrientation.VERTICAL, true, true, false);
		ChartFrame frame = new ChartFrame("Trips By Day of Week", chart);
		frame.pack();
		frame.setVisible(true);
		
		try {
			ChartUtilities.saveChartAsPNG(new File(title+LocationDatabase.dayOfWeekByStamp(System.currentTimeMillis())), chart, 600, 480);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Creates a scatter plot with the x axis being the hour of the day
	 * and the y axis being the number of trips
	 * @param trips
	 * @param title
	 */
	public static void scatterTripSourceDayTimestamp(ArrayList<Trip> trips, String title){
		XYSeriesCollection result = new XYSeriesCollection();
		XYSeries series = new XYSeries("Hour of Day " + Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
		
		//make an arraylist with 0s 
		ArrayList<Integer> day = new ArrayList<Integer>(24);
		for(int i = 0; i < 24; i++)
			day.add(0);
		
		//increment counts of trips on each day
		for(Trip t : trips){
			int hour = LocationDatabase.hourOfDayByStamp(t.source.timestamp);
			day.set(hour, day.get(hour)+1);
		}
		
		for(int i = 0; i < 24; i++)
			series.add(i+1, day.get(i));
		
		result.addSeries(series);
		JFreeChart chart = ChartFactory.createScatterPlot(title, "Hour of Day", "Number of Trips",
				result, PlotOrientation.VERTICAL, true, false, false);
		ChartFrame frame = new ChartFrame("Trips By Hour of Day", chart);
		frame.pack();
		frame.setVisible(true);
		
		try {
			ChartUtilities.saveChartAsPNG(new File(title+LocationDatabase.dayOfWeekByStamp(System.currentTimeMillis())), chart, 600, 480);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Creates a scatter plot with the x axis being a unique location and
	 * the y axis being the number of visits
	 */
	public static void scatterLogOccurencesComparison(TreeMap<Integer, LocationData> t0, TreeMap<Integer, LocationData> t1){
		XYSeriesCollection result = new XYSeriesCollection();
		XYSeries series = new XYSeries("Jacob");
		int c = 1;
		for(Integer i : t0.keySet()){
				series.add(c++, i);
		}
		result.addSeries(series);
		
		XYSeries series1 = new XYSeries("Qiuhua");
		c = 1;
		for(Integer i : t1.keySet()){
			series1.add(c++,i);
		}
		result.addSeries(series1);
		
		JFreeChart chart = ChartFactory.createScatterPlot("Locations By Number of Occurences in Log",
				"Unique Locations", "Number of Occurences", result, PlotOrientation.VERTICAL, true,
				true, false);
		
		chart.setBackgroundPaint(Color.WHITE);
		XYPlot plot = (XYPlot) chart.getPlot();
		XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) plot.getRenderer();
		renderer.setSeriesPaint(0, Color.BLUE);
		renderer.setSeriesPaint(1, new Color(244,127,36));
		
		try {
			ChartUtilities.saveChartAsPNG(new File("Log Occurences" +LocationDatabase.dayOfWeekByStamp(System.currentTimeMillis())), chart, 600, 480);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ChartFrame frame = new ChartFrame("Log Occurences", chart);
		frame.pack();
		frame.setVisible(true);
	}
	/**
	 * Creates an area plot with the x axis the day of the week and the y
	 * axis the number of trips for that day
	 */
	public static void barTripWeekComparison(ArrayList<Trip> t0, ArrayList<Trip> t1, String title){

		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		
		//make an arraylist with 0s 
		ArrayList<Integer> week = new ArrayList<Integer>(7);
		for(int i = 0; i < 7; i++)
			week.add(0);

		//increment counts of trips on each day
		for(Trip t : t0){
			int day = LocationDatabase.dayOfWeekByStamp(t.source.timestamp);
			week.set(day-1, week.get(day-1)+1);
		}
		
		for(int i = 0; i < 7; i++)
			dataset.addValue((Number)week.get(i), "Jacob", i+1);
		
		week = new ArrayList<Integer>(7);
		for(int i = 0; i < 7; i++){
			week.add(0);
		}
		
		for(Trip t : t1){
			int day = LocationDatabase.dayOfWeekByStamp(t.source.timestamp);
			week.set(day-1, week.get(day-1)+1);
		}
		
		for(int i = 0; i < 7; i++)
			dataset.addValue((Number)week.get(i),"Qiuhua", i+1);
		
		JFreeChart chart = ChartFactory.createBarChart(title, "Day of Week", "Number of Trips", dataset);
		chart.setBackgroundPaint(Color.WHITE);
		CategoryPlot plot = (CategoryPlot) chart.getPlot();
		BarRenderer renderer = (BarRenderer) plot.getRenderer();
		renderer.setSeriesPaint(0, Color.BLUE);
		renderer.setSeriesPaint(1, new Color(244,127,36));
		renderer.setDrawBarOutline(false);
		renderer.setShadowVisible(false);
//		renderer.setMaximumBarWidth(0.5);
		renderer.setGradientPaintTransformer(null);
		renderer.setBarPainter(new StandardBarPainter());
		ChartFrame frame = new ChartFrame("Trips By Day of Week", chart);
		frame.pack();
		frame.setVisible(true);
		
		try {
			ChartUtilities.saveChartAsJPEG(new File(title+LocationDatabase.dayOfWeekByStamp(System.currentTimeMillis())), chart, 800, 800);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/**
	 * Creates a scatter plot with the x axis being the hour of the day
	 * and the y axis being the number of trips
	 * @param trips
	 * @param title
	 */
	public static void barTripDayComparison(ArrayList<Trip> t0, ArrayList<Trip> t1, String title){
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		
		//make an arraylist with 0s 
		ArrayList<Integer> day = new ArrayList<Integer>(24);
		for(int i = 0; i < 24; i++)
			day.add(0);
		
		//increment counts of trips on each day
		for(Trip t : t0){
			int hour = LocationDatabase.hourOfDayByStamp(t.source.timestamp);
			day.set(hour, day.get(hour)+1);
		}
		
		for(int i = 0; i < 24; i++)
			dataset.addValue((Number)day.get(i), "Jacob", i+1);
				
		XYSeries series1 = new XYSeries("Qiuhua");
		//make an arraylist with 0s 
		day = new ArrayList<Integer>(24);
		for(int i = 0; i < 24; i++)
			day.add(0);

		//increment counts of trips on each day
		for(Trip t : t1){
			int hour = LocationDatabase.hourOfDayByStamp(t.source.timestamp);
			day.set(hour, day.get(hour)+1);
		}

		for(int i = 0; i < 24; i++)
			dataset.addValue((Number)day.get(i), "Qiuhua", i+1);
		
		JFreeChart chart = ChartFactory.createBarChart(title, "Hour of Day", "Number of Trips", dataset);
		chart.setBackgroundPaint(Color.WHITE);
		CategoryPlot plot = (CategoryPlot) chart.getPlot();
		BarRenderer renderer = (BarRenderer) plot.getRenderer();
		renderer.setSeriesPaint(0, Color.BLUE);
		renderer.setSeriesPaint(1, new Color(244,127,36));
		renderer.setDrawBarOutline(false);
		renderer.setShadowVisible(false);
//		renderer.setMaximumBarWidth(0.5);
		renderer.setGradientPaintTransformer(null);
		renderer.setBarPainter(new StandardBarPainter());
		ChartFrame frame = new ChartFrame("Trips By Hour of Day", chart);
		frame.pack();
		frame.setVisible(true);
		
		try {
			ChartUtilities.saveChartAsPNG(new File(title+LocationDatabase.dayOfWeekByStamp(System.currentTimeMillis())), chart, 800, 800);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
