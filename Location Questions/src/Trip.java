import java.util.ArrayList;

/**
 * Created by jwtrueb on 11/4/14.
 *
 * A Trip has:
 *
 * 1. Source
 * 2. Path
 * 3. Destination
 *
 * These will be in the form of locations:
 * Starting Locations, Locations logged on the
 * way to the destination, and the destination
 * Location.
 */
public class Trip {

    protected LocationData source;
    protected ArrayList<LocationData> path;
    protected LocationData destination;
    protected long duration;
    protected boolean started = false;
    protected boolean staycation = false;

    /**
     * new trip hasnt started yet
     */
    public Trip(){
    	source = null;
    	path = new ArrayList<LocationData>();
    	destination = null;
    	duration = Long.MAX_VALUE;
    }
    
    /**
     * called when starting a trip
     * @param source
     */
    public Trip(LocationData source){
    	this.source = source;
    	path = new ArrayList<LocationData>();
    	destination = null;
    	started = true;
    }
    
    /**
     * probably wont ever be used
     * @param source
     * @param pathTaken
     * @param destination
     */
    public Trip(LocationData source, ArrayList<LocationData> pathTaken, LocationData destination) {
        this.source = source;
        this.path = pathTaken;
        this.destination = destination;
        started = true; //did the whole trip already
        started = false;
    }

    /**
     * adds a location to the path that describes your journey
     * @param loc
     */
    public void addToPath(LocationData loc){
    	path.add(loc);
    }
    
    @Override
    /**
     * String representation of the trip
     * @return String Trip
     */
    public String toString(){
    	if(destination == null || source == null)
    		return "This was not a real trip";
    	String trip = "Started at : " + source.toString() + " at " + LocationDatabase.convertStamp(source.getTimestamp())+ "\n";
    	if(!staycation){
    		int i = 1;
    		for(LocationData loc : path){
    			trip += i++ + ") " + loc.toString() + " \n";
    		}
    	}
    	trip += "Ended at : " + destination.toString() + " at " + LocationDatabase.convertStamp(destination.getTimestamp()) + "\n";
    	return trip;
    }
    
    public LocationData getSource() {
        return source;
    }

    public ArrayList<LocationData> getPath() {
        return path;
    }

    public LocationData getDestination() {
        return destination;
    }
    
    public boolean isStarted(){
    	return started;
    }

    public void setSource(LocationData source) {
        this.source = source;
    }

    public void setPath(ArrayList<LocationData> path) {
        this.path = path;
    }

    public void setDestination(LocationData destination) {
        this.destination = destination;
        
        //determine the duration
        duration = destination.timestamp - source.timestamp;
    }
    
    public void setStarted(boolean started){
    	this.started = started;
    }

	public void setStaycation() {
		staycation = true;
		
	}
}
