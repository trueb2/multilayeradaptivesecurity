import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by jwtrueb on 10/23/14.
 */
public class LocationData {

    int id;
    double lat, lng;
    long timestamp;
    String date;
    String address;
    double accuracy = 3; //number of decimal places of accuracy

    public LocationData(double lat, double lng, long timestamp, String address){
    	
        this.lat = lat;
        this.lng = lng;
        this.timestamp = timestamp;
        this.date = LocationDatabase.convertStamp(timestamp);
        if(address.length() < 5){ //null
        	if(LocationDatabase.addresses != null && LocationDatabase.addresses.containsKey(this)){
        		address = LocationDatabase.addresses.get(this);
        	} else {
        		this.address = getRoadName(lat+"",lng+"");
        	}
        }
        else {
        	this.address = address;
        }
    }
    
    /**
     * compares the LocationData to another LocationData by timestamp
     */
    public int compareTime(LocationData loc){
    	if( timestamp < loc.getTimestamp())
    		return -1;
    	else if( timestamp > loc.getTimestamp())
    		return 1;
    	else
    		return 0;
    }

    @Override
    public boolean equals(Object o){
        if(!(o instanceof LocationData)){
            return false;
        }

        LocationData loc = (LocationData) o;

        double dx = Math.abs(Math.abs(this.lat) - Math.abs(loc.lat)); //wont work for opposite sides of the globe
        double dy = Math.abs(Math.abs(this.lng) - Math.abs(loc.lng)); //wont work for opposite sides of the globe

        double tolerance =  Math.pow(10, -1d*accuracy);
        
        return dx < tolerance && dy < tolerance;
    }

    @Override
    public int hashCode(){
        Double bufferedLat = ((double)((int)(lat * Math.pow(10, accuracy))))/Math.pow(10, accuracy);
        Double bufferedLng = ((double)((int)(lng * Math.pow(10, accuracy))))/Math.pow(10, accuracy);

        return bufferedLat.hashCode() + bufferedLng.hashCode();
    }
    
    public static String getRoadName(String lat, String lng) {

        String roadName = "";
        String url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=__LAT__,__LNG__&sensor=false";

        url = url.replaceAll("__LAT__", lat.replaceAll(" ", ""));
        url = url.replaceAll("__LNG__", lng.replaceAll(" ", ""));

        DefaultHttpClient httpclient = new DefaultHttpClient(new BasicHttpParams());
        HttpGet httpget = new HttpGet(url);

        InputStream inputStream = null;
        String result = null;
        try {
            HttpResponse response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            inputStream = entity.getContent();
            // json is UTF-8 by default
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            result = sb.toString();
        } catch (Exception e) {
            // Oops
        }
        finally {
            try{if(inputStream != null)inputStream.close();}catch(Exception squish){}
        }

        try {
        	if(result == null)
        		return null;
            if(result.equals("{\n   \"error_message\" : \"You have exceeded your daily request quota for this API.\",\n   \"results\" : [],\n   \"status\" : \"OVER_QUERY_LIMIT\"\n}\n")) {
                System.out.println("Exceeded daily request quota");
            	return null;
            }

            JSONObject jObject = new JSONObject(result);
            JSONArray jArray = jObject.getJSONArray("results");

            if (jArray == null && jArray.length() <= 0)
                return null;

            for (int i = 0; i < jArray.length(); i++) {
                try {
                    JSONObject oneObject = jArray.getJSONObject(i);
                    // Pulling items from the array
                    roadName = oneObject.getString("formatted_address");
//                    roadName = roadNameToSQLSafe(roadName);
                    return roadName;
                } catch (JSONException e) {
                    // Oops
                }
            }
        } catch (JSONException e){//this wont get thrown
            e.printStackTrace();
        }
        return null;
    }
    
    
    public static String convertMillis(long millis){
    	return String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes(millis),
			    TimeUnit.MILLISECONDS.toSeconds(millis) - 
			    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
    }

    @Override
    public String toString(){
        return lat + "," + lng;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAddress(){
    	if(address != null)
    		return address;
    	else 
    		return lat+","+lng;
    }

    public void setAddress(String address){
        this.address = address;
    }
}