multilayerAdaptiveSecurity
==========================

This repositry contains the development of an android application that establishes different levels of security by developing personalized questions based on the history available to the application.  This application will provide an interface and implementation for easily securing the applications that deserve extra security, while maintaining the look and feel of an unlocked system as much as possible. It is a user-friendly security system that avoids the issues surrounding a single pin guarding the android system.
